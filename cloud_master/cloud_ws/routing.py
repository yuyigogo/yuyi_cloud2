# this file is url routing for websocket
from django.urls import re_path

from .consumers import GatewaySensorsConsumer, UserConsumer

websocket_urlpatterns = [
    re_path(
        r"ws/cloud-user-id/(?P<user_id>[a-zA-Z0-9]+)/$",
        UserConsumer.as_asgi(),
    ),
    re_path(
        r"ws/gateways/(?P<gateway_id>[a-zA-Z0-9]+)/sensor-list/$",
        GatewaySensorsConsumer.as_asgi(),
    ),
]
