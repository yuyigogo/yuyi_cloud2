import json
import logging
from typing import Optional

from channels.generic.websocket import AsyncWebsocketConsumer

from cloud_ws.ws_uri_code import WsGroupName
from user_management.models.mongo_token import MongoToken
from user_management.models.user import CloudUser

from common.const import WS_CUSTOMER_ID_SET
from common.storage.redis import ws_redis
from urllib.parse import parse_qs

from user_management.services.user_token_service import UserTokenService

logger = logging.getLogger(__name__)


class UserConsumer(AsyncWebsocketConsumer):
    @classmethod
    def validate_user_id(cls, user_id: str) -> Optional[str]:
        try:
            customer_id = str(
                CloudUser.objects.only("customer").get(id=user_id).customer
            )
        except Exception as e:
            logger.error(f"invalid {user_id=} in connect ws!")
            return
        return customer_id

    async def connect(self):
        ws_params = self.scope["url_route"]["kwargs"]
        self.user_id = ws_params["user_id"]
        self.customer_id = self.validate_user_id(self.user_id)
        if self.customer_id is None:
            await self.close()
        self.ws_group_name = f"{WsGroupName.SENSOR_DATA.value}{self.user_id}"
        # Join into group
        await self.channel_layer.group_add(self.ws_group_name, self.channel_name)

        await self.accept()
        logger.info(f"connect ws succeed to {self.ws_group_name=}")
        # add ws connected set
        ws_redis.sadd(f"{WS_CUSTOMER_ID_SET}{self.customer_id}", self.ws_group_name)

    async def disconnect(self, close_code):
        # Leave ws group
        await self.channel_layer.group_discard(self.ws_group_name, self.channel_name)
        ws_redis.srem(f"{WS_CUSTOMER_ID_SET}{self.customer_id}", self.ws_group_name)
        logger.info(f"disconnect to {self.ws_group_name=}")

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        data = text_data_json["data"]
        logger.info(f"received ws data in {self.ws_group_name=} with {text_data=}")
        # Send message to room group
        await self.channel_layer.group_send(
            self.ws_group_name, {"type": "send_ws_data", "data": data}
        )

    # Receive message from sensor group
    async def send_ws_data(self, event):
        data = event["data"]
        # Send message to WebSocket
        await self.send(text_data=json.dumps({"message": data}))


class GatewaySensorsConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        params = self.scope["url_route"]["kwargs"]
        self.gateway_id = params["gateway_id"]

        # Extract the token from the query string
        query_string = self.scope["query_string"].decode()
        query_params = parse_qs(query_string)
        token = query_params.get("token", [None])[0]  # Default to None if token is not provided

        if not token or not self.validate_token(token):
            await self.close()
            logger.warning(f"Invalid or missing token: {token}")
            return

        self.ws_group_name = f"{WsGroupName.GATEWAY_SENSORS.value}{self.gateway_id}"

        # Join the group
        await self.channel_layer.group_add(self.ws_group_name, self.channel_name)
        await self.accept()
        logger.info(f"Connected to {self.ws_group_name}!")

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(self.ws_group_name, self.channel_name)
        logger.info(f"Disconnected from {self.ws_group_name}")

    async def ws_send_gateway_sensor_data(self, event):
        data = event["data"]
        # Send message to WebSocket
        await self.send(text_data=json.dumps({"message": data}))

    def validate_token(self, token: str) -> bool:
        logger.info(f"{token=}")
        # UserTokenService.get_token_by_key(token)
        # todo 1
        return True
