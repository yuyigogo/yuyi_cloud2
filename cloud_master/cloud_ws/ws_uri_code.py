from enum import unique

from common.const import BaseEnum


@unique
class Uri2S(int, BaseEnum):
    """
    Clint to Server: 10000 - 19999
    """

    C2S_LOGIN_REQUEST = 10000
    C2S_PING = 10001


@unique
class Uri2C(int, BaseEnum):
    """
    Server to Client: 20000 - 29999
    """

    S2C_LOGIN_REPLY = 20000
    S2C_SENSOR_DATA = 20001
    S2C_ALARM_DATA = 20002
    S2C_GATEWAY_SUCCESS_DATA = 20003
    S2C_GATEWAY_TIMEOUT_DATA = 20004
    S2C_GATEWAY_FAILED_DATA = 20005


@unique
class WsGroupName(str, BaseEnum):
    """ws group name should not contain ':'"""
    GATEWAY_SENSORS = "ws-gateway-sensors-"
    SENSOR_DATA = "ws-sensor-data-"
