import logging
import time
from functools import lru_cache

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from cloud_ws.ws_uri_code import Uri2C, WsGroupName
from customer.models.customer import Customer

from common.const import ALL, WS_CUSTOMER_ID_SET
from common.storage.redis import ws_redis

logger = logging.getLogger(__name__)


class WsSensorDataSend(object):
    """this ws is to send:
    1.sensor data;
    2.alarm data
    """

    def __init__(self, customer_id: str):
        self.customer_id = customer_id
        self.ws_customer_key = f"{WS_CUSTOMER_ID_SET}{customer_id}"

    @classmethod
    @lru_cache
    def get_named_all_customer_id(cls) -> str:
        return str(Customer.objects.get(name=ALL).pk)

    def get_exist_group_names(self) -> set:
        ws_group_names = ws_redis.smembers(self.ws_customer_key)
        named_all_customer_id = self.get_named_all_customer_id()
        super_admin_ws_key = f"{WS_CUSTOMER_ID_SET}{named_all_customer_id}"
        super_admin_ws_group_name = ws_redis.smembers(super_admin_ws_key)
        return ws_group_names.union(super_admin_ws_group_name)

    def ws_send(self, msg="", code=Uri2C.S2C_LOGIN_REPLY.value, data=None):
        data = {"msg": msg, "code": code, "data": data}
        group_names = self.get_exist_group_names()
        if not group_names:
            logger.info("[WebSocket send] ws group not found!")
        else:
            channel_layer = get_channel_layer()
            for group_name in group_names:
                try:
                    async_to_sync(channel_layer.group_send)(
                        group_name, {"type": "send_ws_data", "data": data}
                    )
                except Exception as e:
                    logger.warning(
                        f"[WebSocket send err] ignore {group_name=} {type(e).__name__}: {e}"
                    )
                    continue
                logger.info(f"[WebSocket send successfully] for {group_name=}, {data=}")

    @classmethod
    def assemble_ws_sensor_alarm_data(
        cls, sensor_obj_dict: dict, alarm_info_dict: dict
    ) -> dict:
        sensor_obj_dict["_id"] = str(sensor_obj_dict["_id"])
        ws_data = {
            "uri": 1000,
            "msg": "",
            "code": 0,
            "data": {
                "sensor_data": sensor_obj_dict,
                "alarm_data": {"alarm_detail": alarm_info_dict},
            },
        }
        return ws_data


class WsGatewaySensorsDataSend(object):
    """
    this ws is to send:
    successfully to get sensor list from gateway.
    """

    def __init__(self, gateway_id: str):
        """gateway_id is client number"""
        self.gateway_id = gateway_id
        self.prefix = "asgi"  # this key can't change,because it's set default by RedisChannelLayer
        self.ws_group_name = f"{WsGroupName.GATEWAY_SENSORS.value}{self.gateway_id}"

    def _group_key(self) -> bytes:
        """
        Common function to make the storage key for the group.
        """
        return ("%s:group:%s" % (self.prefix, self.ws_group_name)).encode("utf8")

    def group_exists(self) -> bool:
        key = self._group_key().decode()
        time.sleep(2)
        exists = ws_redis.exists(key)
        logger.info(f"Checking existence of group key {key=}: {exists}")
        return exists

    def ws_send(self, msg="success", code=Uri2C.S2C_GATEWAY_SUCCESS_DATA.value, data=None):
        logger.info(f"start to send data in WsGatewaySensorsDataSend with {msg=}, {code=}")
        if not self.group_exists():
            logger.warning(f"ws group: {self.ws_group_name} not exist! ignor it!")
            return
        data = {"msg": msg, "code": code, "data": data}
        channel_layer = get_channel_layer()
        try:
            async_to_sync(channel_layer.group_send)(
                self.ws_group_name,
                {"type": "ws_send_gateway_sensor_data", "data": data},
            )
        except Exception as e:
            logger.warning(
                f"[WsGatewaySensorsDataSend send err] {self.ws_group_name=} {type(e).__name__}: {e}"
            )
        logger.info(
            f"[WsGatewaySensorsDataSend send successfully] for {self.ws_group_name=}, {data=}"
        )
