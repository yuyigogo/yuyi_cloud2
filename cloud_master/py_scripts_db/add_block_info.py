from file_management.models.measure_point import (
    DEFAULT_ALARM_RESTRAINT_INFO,
    MeasurePoint,
)


class AlarmBlockInfo:
    @classmethod
    def run_script(cls):
        try:
            MeasurePoint.objects.filter(
                __raw__={"enable_block_alarm": {"$exists": False}}
            ).update(
                set__enable_block_alarm=False,
                set__enable_alarm_restraint=True,
                set__alarm_restraint_info=DEFAULT_ALARM_RESTRAINT_INFO,
                set__restraint_flag=False,
                set__restraint_deadline=None
            )
        except Exception as e:
            print(f"update MeasurePoint's alarm block info failed with {e}")
