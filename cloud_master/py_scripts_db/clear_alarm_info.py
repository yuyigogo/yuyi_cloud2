from alarm_management.models.alarm_info import AlarmInfo
from file_management.models.electrical_equipment import ElectricalEquipment
from file_management.models.measure_point import MeasurePoint
from sites.models.site import Site


class ClearAlarmInfo:
    """
    excel导入档案时，测点直接更新档案信息，导致alarm_info里的site_id和这个测点对应的site_id不一致
    """
    @classmethod
    def run_script(cls):
        site = Site.objects.get(pk="6672f5ad520e5b048a538598")
        point_infos = dict(MeasurePoint.objects.filter(is_online=True, site_id=site.pk).values_list("id", "equipment_id"))
        for point_id, equipment_id in point_infos.items():
            alarm = AlarmInfo.objects.filter(point_id=point_id, equipment_id__ne=equipment_id)
