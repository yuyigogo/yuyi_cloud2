from cloud.settings import MONGO_CLIENT
from file_management.models.measure_point import MeasurePoint

from common.const import SensorType


class AddOnlineFields:
    @classmethod
    def run_script(cls):
        MeasurePoint.objects.filter(
            __raw__={
                "is_online": {"$exists": False},
                "measure_type": SensorType.mech.value,
            }
        ).update(set__is_online=True, set__acq_period=0)
        points = MeasurePoint.objects.filter(
            __raw__={
                "is_online": {"$exists": False},
                "measure_type": {"$ne": SensorType.mech.value},
            }
        )
        for p in points:
            try:
                sensor_type = p.measure_type
                sensor_id = p.sensor_number
                info = cls._get_sensor_acq_period(sensor_type, sensor_id)
                if not info:
                    print(f"set {str(p.id)}'s is_online to false")
                    p.is_online = False
                    p.update(set__is_online=False)
                else:
                    print(f"set {str(p.id)}'s is_online to true")
                    p.is_online = True
                    p.acq_period = info["acq_period"]
                    p.last_received_time = info["create_date"]
                    p.update(set__is_online=True, set__acq_period=info["acq_period"],
                             set__last_received_time=info["create_date"])
            except Exception as e:
                print(f"add online fields for {str(p.id)=} {e=}")
                continue

    @classmethod
    def _get_sensor_acq_period(cls, sensor_type, sensor_id):
        display_fields = {
            "create_date": 1,
        }
        if sensor_type == SensorType.temp:
            display_fields.update({"upload_interval": 1})
        else:
            display_fields.update({"acq_period": 1})
        my_col = MONGO_CLIENT[sensor_type]
        raw_query = {"sensor_id": sensor_id, "is_latest": True}
        sensor = my_col.find_one(raw_query, display_fields)
        info = dict()
        if sensor:
            info = {"create_date": sensor["create_date"]}
            if sensor_type == SensorType.temp:
                info.update({"acq_period": sensor["upload_interval"]})
            else:
                info.update({"acq_period": sensor["acq_period"]})
        return info
