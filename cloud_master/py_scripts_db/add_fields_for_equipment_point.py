from file_management.models.electrical_equipment import ElectricalEquipment
from file_management.models.measure_point import MeasurePoint
from sites.models.site import Site


class AddFields:
    @classmethod
    def run_script(cls):
        equipments = ElectricalEquipment.objects.filter(
            __raw__={"customer_id": {"$exists": False}}
        )
        site_ids = equipments.values_list("site_id")
        site_id_customer_id = {s.pk: s.customer for s in Site.objects(id__in=site_ids)}
        for site_id in site_ids:
            try:
                ElectricalEquipment.objects(site_id=site_id).update(
                    customer_id=site_id_customer_id[site_id]
                )
            except Exception as e:
                print(f"update ElectricalEquipment's customer_id failed with {e}")
        for e in equipments:
            e.reload()

        equipment_ids = MeasurePoint.objects.filter(
            __raw__={"customer_id": {"$exists": False}, "site_id": {"$exists": False}}
        ).values_list("equipment_id")
        eq_info = {e.pk: [e.customer_id, e.site_id] for e in ElectricalEquipment.objects(id__in=equipment_ids)}
        for e_id, info_list in eq_info.items():
            try:
                MeasurePoint.objects(equipment_id=e_id).update(set__customer_id=info_list[0], set__site_id=info_list[1])
            except Exception as e:
                print(f"update MeasurePoint's customer_id and site_id failed with {e}")
