import logging

from cloud_home.services.map_service import MapService

from common.framework.response import BaseResponse
from common.framework.view import BaseView

logger = logging.getLogger(__name__)


class MapTressView(BaseView):
    def get(self, request):
        user = request.user
        logger.info(f"{user.username} request map tress")
        map_trees = MapService(user).get_map_tress_info()
        return BaseResponse(data=map_trees)
