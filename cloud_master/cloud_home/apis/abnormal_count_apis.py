from cloud_home.services.abnormal_count_service import AbnormalCacheService

from common.const import RoleLevel
from common.framework.permissions import PermissionFactory
from common.framework.response import BaseResponse
from common.framework.view import BaseView

"""
1. hincrby:
    subscribe_message: hincrby: alarm_num;
    when processed/unprocessed a alarm: hincrby: processed_num;
2. expire time
3. ws
"""


class CustomerAbnormalCountView(BaseView):
    permission_classes = (
        PermissionFactory(
            RoleLevel.CLIENT_SUPER_ADMIN.value,
            RoleLevel.CLOUD_SUPER_ADMIN.value,
            RoleLevel.ADMIN.value,
        ),
    )

    def get(self, request, customer_id):
        data = AbnormalCacheService.get_customer_abnormal_count_infos(customer_id)
        return BaseResponse(data=data)


class SiteAbnormalCountView(BaseView):
    def get(self, request, site_id):
        data = AbnormalCacheService.get_site_abnormal_count_infos(site_id)
        return BaseResponse(data=data)
