from cloud_home.services.latest3_alarms_service import LatestAlarmsService

from common.const import RoleLevel
from common.framework.permissions import PermissionFactory
from common.framework.response import BaseResponse
from common.framework.view import BaseView


class CustomerLatestAlarmsView(BaseView):
    permission_classes = (
        PermissionFactory(
            RoleLevel.CLIENT_SUPER_ADMIN.value,
            RoleLevel.CLOUD_SUPER_ADMIN.value,
            RoleLevel.ADMIN.value,
        ),
    )

    def get(self, request, customer_id):
        service = LatestAlarmsService(customer_id=customer_id)
        data = service.get_customer_or_site_latest3_alarms()
        return BaseResponse(data=data)


class SiteLatestAlarmsView(BaseView):
    def get(self, request, site_id):
        service = LatestAlarmsService(site_id=site_id)
        data = service.get_customer_or_site_latest3_alarms()
        return BaseResponse(data=data)
