from collections import defaultdict

from customer.models.customer import Customer
from equipment_management.models.sensor_config import SensorConfig
from sites.models.site import Site

from common.const import ALL, SITE_UNPROCESSED_NUM
from common.framework.service import BaseService
from common.storage.redis import normal_redis


class MapService(BaseService):
    def __init__(self, user):
        self.user = user

    @classmethod
    def get_sensor_num_in_site(cls, site_ids: list) -> dict:
        configs = SensorConfig.objects.only("site_id").filter(site_id__in=site_ids)
        sensor_num_infos = defaultdict(int)
        for config in configs:
            sensor_num_infos[config.site_id] += 1
        return sensor_num_infos

    def get_map_tress_info(self) -> list:
        if self.user.is_cloud_or_client_super_admin():
            customer_id = None
        else:
            customer_id = str(self.user.customer)
        map_trees_info = []
        if customer_id:
            info_dict = {}
            customer = Customer.objects.only("name").get(id=customer_id)
            info_dict["type"] = "customer"
            info_dict["label"] = customer.name
            info_dict["id"] = customer_id
            sites = Site.objects.only("name", "site_location").filter(
                customer=customer_id
            ).order_by("id")
            if self.user.is_normal_user():
                sites = sites.filter(id__in=self.user.sites)
            sensor_num_infos = self.get_sensor_num_in_site(sites.values_list("id"))
            info_dict["children"] = [
                {
                    "type": "site",
                    "label": site.name,
                    "site_location": site.site_location,
                    "id": str(site.pk),
                    "sensor_num": sensor_num_infos.get(site.pk, 0),
                    "unprocessed_num": normal_redis.get(
                        f"{SITE_UNPROCESSED_NUM}{str(site.pk)}"
                    ),
                }
                for site in sites
            ]
            map_trees_info.append(info_dict)
        else:
            customers = Customer.objects.only("name").filter(name__ne=ALL).order_by("id")
            customer_infos = {
                customer.pk: {
                    "type": "customer",
                    "label": customer.name,
                    "id": str(customer.pk),
                }
                for customer in customers
            }
            sites = Site.objects.only("name", "site_location", "customer").filter(
                customer__in=customer_infos.keys()
            ).order_by("id")
            sensor_num_infos = self.get_sensor_num_in_site(sites.values_list("id"))
            site_info = defaultdict(list)
            for site in sites:
                site_id = site.pk
                site_info[site.customer].append(
                    {
                        "type": "site",
                        "label": site.name,
                        "site_location": site.site_location,
                        "id": str(site_id),
                        "sensor_num": sensor_num_infos.get(site_id, 0),
                        "unprocessed_num": normal_redis.get(
                            f"{SITE_UNPROCESSED_NUM}{str(site_id)}"
                        ),
                    }
                )
            for customer_id, customer_info_dict in customer_infos.items():
                children = site_info.get(customer_id, [])
                customer_info_dict["children"] = children
                map_trees_info.append(customer_info_dict)
        return map_trees_info
