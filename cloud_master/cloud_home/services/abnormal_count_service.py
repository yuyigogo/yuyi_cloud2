import calendar
import datetime
import logging
from functools import lru_cache

from cloud_home.models.status_statistics import CAlarmCount, SAlarmCount
from customer.models.customer import Customer

from common.const import (
    ALL,
    customer_day_abnormal_info,
    customer_month_abnormal_info,
    customer_week_abnormal_info,
    site_day_abnormal_info,
    site_month_abnormal_info,
    site_week_abnormal_info,
)
from common.framework.service import BaseService
from common.storage.redis import normal_redis

logger = logging.getLogger(__name__)


class AbnormalCacheService(BaseService):
    """
    this service is for creating or updating day/week/month abnormal info for customer/site.
    1. hincrby:
    subscribe_message: hincrby: alarm_num;
    when processed/unprocessed a alarm: hincrby: processed_num;
    2. expire time
    3. ws
    所有的增减删都操作redis,总数记录在mongo中，每天凌晨定时任务重新统计
    2024-04-23：
        1.新增累计总数统计，该统计存储在mongodb中；
        2.新增定时任务：每天凌晨0点执行；手动刷新时触发；档案变化时触发
        3.注意：当测点删除时，统计时不能计算这些测点对应的告警
    """

    ALARM_NUM = "alarm_num"  # 异常数
    PROCESSED_NUM = "processed_num"  # 处理数

    def __init__(self, customer_id: str, site_id: str):
        self.customer_id = customer_id
        self.site_id = site_id
        self.customer_day_abnormal_info_key = (
            f"{customer_day_abnormal_info}{customer_id}"
        )
        self.customer_week_abnormal_info_key = (
            f"{customer_week_abnormal_info}{customer_id}"
        )
        self.customer_month_abnormal_info_key = (
            f"{customer_month_abnormal_info}{customer_id}"
        )

        self.site_day_abnormal_info_key = f"{site_day_abnormal_info}{site_id}"
        self.site_week_abnormal_info_key = f"{site_week_abnormal_info}{site_id}"
        self.site_month_abnormal_info_key = f"{site_month_abnormal_info}{site_id}"

    @classmethod
    @lru_cache
    def get_named_all_customer_id(cls):
        return str(Customer.objects.get(name=ALL).pk)

    @property
    def named_all_customer_day_abnormal_info_key(self):
        return f"{customer_day_abnormal_info}{self.get_named_all_customer_id()}"

    @property
    def named_all_customer_week_abnormal_info_key(self):
        return f"{customer_week_abnormal_info}{self.get_named_all_customer_id()}"

    @property
    def named_all_customer_month_abnormal_info_key(self):
        return f"{customer_month_abnormal_info}{self.get_named_all_customer_id()}"

    def auto_increment_customer_abnormal_infos(self, is_alarm_num=True, amount=1):
        """
        increment or decrement customer's abnormal infos;
        is_alarm_num: bool, if true, means deal with alarm_num key, else is processed_num key
        amount: int, increment/decrement number
        """
        updated_key = self.ALARM_NUM if is_alarm_num else self.PROCESSED_NUM
        self.update_customer_total_abnormal_count_info(
            self.customer_id, is_alarm_num, amount
        )
        # customer day info
        if normal_redis.exists(self.customer_day_abnormal_info_key):
            normal_redis.hincrby(
                self.customer_day_abnormal_info_key, updated_key, amount
            )
            normal_redis.hincrby(
                self.named_all_customer_day_abnormal_info_key, updated_key, amount
            )
        else:
            now = datetime.datetime.now()
            zero_today = now - datetime.timedelta(
                hours=now.hour,
                minutes=now.minute,
                seconds=now.second,
                microseconds=now.microsecond,
            )
            expire_time = zero_today + datetime.timedelta(seconds=86400)
            # set value
            normal_redis.hset(
                self.customer_day_abnormal_info_key, key=updated_key, value=amount
            )
            normal_redis.hset(
                self.named_all_customer_day_abnormal_info_key,
                key=updated_key,
                value=amount,
            )
            # set expire time
            normal_redis.expireat(self.customer_day_abnormal_info_key, expire_time)
            normal_redis.expireat(
                self.named_all_customer_day_abnormal_info_key, expire_time
            )
        # customer week info
        if normal_redis.exists(self.customer_week_abnormal_info_key):
            normal_redis.hincrby(
                self.customer_week_abnormal_info_key, updated_key, amount
            )
            normal_redis.hincrby(
                self.named_all_customer_week_abnormal_info_key, updated_key, amount
            )
        else:
            now = datetime.datetime.now()
            zero_today = now - datetime.timedelta(
                hours=now.hour,
                minutes=now.minute,
                seconds=now.second,
                microseconds=now.microsecond,
            )
            interval_days = 7 - now.isoweekday() + 1
            expire_time = zero_today + datetime.timedelta(days=interval_days)
            # set value
            normal_redis.hset(
                self.customer_week_abnormal_info_key, key=updated_key, value=amount
            )
            normal_redis.hset(
                self.named_all_customer_week_abnormal_info_key,
                key=updated_key,
                value=amount,
            )
            # set expire time
            normal_redis.expireat(self.customer_week_abnormal_info_key, expire_time)
            normal_redis.expireat(
                self.named_all_customer_week_abnormal_info_key, expire_time
            )
        # customer month info
        if normal_redis.exists(self.customer_month_abnormal_info_key):
            normal_redis.hincrby(
                self.customer_month_abnormal_info_key, updated_key, amount
            )
            normal_redis.hincrby(
                self.named_all_customer_month_abnormal_info_key, updated_key, amount
            )
        else:
            now = datetime.datetime.now()
            zero_today = now - datetime.timedelta(
                hours=now.hour,
                minutes=now.minute,
                seconds=now.second,
                microseconds=now.microsecond,
            )
            _, total_days = calendar.monthrange(now.year, now.month)
            interval_days = total_days - now.day + 1
            expire_time = zero_today + datetime.timedelta(days=interval_days)
            # set value
            normal_redis.hset(
                self.customer_month_abnormal_info_key, key=updated_key, value=amount
            )
            normal_redis.hset(
                self.named_all_customer_month_abnormal_info_key,
                key=updated_key,
                value=amount,
            )
            # set expire time
            normal_redis.expireat(self.customer_month_abnormal_info_key, expire_time)
            normal_redis.expireat(
                self.named_all_customer_month_abnormal_info_key, expire_time
            )

    def auto_increment_site_abnormal_infos(self, is_alarm_num=True, amount=1):
        updated_key = self.ALARM_NUM if is_alarm_num else self.PROCESSED_NUM
        self.update_site_total_abnormal_count_info(self.site_id, is_alarm_num, amount)
        # site day info
        if normal_redis.exists(self.site_day_abnormal_info_key):
            normal_redis.hincrby(self.site_day_abnormal_info_key, updated_key, amount)
        else:
            now = datetime.datetime.now()
            zero_today = now - datetime.timedelta(
                hours=now.hour,
                minutes=now.minute,
                seconds=now.second,
                microseconds=now.microsecond,
            )
            expire_time = zero_today + datetime.timedelta(seconds=86400)
            # set value
            normal_redis.hset(
                self.site_day_abnormal_info_key, key=updated_key, value=amount
            )
            # set expire time
            normal_redis.expireat(self.site_day_abnormal_info_key, expire_time)
        # site week info
        if normal_redis.exists(self.site_week_abnormal_info_key):
            normal_redis.hincrby(self.site_week_abnormal_info_key, updated_key, amount)
        else:
            now = datetime.datetime.now()
            zero_today = now - datetime.timedelta(
                hours=now.hour,
                minutes=now.minute,
                seconds=now.second,
                microseconds=now.microsecond,
            )
            interval_days = 7 - now.isoweekday() + 1
            expire_time = zero_today + datetime.timedelta(days=interval_days)
            # set value
            normal_redis.hset(
                self.site_week_abnormal_info_key, key=updated_key, value=amount
            )
            # set expire time
            normal_redis.expireat(self.site_week_abnormal_info_key, expire_time)
        # site month info
        if normal_redis.exists(self.site_month_abnormal_info_key):
            normal_redis.hincrby(self.site_month_abnormal_info_key, updated_key, amount)
        else:
            now = datetime.datetime.now()
            zero_today = now - datetime.timedelta(
                hours=now.hour,
                minutes=now.minute,
                seconds=now.second,
                microseconds=now.microsecond,
            )
            _, total_days = calendar.monthrange(now.year, now.month)
            interval_days = total_days - now.day + 1
            expire_time = zero_today + datetime.timedelta(days=interval_days)
            # set value
            normal_redis.hset(
                self.site_month_abnormal_info_key, key=updated_key, value=amount
            )
            # set expire time
            normal_redis.expireat(self.site_month_abnormal_info_key, expire_time)

    @classmethod
    def get_value(cls, abnormal_info_key: str) -> dict:
        """get abnormal_infos from redis"""
        fields = (cls.ALARM_NUM, cls.PROCESSED_NUM)
        values = normal_redis.hmget(abnormal_info_key, fields)
        values = [int(value) if value is not None else 0 for value in values]
        return dict(zip(fields, values))

    @classmethod
    def get_customer_abnormal_count_infos(cls, customer_id: str) -> dict:
        customer_day_abnormal_key = f"{customer_day_abnormal_info}{customer_id}"
        customer_week_abnormal_key = f"{customer_week_abnormal_info}{customer_id}"
        customer_month_abnormal_key = f"{customer_month_abnormal_info}{customer_id}"
        c_alarm_info = CAlarmCount.objects.get(customer_id=customer_id)
        total_abnormal_info = {
            "alarm_num": c_alarm_info.alarm_num,
            "processed_num": c_alarm_info.processed_num,
        }
        return {
            "customer_total_abnormal_info": total_abnormal_info,
            "customer_day_abnormal_info": cls.get_value(customer_day_abnormal_key),
            "customer_week_abnormal_info": cls.get_value(customer_week_abnormal_key),
            "customer_month_abnormal_info": cls.get_value(customer_month_abnormal_key),
        }

    @classmethod
    def get_site_abnormal_count_infos(cls, site_id: str) -> dict:
        site_day_abnormal_key = f"{site_day_abnormal_info}{site_id}"
        site_week_abnormal_key = f"{site_week_abnormal_info}{site_id}"
        site_month_abnormal_key = f"{site_month_abnormal_info}{site_id}"
        s_alarm_info = SAlarmCount.objects.get(site_id=site_id)
        total_abnormal_info = {
            "alarm_num": s_alarm_info.alarm_num,
            "processed_num": s_alarm_info.processed_num,
        }
        return {
            "site_total_abnormal_info": total_abnormal_info,
            "site_day_abnormal_info": cls.get_value(site_day_abnormal_key),
            "site_week_abnormal_info": cls.get_value(site_week_abnormal_key),
            "site_month_abnormal_info": cls.get_value(site_month_abnormal_key),
        }

    def set_customer_abnormal_count_info(
        self, month_info: dict, week_info: dict, day_info: dict
    ):
        """set the info from the task"""
        # customer day info
        if normal_redis.exists(self.customer_day_abnormal_info_key):
            normal_redis.hmset(self.customer_day_abnormal_info_key, day_info)
        else:
            now = datetime.datetime.now()
            zero_today = now - datetime.timedelta(
                hours=now.hour,
                minutes=now.minute,
                seconds=now.second,
                microseconds=now.microsecond,
            )
            expire_time = zero_today + datetime.timedelta(seconds=86400)
            # set value
            normal_redis.hmset(self.customer_day_abnormal_info_key, day_info)
            # set expire time
            normal_redis.expireat(self.customer_day_abnormal_info_key, expire_time)
        # customer week info
        if normal_redis.exists(self.customer_week_abnormal_info_key):
            normal_redis.hmset(self.customer_week_abnormal_info_key, week_info)
        else:
            now = datetime.datetime.now()
            zero_today = now - datetime.timedelta(
                hours=now.hour,
                minutes=now.minute,
                seconds=now.second,
                microseconds=now.microsecond,
            )
            interval_days = 7 - now.isoweekday() + 1
            expire_time = zero_today + datetime.timedelta(days=interval_days)
            # set value
            normal_redis.hmset(self.customer_week_abnormal_info_key, week_info)
            # set expire time
            normal_redis.expireat(self.customer_week_abnormal_info_key, expire_time)
        # customer month info
        if normal_redis.exists(self.customer_month_abnormal_info_key):
            normal_redis.hmset(self.customer_month_abnormal_info_key, month_info)
        else:
            now = datetime.datetime.now()
            zero_today = now - datetime.timedelta(
                hours=now.hour,
                minutes=now.minute,
                seconds=now.second,
                microseconds=now.microsecond,
            )
            _, total_days = calendar.monthrange(now.year, now.month)
            interval_days = total_days - now.day + 1
            expire_time = zero_today + datetime.timedelta(days=interval_days)
            # set value
            normal_redis.hmset(self.customer_month_abnormal_info_key, month_info)
            # set expire time
            normal_redis.expireat(self.customer_month_abnormal_info_key, expire_time)

    def set_site_abnormal_count_info(
        self, month_info: dict, week_info: dict, day_info: dict
    ):
        """set the info from the task"""
        # site day info
        if normal_redis.exists(self.site_day_abnormal_info_key):
            normal_redis.hmset(self.site_day_abnormal_info_key, day_info)
        else:
            now = datetime.datetime.now()
            zero_today = now - datetime.timedelta(
                hours=now.hour,
                minutes=now.minute,
                seconds=now.second,
                microseconds=now.microsecond,
            )
            expire_time = zero_today + datetime.timedelta(seconds=86400)
            # set value
            normal_redis.hmset(self.site_day_abnormal_info_key, day_info)
            # set expire time
            normal_redis.expireat(self.site_day_abnormal_info_key, expire_time)
        # site week info
        if normal_redis.exists(self.site_week_abnormal_info_key):
            normal_redis.hmset(self.site_week_abnormal_info_key, week_info)
        else:
            now = datetime.datetime.now()
            zero_today = now - datetime.timedelta(
                hours=now.hour,
                minutes=now.minute,
                seconds=now.second,
                microseconds=now.microsecond,
            )
            interval_days = 7 - now.isoweekday() + 1
            expire_time = zero_today + datetime.timedelta(days=interval_days)
            # set value
            normal_redis.hmset(self.site_week_abnormal_info_key, week_info)
            # set expire time
            normal_redis.expireat(self.site_week_abnormal_info_key, expire_time)
        # site month info
        if normal_redis.exists(self.site_month_abnormal_info_key):
            normal_redis.hmset(self.site_month_abnormal_info_key, month_info)
        else:
            now = datetime.datetime.now()
            zero_today = now - datetime.timedelta(
                hours=now.hour,
                minutes=now.minute,
                seconds=now.second,
                microseconds=now.microsecond,
            )
            _, total_days = calendar.monthrange(now.year, now.month)
            interval_days = total_days - now.day + 1
            expire_time = zero_today + datetime.timedelta(days=interval_days)
            # set value
            normal_redis.hmset(self.site_month_abnormal_info_key, month_info)
            # set expire time
            normal_redis.expireat(self.site_month_abnormal_info_key, expire_time)

    @classmethod
    def update_customer_total_abnormal_count_info(
        cls, customer_id: str, is_alarm_num=True, amount=1
    ):
        logger.info(
            f"update_customer_total_abnormal_count_info with {customer_id=}, {is_alarm_num=}, {amount=}"
        )
        c_alarm_info = CAlarmCount.objects.get(customer_id=customer_id)
        if is_alarm_num:
            c_alarm_info.alarm_num = c_alarm_info.alarm_num + amount
        else:
            c_alarm_info.processed_num = c_alarm_info.processed_num + amount
        c_alarm_info.save()

    @classmethod
    def update_site_total_abnormal_count_info(
        cls, site_id: str, is_alarm_num=True, amount=1
    ):
        logger.info(
            f"update_site_total_abnormal_count_info with {site_id=}, {is_alarm_num=}, {amount=}"
        )
        s_alarm_info = SAlarmCount.objects.get(site_id=site_id)
        if is_alarm_num:
            s_alarm_info.alarm_num = s_alarm_info.alarm_num + amount
        else:
            s_alarm_info.processed_num = s_alarm_info.processed_num + amount
        s_alarm_info.save()
