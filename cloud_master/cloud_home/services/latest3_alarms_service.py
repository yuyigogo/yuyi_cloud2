from typing import Optional

from alarm_management.models.alarm_info import AlarmInfo
from bson import ObjectId
from cloud.models import bson_to_dict
from customer.models.customer import Customer
from file_management.models.electrical_equipment import ElectricalEquipment
from mongoengine import Q
from sites.models.site import Site

from common.const import ALL, AlarmLevel, AlarmType
from common.framework.service import BaseService
from common.utils import get_objects_pagination


class LatestAlarmsService(BaseService):
    def __init__(
        self, customer_id: Optional[str] = None, site_id: Optional[str] = None
    ):
        assert customer_id or site_id, "LatestAlarmsService init error"
        self.customer_id = customer_id
        self.site_id = site_id
        if self.customer_id:
            self.named_all_customer_id = str(Customer.objects.get(name=ALL).pk)
            self.is_named_all = customer_id == self.named_all_customer_id

    def get_customer_or_site_latest3_alarms(self):
        query = Q(
            alarm_type=AlarmType.POINT_ALARM.value,
            is_processed=False,
            alarm_level__in=AlarmLevel.abnormal_alarm_level(),
        )
        if self.site_id:
            query &= Q(site_id=self.site_id)
        else:
            if not self.is_named_all:
                query &= Q(customer_id=self.customer_id)
        alarm_infos = AlarmInfo.objects.filter(query)
        alarm_infos_by_page = get_objects_pagination(
            1, 3, alarm_infos.order_by("-create_date")
        )
        site_ids, equipment_ids, data = [], [], []
        for alarm_info in alarm_infos_by_page:
            site_id = str(alarm_info.site_id)
            equipment_id = str(alarm_info.equipment_id)
            site_ids.append(site_id)
            equipment_ids.append(equipment_id)
            data.append(
                {
                    "customer_id": str(alarm_info.customer_id),
                    "site_id": site_id,
                    "equipment_id": equipment_id,
                    "is_processed": alarm_info.is_processed,
                    "alarm_level": alarm_info.alarm_level,
                    "sensor_type": alarm_info.sensor_type,
                    "sensor_id": alarm_info.sensor_id,
                    "alarm_id": str(alarm_info.pk),
                    "processed_remarks": alarm_info.processed_remarks,
                    "create_date": alarm_info.create_date.strftime('%Y-%m-%d %H:%M:%S'),
                    "sensor_data_id": str(alarm_info.sensor_data_id),
                }
            )
        site_infos, equipment_infos = self.get_site_equipment_name(
            site_ids, equipment_ids
        )
        [
            d.update(
                {
                    "site_name": site_infos.get(ObjectId(d["site_id"])),
                    "equipment_name": equipment_infos.get(ObjectId(d["equipment_id"])),
                }
            )
            for d in data
        ]
        return data

    @classmethod
    def get_site_equipment_name(cls, site_ids: list, equipment_ids: list) -> tuple:
        site_infos = dict(
            Site.objects.filter(id__in=site_ids).values_list("id", "name")
        )
        equipment_infos = dict(
            ElectricalEquipment.objects.filter(id__in=equipment_ids).values_list(
                "id", "device_name"
            )
        )
        return site_infos, equipment_infos
