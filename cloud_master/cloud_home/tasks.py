import logging
from datetime import datetime, timedelta
from typing import Optional

from alarm_management.models.alarm_info import AlarmInfo
from bson import ObjectId
from cloud_home.models.status_statistics import (
    CAbnormalRatio,
    CAlarmCount,
    CStatusStatistic,
    SAbnormalRatio,
    SAlarmCount,
    SStatusStatistic,
)
from cloud_home.services.abnormal_count_service import AbnormalCacheService
from cloud_home.services.status_statistics_service import StatusStatisticService
from common.storage.redis import normal_redis
from customer.models.customer import Customer
from file_management.models.measure_point import MeasurePoint
from mongoengine import Q
from pymongo import UpdateOne
from sites.models.site import Site

from common.const import ALL, AlarmLevel, AlarmType, SITE_UNPROCESSED_NUM
from common.scheduler import cloud_task

logger = logging.getLogger(__name__)


@cloud_task
def async_customer_status_statistic(customer_ids: Optional[list] = None):
    """
    1. counting customer/site status infos;
    2. the status infos include:
        a. asset info;
        b. equipment status info;
        c. sensor online ratio info;
        d. point distribution info;
    3. insert those info into c_status_statistic/s_status_statistic
    """
    logger.info(f"start to run async_customer_status_statistic for {customer_ids=}")
    if customer_ids is None:
        # get all customer_ids
        customer_ids = Customer.objects(name__ne=ALL).values_list("id")
        customer_ids = list(map(str, customer_ids))
    bulk_operations = []
    for customer_id in customer_ids:
        service = StatusStatisticService(customer_id=customer_id)
        asset_info = service.get_customer_asset_info()
        equipment_status_info = service.get_customer_equipment_status_statistics()
        sensor_online_ratio = service.get_customer_or_site_sensor_online_ratio()
        point_distribution_info = service.get_customer_or_site_point_distribution_info()
        bulk_operations.append(
            UpdateOne(
                {"customer_id": ObjectId(customer_id)},
                {
                    "$set": {
                        "asset_info": asset_info,
                        "equipment_status_info": equipment_status_info,
                        "sensor_online_ratio": sensor_online_ratio,
                        "point_distribution_info": point_distribution_info,
                        "create_date": datetime.now(),
                        "update_date": datetime.now(),
                    }
                },
                upsert=True,
            )
        )
    collection = CStatusStatistic._get_collection()
    collection.bulk_write(bulk_operations, ordered=False)
    logger.info(
        f"success to insert {len(bulk_operations)} c_status_statistic for {customer_ids=}"
    )


@cloud_task
def async_site_status_statistic(site_ids: Optional[list] = None):
    logger.info(f"start to run async_site_status_statistic for {site_ids=}")
    update_named_all_site = False
    if site_ids is None:
        # get all site_ids
        update_named_all_site = True
        site_ids = Site.objects(name__ne=ALL).values_list("id")
        site_ids = list(map(str, site_ids))
    bulk_operations = []
    for site_id in site_ids:
        service = StatusStatisticService(site_id=site_id)
        asset_info = service.get_site_asset_info()
        equipment_status_info = service.get_site_equipment_status_statistics_in_site()
        sensor_online_ratio = service.get_customer_or_site_sensor_online_ratio()
        point_distribution_info = service.get_customer_or_site_point_distribution_info()
        bulk_operations.append(
            UpdateOne(
                {"site_id": ObjectId(site_id)},
                {
                    "$set": {
                        "asset_info": asset_info,
                        "equipment_status_info": equipment_status_info,
                        "sensor_online_ratio": sensor_online_ratio,
                        "point_distribution_info": point_distribution_info,
                        "create_date": datetime.now(),
                        "update_date": datetime.now(),
                    }
                },
                upsert=True,
            )
        )
    # if update_named_all_site:
    #     site = Site.objects.only("customer").get(name=ALL)
    #     c_all_info = CStatusStatistic.objects.get(customer_id=site.customer)
    #     bulk_operations.append(
    #         UpdateOne(
    #             {"site_id": site.id},
    #             {
    #                 "$set": {
    #                     "asset_info": c_all_info.asset_info,
    #                     "equipment_status_info": c_all_info.equipment_status_info,
    #                     "sensor_online_ratio": c_all_info.sensor_online_ratio,
    #                     "point_distribution_info": c_all_info.point_distribution_info,
    #                     "create_date": datetime.now(),
    #                     "update_date": datetime.now(),
    #                 }
    #             },
    #             upsert=True,
    #         )
    #     )
    collection = SStatusStatistic._get_collection()
    collection.bulk_write(bulk_operations, ordered=False)
    logger.info(
        f"success to insert {len(bulk_operations)} s_status_statistic for {site_ids=}"
    )


@cloud_task
def async_customer_equipment_abnormal_ratio(customer_ids: Optional[list] = None):
    logger.info(
        f"start to run async_customer_equipment_abnormal_ratio for {customer_ids=}"
    )
    if customer_ids is None:
        # get all customer_ids
        customer_ids = Customer.objects(name__ne=ALL).values_list("id")
        customer_ids = list(map(str, customer_ids))
    bulk_operations = []
    for customer_id in customer_ids:
        service = StatusStatisticService(customer_id=customer_id)
        ratio = service.get_customer_or_site_equipment_abnormal_ratio()
        bulk_operations.append(
            {
                "customer_id": ObjectId(customer_id),
                "ratio": ratio,
                "create_date": datetime.now(),
                "update_date": datetime.now(),
            }
        )
    collection = CAbnormalRatio._get_collection()
    collection.insert_many(bulk_operations, ordered=False)
    logger.info(
        f"success to insert {len(bulk_operations)} c_abnormal_ratio for {customer_ids=}"
    )


@cloud_task
def async_site_equipment_abnormal_ratio(site_ids: Optional[list] = None):
    logger.info(f"start to run async_site_equipment_abnormal_ratio for {site_ids=}")
    if site_ids is None:
        # get all site_ids
        site_ids = Site.objects(name__ne=ALL).values_list("id")
        site_ids = list(map(str, site_ids))
    bulk_operations = []
    for site_id in site_ids:
        service = StatusStatisticService(site_id=site_id)
        ratio = service.get_customer_or_site_equipment_abnormal_ratio()
        bulk_operations.append(
            {
                "site_id": ObjectId(site_id),
                "ratio": ratio,
                "create_date": datetime.now(),
                "update_date": datetime.now(),
            }
        )
    if site_ids is None:
        site = Site.objects.only("customer").get(name=ALL)
        named_all_site_id = site.pk
        c_abnormal_ratio = (
            CAbnormalRatio.objects.filter(customer_id=site.customer)
            .order_by("-create_date")
            .first()
        )
        bulk_operations.append(
            {
                "site_id": named_all_site_id,
                "ratio": c_abnormal_ratio.ratio,
                "create_date": datetime.now(),
                "update_date": datetime.now(),
            }
        )
    collection = SAbnormalRatio._get_collection()
    collection.insert_many(bulk_operations, ordered=False)
    logger.info(
        f"success to insert {len(bulk_operations)} s_abnormal_ratio for {site_ids=}"
    )


@cloud_task
def async_customer_abnormal_count(customer_ids: Optional[list] = None):
    """
    公司下异常情况处理统计：处理数/异常数；总数/本月/本周/本日
    1.排除已删除的告警：查询粒度到point_id
    2.先查总数，再按照时间查询(create_date)
    3.统计完，插入到缓存
    4.接口操作上(读/改)都是操作缓存，由定时任务重新统计时，更新改缓存
    :param customer_ids:
    :return:
    """
    logger.info(f"start to run async_customer_abnormal_count for {customer_ids=}")
    if customer_ids is None:
        # get all customer_ids
        customer_ids = Customer.objects(name__ne=ALL).values_list("id")
        customer_ids = list(map(str, customer_ids))
    bulk_operations = []
    for customer_id in customer_ids:
        point_ids = MeasurePoint.objects.filter(customer_id=customer_id).values_list(
            "id"
        )
        # 这里因为excel导入档案时，测点直接更新档案信息，导致alarm_info里的site_id和这个测点对应的site_id不一致
        # 所以查询base_alarm_query时需要加上customer_id或者site_id
        base_alarm_query = Q(
            alarm_type=AlarmType.POINT_ALARM.value,
            alarm_level__in=AlarmLevel.abnormal_alarm_level(),
            point_id__in=point_ids,
            customer_id=customer_id,
        )
        base_alarm = AlarmInfo.objects.filter(base_alarm_query)
        # 先查总数
        abnormal_num = base_alarm.count()
        processed_query = base_alarm_query & Q(is_processed=True)
        processed_num = base_alarm.filter(processed_query).count()
        bulk_operations.append(
            UpdateOne(
                {"customer_id": ObjectId(customer_id)},
                {
                    "$set": {
                        "customer_id": ObjectId(customer_id),
                        "processed_num": processed_num,
                        "alarm_num": abnormal_num,
                        "create_date": datetime.now(),
                        "update_date": datetime.now(),
                    }
                },
                upsert=True,
            ),
        )

        # 查本月
        now = datetime.now()
        first_day_of_month = datetime(now.year, now.month, 1, 0, 0)
        m_total_query = (
            base_alarm_query
            & Q(create_date__gte=first_day_of_month)
            & Q(create_date__lte=now)
        )
        m_abnormal_num = base_alarm.filter(m_total_query).count()
        m_processed_query = m_total_query & Q(is_processed=True)
        m_processed_num = base_alarm.filter(m_processed_query).count()
        month_info = {
            AbnormalCacheService.ALARM_NUM: m_abnormal_num,
            AbnormalCacheService.PROCESSED_NUM: m_processed_num,
        }

        # 查本周
        first_of_day = datetime(now.year, now.month, now.day)
        first_day_of_week = first_of_day - timedelta(days=now.weekday())
        w_total_query = (
            base_alarm_query
            & Q(create_date__gte=first_day_of_week)
            & Q(create_date__lte=now)
        )
        w_abnormal_num = base_alarm.filter(w_total_query).count()
        w_processed_query = w_total_query & Q(is_processed=True)
        w_processed_num = base_alarm.filter(w_processed_query).count()
        week_info = {
            AbnormalCacheService.ALARM_NUM: w_abnormal_num,
            AbnormalCacheService.PROCESSED_NUM: w_processed_num,
        }

        # 查当天
        d_total_query = (
            base_alarm_query
            & Q(create_date__gte=first_of_day)
            & Q(create_date__lte=now)
        )
        d_abnormal_num = base_alarm.filter(d_total_query).count()
        d_processed_query = d_total_query & Q(is_processed=True)
        d_processed_num = base_alarm.filter(d_processed_query).count()
        day_info = {
            AbnormalCacheService.ALARM_NUM: d_abnormal_num,
            AbnormalCacheService.PROCESSED_NUM: d_processed_num,
        }

        # set to redis
        AbnormalCacheService(
            customer_id=customer_id, site_id=""
        ).set_customer_abnormal_count_info(month_info, week_info, day_info)

    if bulk_operations:
        collection = CAlarmCount._get_collection()
        collection.bulk_write(bulk_operations, ordered=False)
        logger.info(
            f"success to insert {len(bulk_operations)} c_alarm_count for {customer_ids=}"
        )
    logger.info(f"finished to run async_customer_abnormal_count for {customer_ids=}")


@cloud_task
def async_site_abnormal_count(site_ids: Optional[list] = None):
    """
    公司下异常情况处理统计：处理数/异常数；总数/本月/本周/本日
    1.排除已删除的告警：查询粒度到point_id
    2.先查总数，再按照时间查询(create_date)
    3.统计完，插入到缓存
    4.接口操作上(读/改)都是操作缓存，由定时任务重新统计时，更新改缓存
    5.档案删除，这些数据需要删除
    6.统计完后，需要设置 unprocessed_unm for site = total - processed_num (小圆圈)
    :param site_ids:
    :return:
    """
    logger.info(f"start to run async_site_abnormal_count for {site_ids=}")
    if site_ids is None:
        # get all site ids
        site_ids = Site.objects(name__ne=ALL).values_list("id")
        site_ids = list(map(str, site_ids))
    bulk_operations = []
    for site_id in site_ids:
        point_ids = MeasurePoint.objects.filter(site_id=site_id).values_list("id")
        base_alarm_query = Q(
            alarm_type=AlarmType.POINT_ALARM.value,
            alarm_level__in=AlarmLevel.abnormal_alarm_level(),
            point_id__in=point_ids,
            site_id=site_id,
        )
        base_alarm = AlarmInfo.objects.filter(base_alarm_query)
        # 先查总数
        abnormal_num = base_alarm.count()
        processed_query = base_alarm_query & Q(is_processed=True)
        processed_num = base_alarm.filter(processed_query).count()
        # 上面的6
        normal_redis.set(f"{SITE_UNPROCESSED_NUM}{site_id}", abnormal_num - processed_num)
        bulk_operations.append(
            UpdateOne(
                {"site_id": ObjectId(site_id)},
                {
                    "$set": {
                        "site_id": ObjectId(site_id),
                        "processed_num": processed_num,
                        "alarm_num": abnormal_num,
                        "create_date": datetime.now(),
                        "update_date": datetime.now(),
                    }
                },
                upsert=True,
            ),
        )

        # 查本月
        now = datetime.now()
        first_day_of_month = datetime(now.year, now.month, 1, 0, 0)
        m_total_query = (
            base_alarm_query
            & Q(create_date__gte=first_day_of_month)
            & Q(create_date__lte=now)
        )
        m_abnormal_num = base_alarm.filter(m_total_query).count()
        m_processed_query = m_total_query & Q(is_processed=True)
        m_processed_num = base_alarm.filter(m_processed_query).count()
        month_info = {
            AbnormalCacheService.ALARM_NUM: m_abnormal_num,
            AbnormalCacheService.PROCESSED_NUM: m_processed_num,
        }

        # 查本周
        first_of_day = datetime(now.year, now.month, now.day)
        first_day_of_week = first_of_day - timedelta(days=now.weekday())
        w_total_query = (
            base_alarm_query
            & Q(create_date__gte=first_day_of_week)
            & Q(create_date__lte=now)
        )
        w_abnormal_num = base_alarm.filter(w_total_query).count()
        w_processed_query = w_total_query & Q(is_processed=True)
        w_processed_num = base_alarm.filter(w_processed_query).count()
        week_info = {
            AbnormalCacheService.ALARM_NUM: w_abnormal_num,
            AbnormalCacheService.PROCESSED_NUM: w_processed_num,
        }

        # 查当天
        d_total_query = (
            base_alarm_query
            & Q(create_date__gte=first_of_day)
            & Q(create_date__lte=now)
        )
        d_abnormal_num = base_alarm.filter(d_total_query).count()
        d_processed_query = d_total_query & Q(is_processed=True)
        d_processed_num = base_alarm.filter(d_processed_query).count()
        day_info = {
            AbnormalCacheService.ALARM_NUM: d_abnormal_num,
            AbnormalCacheService.PROCESSED_NUM: d_processed_num,
        }

        # set to redis
        AbnormalCacheService(
            customer_id="", site_id=site_id
        ).set_site_abnormal_count_info(month_info, week_info, day_info)

    if bulk_operations:
        collection = SAlarmCount._get_collection()
        collection.bulk_write(bulk_operations, ordered=False)
        logger.info(
            f"success to insert {len(bulk_operations)} s_alarm_count for {site_ids=}"
        )
    logger.info(f"finished to run async_site_abnormal_count for {site_ids=}")
