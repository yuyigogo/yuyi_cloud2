import logging

from customer.models.customer import Customer
from mongoengine import DoesNotExist, MultipleObjectsReturned
from rest_framework.fields import CharField, FileField
from sites.models.site import Site

from common.framework.exception import APIException
from common.framework.serializer import BaseSerializer

logger = logging.getLogger(__name__)


class ExcelImportSerializer(BaseSerializer):
    file = FileField(required=True, allow_empty_file=False)


class FileExportViewSerializer(BaseSerializer):
    customer_id = CharField(required=True)
    site_id = CharField(required=False)

    def validate(self, data: dict) -> dict:
        user = self.context["request"].user
        customer_id = data["customer_id"]
        if user.is_normal_admin() and str(user.customer) != customer_id:
            raise APIException("用户无权限!")
        return data

    def validate_customer_id(self, customer_id: str) -> str:
        try:
            customer = Customer.objects.get(pk=customer_id)
        except (DoesNotExist, MultipleObjectsReturned) as e:
            raise APIException(f"can't get customer by {customer_id}")
        self.context["customer"] = customer
        return customer_id

    def validate_site_id(self, site_id: str) -> str:
        try:
            site = Site.objects.get(pk=site_id)
        except (DoesNotExist, MultipleObjectsReturned) as e:
            raise APIException(f"can't get site by {site_id}")
        self.context["site"] = site
        return site_id
