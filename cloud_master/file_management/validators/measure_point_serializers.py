import logging
from typing import Optional

from file_management.models.electrical_equipment import ElectricalEquipment
from file_management.models.measure_point import MeasurePoint
from mongoengine import DoesNotExist
from rest_framework.fields import BooleanField, CharField, DictField

from common.const import (
    MAX_LENGTH_NAME,
    MAX_MESSAGE_LENGTH,
    MODEL_KEY_TO_SENSOR_TYPE,
    SENSOR_INFO_PREFIX,
    AlarmRestraintKeys,
    SensorType,
)
from common.error_code import StatusCode
from common.framework.exception import APIException, InvalidException
from common.framework.serializer import BaseSerializer
from common.storage.redis import normal_redis


def get_equipment(equipment_id: str) -> Optional[ElectricalEquipment]:
    try:
        return ElectricalEquipment.objects.only("site_id", "customer_id").get(
            id=equipment_id
        )
    except DoesNotExist:
        raise InvalidException(f"invalid {equipment_id=}")


def validate_sensor_id_by_model_key(sensor_id: str) -> tuple:
    """
    :param sensor_id:
    :return: model_key, sensor_type
    """
    sensor_model_key = f"{SENSOR_INFO_PREFIX}{sensor_id}"
    model_key = normal_redis.hget(sensor_model_key, "model_key")
    if not model_key:
        return None, None
    else:
        return model_key, MODEL_KEY_TO_SENSOR_TYPE.get(model_key)


class CreatePointSerializer(BaseSerializer):
    measure_name = CharField(required=True, max_length=MAX_LENGTH_NAME)
    measure_type = CharField(required=True)
    sensor_number = CharField(required=True)
    remarks = CharField(
        max_length=MAX_MESSAGE_LENGTH, allow_blank=True, allow_null=True
    )
    enable_block_alarm = BooleanField(default=False)
    enable_alarm_restraint = BooleanField(default=False)
    alarm_restraint_info = DictField(required=False)

    def validate_measure_name(self, measure_name):
        equipment_id = self.context["equipment_id"]
        if (
            MeasurePoint.objects(
                equipment_id=equipment_id, measure_name=measure_name
            ).count()
            > 0
        ):
            raise APIException(
                msg="测点名称已存在！", code=StatusCode.POINT_NAME_DUPLICATE.value
            )
        return measure_name

    def validate_measure_type(self, measure_type: str) -> str:
        if measure_type not in SensorType.values():
            raise APIException(f"invalid {measure_type=}")
        return measure_type

    def validate_alarm_restraint_info(self, alarm_restraint_info: dict) -> dict:
        keys = set(alarm_restraint_info.keys())
        if keys != set(AlarmRestraintKeys.values()):
            raise APIException(f"{alarm_restraint_info=} keys error!")
        return alarm_restraint_info

    def validate(self, data: dict) -> dict:
        sensor_number = data["sensor_number"]
        measure_type = data["measure_type"]
        model_key, sensor_type = validate_sensor_id_by_model_key(sensor_number)
        if model_key and measure_type != sensor_type:
            raise APIException(f"传感器类型匹配错误！")
        equipment_id = self.context["equipment_id"]
        equipment = get_equipment(equipment_id)
        if (
            MeasurePoint.objects(
                measure_type=data["measure_type"], sensor_number=data["sensor_number"]
            ).count()
            > 0
        ):
            raise APIException("传感器编号已绑定！")
        data["site_id"] = str(equipment.site_id)
        data["customer_id"] = str(equipment.customer_id)
        if not self.admin_has_permission_to_customer(
            self.context["request"].user, equipment.customer_id
        ):
            raise APIException("该用户无次操作权限!")

        # validate enable_block_alarm and alarm_restraint
        enable_block_alarm = data.get("enable_block_alarm", False)
        enable_alarm_restraint = data.get("enable_alarm_restraint")
        alarm_restraint_info = data.get("alarm_restraint_info")
        if enable_block_alarm and enable_alarm_restraint:
            raise APIException(f"报警屏蔽和抑制使能冲突!")
        if enable_alarm_restraint and not alarm_restraint_info:
            raise APIException(f"报警抑制信息缺失!")
        return data


class UpdatePointSerializer(BaseSerializer):
    measure_name = CharField(max_length=MAX_LENGTH_NAME)
    measure_type = CharField()
    sensor_number = CharField()
    remarks = CharField(
        max_length=MAX_MESSAGE_LENGTH, allow_blank=True, allow_null=True
    )
    enable_block_alarm = BooleanField(default=False)
    enable_alarm_restraint = BooleanField(default=False)
    alarm_restraint_info = DictField(required=False)

    def validate_measure_name(self, measure_name):
        equipment_id = self.context["equipment_id"]
        point_id = self.context["point_id"]
        if (
            MeasurePoint.objects(
                equipment_id=equipment_id, measure_name=measure_name, id__ne=point_id
            ).count()
            > 0
        ):
            raise APIException(
                msg="测点名称已存在！", code=StatusCode.POINT_NAME_DUPLICATE.value
            )
        return measure_name

    def validate(self, data: dict) -> dict:
        equipment_id = self.context["equipment_id"]
        point_id = self.context["point_id"]
        sensor_number = data["sensor_number"]
        measure_type = data["measure_type"]
        model_key, sensor_type = validate_sensor_id_by_model_key(sensor_number)
        if model_key and measure_type != sensor_type:
            raise APIException(f"传感器{model_key=}和类型不匹配{sensor_type=}")
        try:
            point = MeasurePoint.objects.get(id=point_id)
        except DoesNotExist:
            raise InvalidException(f"invalid {equipment_id=} or {point_id=}")
        if not self.admin_has_permission_to_customer(
            self.context["request"].user, point.customer_id
        ):
            raise APIException("该用户无次操作权限!")
        self.context["point"] = point
        if point.measure_type != data["measure_type"]:
            # forbidden to modify the measure_type
            raise APIException("禁止修改已绑定传感器的测点类型!")
        self.context["changed_sensor_number"] = False
        if point.sensor_number != data["sensor_number"]:
            if (
                MeasurePoint.objects(
                    sensor_number=data["sensor_number"],
                ).count()
                > 0
            ):
                raise APIException("传感器编号已绑定！")
            self.context["changed_sensor_number"] = True
            equipment = get_equipment(equipment_id)
            self.context["site_id"] = str(equipment.site_id)
            self.context["customer_id"] = str(equipment.customer_id)

        enable_block_alarm = data.get("enable_block_alarm", False)
        enable_alarm_restraint = data.get("enable_alarm_restraint")
        alarm_restraint_info = data.get("alarm_restraint_info")
        if enable_block_alarm and enable_alarm_restraint:
            raise APIException(f"报警屏蔽和抑制使能冲突!")
        if enable_alarm_restraint and not alarm_restraint_info:
            raise APIException(f"报警抑制信息缺失!")

        o_alarm_restraint_info = point.alarm_restraint_info
        self.context["change_restraint_flag"] = False
        if enable_block_alarm is True or enable_alarm_restraint is False:
            self.context["change_restraint_flag"] = True

        elif has_value_changed(alarm_restraint_info, o_alarm_restraint_info):
            self.context["change_restraint_flag"] = True
        return data


def has_value_changed(dict1, dict2):
    for key in dict1:
        if key in dict2 and dict1[key] != dict2[key]:
            return True
    return False


class DeletePointSerializer(BaseSerializer):
    clear_resource = BooleanField(default=False)

    def validate(self, data: dict) -> dict:
        point_id = self.context["point_id"]
        try:
            point = MeasurePoint.objects.get(id=point_id)
        except DoesNotExist:
            raise InvalidException(f"invalid {point_id=}")
        if not self.admin_has_permission_to_customer(
            self.context["request"].user, point.customer_id
        ):
            raise APIException("该用户无次操作权限!")
        data["customer_id"] = str(point.customer_id)
        data["site_id"] = str(point.site_id)
        return data
