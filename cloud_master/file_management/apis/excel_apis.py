import logging
import os
from datetime import datetime

from cloud.celery import app
from cloud.settings import BASE_DIR
from django.http import HttpResponse
from file_management.services.excel_service import ExcelService, FileExportService
from file_management.validators.excel_file_serializers import (
    ExcelImportSerializer,
    FileExportViewSerializer,
)
from openpyxl import load_workbook

from common.const import RoleLevel
from common.framework.permissions import PermissionFactory
from common.framework.response import BaseResponse
from common.framework.view import BaseView
from common.utils.str_utils import to_ascii

logger = logging.getLogger(__name__)


class ImportFileView(BaseView):
    permission_classes = (
        PermissionFactory(
            RoleLevel.CLIENT_SUPER_ADMIN.value,
            RoleLevel.CLOUD_SUPER_ADMIN.value,
            RoleLevel.ADMIN.value,
        ),
    )

    def post(self, request):
        data, _ = self.get_validated_data(ExcelImportSerializer)
        data = ExcelService(request.user).file_import(excel_file=data["file"])
        app.send_task("cloud_home.tasks.async_customer_status_statistic")
        app.send_task("cloud_home.tasks.async_customer_abnormal_count")
        app.send_task("cloud_home.tasks.async_customer_equipment_abnormal_ratio")
        app.send_task("cloud_home.tasks.async_site_abnormal_count")
        return BaseResponse(data=data)


class FileExportView(BaseView):
    permission_classes = (
        PermissionFactory(
            RoleLevel.CLIENT_SUPER_ADMIN.value,
            RoleLevel.CLOUD_SUPER_ADMIN.value,
            RoleLevel.ADMIN.value,
        ),
    )

    def get(self, request):
        data, context = self.get_validated_data(FileExportViewSerializer)
        user = request.user
        logger.info(f"{user} request export files with {data=}!")
        customer = context["customer"]
        site = context.get("site")
        file_export_service = FileExportService(customer, site)
        content = file_export_service.get_excel_content()
        now = datetime.now()
        if site:
            filename = (
                f"{customer.name}--{site.name}--云平台档案--{now.strftime('%Y%m%d')}.xlsx"
            )
        else:
            filename = f"{customer.name}--云平台档案--{now.strftime('%Y%m%d')}.xlsx"
        disposition = f"attachment; filename={to_ascii(filename)}"
        response = HttpResponse(content, content_type="application/vnd.ms-excel")
        response["Content-Disposition"] = disposition
        return response


class FileExcelTemplateView(BaseView):
    def get(self, request, *args, **kwargs):
        template_path = os.path.join(BASE_DIR, "cloud_file.xlsx")
        # load excel template
        wb = load_workbook(template_path)
        response = HttpResponse(
            content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        )
        # filename = "云平台档案管理模板.xlsx"
        # url_encoded_filename = quote(filename)
        # response[
        #     "Content-Disposition"
        # ] = f"attachment; filename*=UTF-8''{url_encoded_filename}"
        response[
            "Content-Disposition"
        ] = "attachment; filename=cloud_template_file.xlsx"
        wb.save(response)

        return response
