import logging

from cloud.celery import app
from file_management.models.measure_point import MeasurePoint, DEFAULT_ALARM_RESTRAINT_INFO
from file_management.services.meansure_point_service import MeasurePointService
from file_management.validators.measure_point_serializers import (
    CreatePointSerializer,
    DeletePointSerializer,
    UpdatePointSerializer,
)
from rest_framework.status import HTTP_201_CREATED

from common.const import RoleLevel, SensorType
from common.framework.permissions import PermissionFactory
from common.framework.response import BaseResponse
from common.framework.service import SensorConfigService
from common.framework.view import BaseView

logger = logging.getLogger(__name__)

"""
测点绑定传感器的逻辑：
a.新建测点：拿sensor_id查询是否有SensorConfig,如果有，更新对应的档案信息且has_bound = True;
b.修改测点: 将old_sensor_id对应的SensorConfig has_bound 更新为False，且redis中has_bound=False,同时将new_sensor_id对应进行a操作；
c.删除测点：SensorConfig的has_bound 更新为False，且redis中has_bound=False,如果选择要清楚数据，将该sensor_id对应的数据清除；
"""


class MeasurePointListView(BaseView):
    permission_classes = (
        PermissionFactory(
            RoleLevel.CLIENT_SUPER_ADMIN.value,
            RoleLevel.CLOUD_SUPER_ADMIN.value,
            RoleLevel.ADMIN.value,
        ),
    )

    def post(self, request, equipment_id):
        """create a new measure point for equipment"""
        user = request.user
        data, _ = self.get_validated_data(
            CreatePointSerializer, equipment_id=equipment_id
        )
        logger.info(f"{user.username} request create a new measure point with data")
        measure_point = MeasurePointService(equipment_id).create_new_measure_point(data)
        # update sensor_config and set sensor_info to redis
        SensorConfigService(
            measure_point.sensor_number
        ).sensor_config_operations_in_point_creation(
            data["customer_id"],
            data["site_id"],
            equipment_id,
            str(measure_point.pk),
            measure_point.measure_type,
        )
        customer_ids = [data["customer_id"]]
        site_ids = [data["site_id"]]
        app.send_task(
            "cloud_home.tasks.async_customer_status_statistic", args=(customer_ids,)
        )
        app.send_task(
            "cloud_home.tasks.async_customer_equipment_abnormal_ratio",
            args=(customer_ids,),
        )
        app.send_task(
            "cloud_home.tasks.async_site_status_statistic",
            args=(site_ids,),
        )
        app.send_task(
            "cloud_home.tasks.async_site_equipment_abnormal_ratio",
            args=(site_ids,),
        )
        return BaseResponse(data=measure_point.to_dict(), status_code=HTTP_201_CREATED)

    def get(self, request, equipment_id):
        """get list points in one equipment"""
        points = MeasurePointService(equipment_id).get_points_for_equipment()
        return BaseResponse(data=points)


class MeasurePointView(BaseView):
    permission_classes = (
        PermissionFactory(
            RoleLevel.CLIENT_SUPER_ADMIN.value,
            RoleLevel.CLOUD_SUPER_ADMIN.value,
            RoleLevel.ADMIN.value,
        ),
    )

    def get(self, request, equipment_id, point_id):
        point = MeasurePoint.objects.get(equipment_id=equipment_id, id=point_id)
        return BaseResponse(data=point.to_dict())

    def put(self, request, equipment_id, point_id):
        user = request.user
        data, context = self.get_validated_data(
            UpdatePointSerializer, equipment_id=equipment_id, point_id=point_id
        )
        logger.info(f"{user.username} request update point with {data=}")
        measure_name = data.get("measure_name")
        measure_type = data.get("measure_type")
        sensor_number = data.get("sensor_number")
        remarks = data.get("remarks")
        enable_block_alarm = data["enable_block_alarm"]
        enable_alarm_restraint = data["enable_alarm_restraint"]
        alarm_restraint_info = data.get("alarm_restraint_info", DEFAULT_ALARM_RESTRAINT_INFO)
        point = context["point"]
        old_sensor_number = point.sensor_number
        changed_sensor_number = context["changed_sensor_number"]
        change_restraint_flag = context["change_restraint_flag"]
        update_fields = {
            "enable_block_alarm": enable_block_alarm,
            "enable_alarm_restraint": enable_alarm_restraint,
            "alarm_restraint_info": alarm_restraint_info,
        }
        if change_restraint_flag:
            update_fields["restraint_flag"] = False
            update_fields["restraint_deadline"] = None
        if measure_name:
            update_fields["measure_name"] = measure_name
        if measure_type:
            # measure_type can't change
            update_fields["measure_type"] = measure_type
        if sensor_number:
            update_fields["sensor_number"] = sensor_number
        if remarks:
            update_fields["remarks"] = remarks
        if changed_sensor_number:
            # set those fields to default values
            update_fields["is_online"] = False
            if point.measure_type == SensorType.mech:
                update_fields["is_online"] = True
            update_fields["acq_period"] = 0
            update_fields["last_received_time"] = None
        if update_fields:
            point.update(**update_fields)
        if changed_sensor_number:
            customer_id = context["customer_id"]
            site_id = context["site_id"]
            SensorConfigService(sensor_number).sensor_config_operations_in_point_update(
                old_sensor_number,
                customer_id,
                site_id,
                equipment_id,
                point_id,
                point.measure_type,
            )
            customer_ids = [customer_id]
            site_ids = [site_id]
            app.send_task(
                "cloud_home.tasks.async_customer_status_statistic", args=(customer_ids,)
            )
            app.send_task(
                "cloud_home.tasks.async_customer_equipment_abnormal_ratio",
                args=(customer_ids,),
            )
            app.send_task(
                "cloud_home.tasks.async_customer_abnormal_count", args=(customer_ids,)
            )
            app.send_task(
                "cloud_home.tasks.async_site_status_statistic",
                args=(site_ids,),
            )
            app.send_task(
                "cloud_home.tasks.async_site_equipment_abnormal_ratio",
                args=(site_ids,),
            )
            app.send_task(
                "cloud_home.tasks.async_site_abnormal_count", args=(site_ids,)
            )
        return BaseResponse(data=update_fields)

    def delete(self, request, equipment_id, point_id):
        user = request.user
        data, _ = self.get_validated_data(DeletePointSerializer, point_id=point_id)
        logger.info(f"{user.username} request delete point: {point_id} with {data=}")
        points = MeasurePoint.objects.filter(id=point_id)
        MeasurePointService.delete_points(points, clear_resource=data["clear_resource"])
        customer_ids = [data["customer_id"]]
        site_ids = [data["site_id"]]
        app.send_task(
            "cloud_home.tasks.async_customer_status_statistic", args=(customer_ids,)
        )
        app.send_task(
            "cloud_home.tasks.async_customer_equipment_abnormal_ratio",
            args=(customer_ids,),
        )
        app.send_task(
            "cloud_home.tasks.async_customer_abnormal_count", args=(customer_ids,)
        )
        app.send_task(
            "cloud_home.tasks.async_site_status_statistic",
            args=(site_ids,),
        )
        app.send_task(
            "cloud_home.tasks.async_site_equipment_abnormal_ratio",
            args=(site_ids,),
        )
        app.send_task("cloud_home.tasks.async_site_abnormal_count", args=(site_ids,))
        return BaseResponse()
