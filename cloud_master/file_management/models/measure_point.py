from cloud.models import CloudDocument
from mongoengine import (
    BooleanField,
    DateTimeField,
    DictField,
    IntField,
    ObjectIdField,
    StringField,
)

from common.const import MAX_LENGTH_NAME, MAX_MESSAGE_LENGTH, AlarmRestraintKeys

DEFAULT_ALARM_RESTRAINT_INFO = {
    AlarmRestraintKeys.ALARM_DURATION.value: 60,
    AlarmRestraintKeys.MAX_ALARM_NUMS.value: 2,
    AlarmRestraintKeys.PUSH_NO_ALARM_MINUTES.value: 1380,
}


class MeasurePoint(CloudDocument):
    measure_name = StringField(required=True, max_length=MAX_LENGTH_NAME)
    measure_type = StringField(required=True, max_length=MAX_LENGTH_NAME)
    sensor_number = StringField()
    remarks = StringField(max_length=MAX_MESSAGE_LENGTH)
    customer_id = ObjectIdField(required=True)
    site_id = ObjectIdField(required=True)
    equipment_id = ObjectIdField(required=True)
    is_online = BooleanField(default=False)
    acq_period = IntField(default=0)
    last_received_time = DateTimeField(null=True)
    enable_block_alarm = BooleanField(default=False)
    enable_alarm_restraint = BooleanField(default=True)
    alarm_restraint_info = DictField(default=DEFAULT_ALARM_RESTRAINT_INFO)
    restraint_flag = BooleanField(default=False)  # 报警抑制标记位
    restraint_deadline = DateTimeField(null=True)  # 报警抑制截止时间

    meta = {
        "indexes": [
            "measure_name",
            "customer_id",
            "site_id",
            "equipment_id",
            "sensor_number",
            "measure_type",
            "create_date",
            "is_online",
        ],
        "index_background": True,
        "collection": "measure_point",
    }

    def __str__(self):
        return "MeasurePoint: {}".format(self.measure_name)

    def __repr__(self):
        return self.__str__()
