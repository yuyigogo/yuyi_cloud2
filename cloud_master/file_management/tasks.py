import logging
from datetime import timedelta

from alarm_management.models.alarm_info import AlarmInfo
from django.utils import timezone
from equipment_management.models.sensor_config import SensorConfig
from file_management.models.measure_point import MeasurePoint
from pymongo import UpdateOne

from common.const import AlarmFlag, AlarmLevel, AlarmType, SensorAlarmDesc, SensorType
from common.scheduler import cloud_task

logger = logging.getLogger(__name__)


@cloud_task
def async_calculate_point_sensor_online():
    """
    1.每5分钟扫描一次；
    2.如果掉线，需要生成对应的告警记录；同时记录最后一次在线时间;
    传感器告警不需要记录公司/站点异常信息，未处理个数，只需要考虑传感器在线率
    :return:
    """
    logger.info(f"start to run async_calculate_point_sensor_online")
    points = MeasurePoint.objects.filter(
        is_online=True, measure_type__nin=SensorType.mech_sensors()
    )
    sensor_ids = points.values_list("sensor_number")
    sensor_id2_client_id = dict(
        SensorConfig.objects.filter(sensor_number__in=sensor_ids).values_list(
            "sensor_number", "client_number"
        )
    )
    bulk_update_operations, new_alarm_infos = [], []
    for p in points:
        try:
            acq_period = p.acq_period
            last_received_time = p.last_received_time
            if last_received_time is None:
                last_received_time = p.create_date
            time_diff_threshold = timedelta(seconds=3 * acq_period)
            current_time = timezone.now()
            time_since_last_received = current_time - last_received_time

            if time_since_last_received > time_diff_threshold:
                bulk_update_operations.append(
                    UpdateOne({"_id": p.id}, {"$set": {"is_online": False}})
                )
                client_number = sensor_id2_client_id.get(p.sensor_number)
                alarm_info_dict = {
                    "sensor_id": p.sensor_number,
                    "sensor_type": p.measure_type,
                    "alarm_flag": AlarmFlag.PUSH.value,
                    "alarm_type": AlarmType.SENSOR_ALARM.value,
                    "alarm_level": AlarmLevel.ALARM.value,
                    "alarm_describe": SensorAlarmDesc.offline.value,
                    "is_online": False,
                    "is_latest": False,  # 传感器报警，默认为false
                    "is_processed": False,
                    "processed_remarks": "",
                    "customer_id": p.customer_id,
                    "site_id": p.site_id,
                    "equipment_id": p.equipment_id,
                    "point_id": p.id,
                    "last_received_time": last_received_time,
                    "create_date": current_time,
                    "update_date": current_time,
                    "client_number": client_number,
                }
                new_alarm_infos.append(alarm_info_dict)
                logger.info(f"calculate {str(p.id)}'s is_online to false with ")
        except Exception as e:
            logger.error(f"async_calculate_point_sensor_online for {str(p.id)} {e=}")
            continue
    if bulk_update_operations:
        p_collection = MeasurePoint._get_collection()
        p_collection.bulk_write(bulk_update_operations, ordered=False)
        logger.info(
            f"success to async_calculate_point_sensor_online with num: {len(bulk_update_operations)}"
        )
    if new_alarm_infos:
        a_collection = AlarmInfo._get_collection()
        a_collection.insert_many(new_alarm_infos, ordered=False)
        logger.info(
            f"Inserted {len(new_alarm_infos)} new AlarmInfos in async_calculate_point_sensor_online"
        )
