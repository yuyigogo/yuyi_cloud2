import logging
from typing import Union

from bson import ObjectId
from file_management.models.measure_point import MeasurePoint

from common.const import SensorType
from common.framework.service import BaseService

logger = logging.getLogger(__name__)


class MeasurePointService(BaseService):
    def __init__(self, equipment_id: Union[str, ObjectId]):
        self.equipment_id = equipment_id

    def create_new_measure_point(self, data: dict) -> MeasurePoint:
        measure_type = data["measure_type"]
        is_online = False
        if measure_type in SensorType.mech_sensors():
            is_online = True

        measure_point = MeasurePoint(
            customer_id=data["customer_id"],
            site_id=data["site_id"],
            equipment_id=self.equipment_id,
            measure_name=data["measure_name"],
            measure_type=data["measure_type"],
            sensor_number=data["sensor_number"],
            remarks=data.get("remarks"),
            is_online=is_online,
        )
        enable_block_alarm = data.get("enable_block_alarm")
        enable_alarm_restraint = data.get("enable_alarm_restraint")
        alarm_restraint_info = data.get("alarm_restraint_info")
        if enable_block_alarm is not None:
            measure_point.enable_block_alarm = enable_block_alarm
        if enable_alarm_restraint is not None:
            measure_point.enable_alarm_restraint = enable_alarm_restraint
            measure_point.alarm_restraint_info = alarm_restraint_info

        measure_point.save()
        return measure_point

    def get_points_for_equipment(self):
        measure_points = MeasurePoint.objects.filter(equipment_id=self.equipment_id)
        return [
            {
                "measure_name": mp.measure_name,
                "measure_type": mp.measure_type,
                "sensor_number": mp.operation_number,
                "remarks": mp.remarks,
                "customer_id": str(mp.customer_id),
                "site_id": str(mp.site_id),
                "equipment_id": str(self.equipment_id),
            }
            for mp in measure_points
        ]
