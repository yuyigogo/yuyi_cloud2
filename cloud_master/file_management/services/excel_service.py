import logging
from datetime import datetime
from typing import Optional

from customer.models.customer import Customer
from file_management.models.electrical_equipment import ElectricalEquipment
from file_management.models.measure_point import MeasurePoint
from rest_framework.status import HTTP_400_BAD_REQUEST
from sites.models.site import Site

from common.const import DATE_FORMAT_EN, DeviceType, SensorType
from common.error_code import StatusCode
from common.framework.exception import APIException, InvalidException
from common.framework.service import BaseService, SensorConfigService
from common.utils.excel_utils import Workbook, data_to_xlsx

logger = logging.getLogger(__name__)


class ExcelService(object):
    def __init__(self, user):
        self.user = user

    def file_import(
        self, excel_file=None, excel_file_name=None, sheet_name=None
    ) -> dict:
        """
        import customer/site/equipment/point/sensor_config from excel
        :param excel_file: excel file object or excel file contents
        :param excel_file_name: excel file name
        :param sheet_name: excel sheet name
        :return: None
        """
        workbook = Workbook.open(excel_file_name=excel_file_name, excel_file=excel_file)
        workbook_data = list(self.read_workbook_data(workbook, sheet_name))
        try:
            assembled_data = [self.assemble_excel_data(data) for data in workbook_data]
        except Exception:
            raise ExcelException("Excel格式错误！or 解析Excel错误！")
        return self.save_workbook_data(assembled_data)

    def save_workbook_data(self, workbook_data: list) -> dict:
        if not workbook_data:
            raise InvalidException(
                "该文件是空文件",
                code=StatusCode.IMPORT_EXCEL_IS_EMPTY.value,
            )
        success_num, failed_num = 0, 0
        error_msgs = []
        for index, data_list in enumerate(workbook_data):
            try:
                if self.user.is_normal_admin():
                    customer_id = str(self.user.customer)
                else:
                    customer_id = self.crete_or_update_customer(data_list[0])
                site_id = self.crete_or_update_site(data_list[1], customer_id)
                equipment_id = self.crete_or_update_equipment(
                    data_list[2], site_id, customer_id
                )
                point = self.crete_or_update_point(
                    data_list[3], equipment_id, site_id, customer_id
                )
                if point:
                    SensorConfigService(
                        point.sensor_number
                    ).sensor_config_operations_in_point_creation(
                        customer_id,
                        site_id,
                        equipment_id,
                        str(point.pk),
                        point.measure_type,
                    )
                success_num += 1
            except ExcelException as e:
                error_msg = e.msg
                failed_num += 1
                error_msgs.append(f"第{index + 1}条导入错误, 错误详情:{error_msg}")
            except Exception as e:
                error_msg = str(e)
                failed_num += 1
                error_msgs.append(f"第{index + 1}条导入错误, 错误详情:{error_msg}")
        return {
            "total": len(workbook_data),
            "success_num": success_num,
            "failed_num": failed_num,
            "error_msgs": error_msgs,
        }

    @classmethod
    def read_workbook_data(cls, workbook, sheet_name=None):
        for column in workbook.get_columns(
            sheet_name=sheet_name, r_offset=2, c_offset=1
        ):
            result = []
            for value in column:
                if isinstance(value, datetime):
                    value = value.strftime(DATE_FORMAT_EN)
                result.append(value)
            if any(value.strip() != "" for value in result):
                yield result

    @classmethod
    def assemble_excel_data(cls, data: list):
        customer = {
            "name": data[0],
            "administrative_division": {
                "province": data[1],
                "city": data[2],
                "region": data[3],
            },
            "remarks": data[4],
        }
        o_site_location = data[10].replace(" ", "").split(",")
        if len(o_site_location) == 1:
            o_site_location = data[10].replace(" ", "").split("，")
        o_site_location = [float(i) for i in o_site_location]
        site = {
            "name": data[5],
            "voltage_level": data[6],
            "administrative_division": {
                "province": data[7],
                "city": data[8],
                "region": data[9],
            },
            "site_location": o_site_location,
            "remarks": data[11],
        }
        equipment = {
            "device_name": data[12],
            "device_type": data[13],
            "voltage_level": data[14],
            "operation_number": data[15],
            "asset_number": data[16],
            "device_model": data[17],
            "factory_number": data[18],
            "remarks": data[19],
        }
        point = {
            "measure_name": data[20],
            "sensor_number": data[21],
            "measure_type": data[22],
            "remarks": data[23],
        }
        return [customer, site, equipment, point]

    @classmethod
    def crete_or_update_customer(cls, customer_dict: dict) -> str:
        customer = Customer.objects(name=customer_dict["name"]).first()
        if customer:
            customer.update(**customer_dict)
        else:
            customer = Customer(**customer_dict)
            customer.save()
        return str(customer.pk)

    @classmethod
    def crete_or_update_site(cls, site_dict: dict, customer_id: str) -> str:
        site = Site.objects(customer=customer_id, name=site_dict["name"]).first()
        site_dict.update({"customer": customer_id})
        if site:
            site.update(**site_dict)
        else:
            site = Site(**site_dict)
            site.save()
        return str(site.pk)

    @classmethod
    def crete_or_update_equipment(
        cls, equipment_dict: dict, site_id: str, customer_id: str
    ) -> str:
        equipment = ElectricalEquipment.objects(
            site_id=site_id, device_name=equipment_dict["device_name"]
        ).first()
        equipment_dict.update({"site_id": site_id, "customer_id": customer_id})
        if equipment:
            equipment.update(**equipment_dict)
        else:
            device_type = equipment_dict["device_type"]
            if device_type not in DeviceType.values():
                logger.error(f"invalid {device_type=} in excel import!")
                raise ExcelException("设备类型错误！")
            equipment = ElectricalEquipment(**equipment_dict)
            equipment.save()
        return str(equipment.pk)

    @classmethod
    def crete_or_update_point(
        cls, point_dict: dict, equipment_id: str, site_id: str, customer_id: str
    ) -> MeasurePoint:
        sensor_number = point_dict["sensor_number"]
        point = MeasurePoint.objects(sensor_number=sensor_number).first()

        if point:
            pre_info = [
                str(point.customer_id),
                str(point.site_id),
                str(point.equipment_id),
                point.measure_name,
            ]
            new_info = [customer_id, site_id, equipment_id, point_dict["measure_name"]]
            if not cls.is_same_point(pre_info, new_info):
                logger.error(f"the {sensor_number=} has already bound with {pre_info=}")
                raise ExcelException(f"{sensor_number}已经绑定过!")
            if point.measure_type != point_dict["measure_type"]:
                logger.error(f"the exited point can't change its measure_type!")
                raise ExcelException("不允许改变已存在的测点类型!")
            else:
                point.update(**point_dict)
        else:
            measure_type = point_dict["measure_type"]
            if measure_type not in SensorType.values():
                logger.error(f"invalid {measure_type=} in excel import!")
                raise ExcelException("无效的测点类型!")
            point_dict.update(
                {
                    "equipment_id": equipment_id,
                    "site_id": site_id,
                    "customer_id": customer_id,
                }
            )
            point = MeasurePoint(**point_dict)
            point.save()
        return point

    @classmethod
    def is_same_point(cls, pre_list: list, new_list: list) -> bool:
        pre_str = "".join(pre_list)
        new_str = "".join(new_list)
        return pre_str == new_str


class FileExportService(BaseService):
    def __init__(self, customer: Customer, site: Optional[Site]):
        self.customer = customer
        self.site = site

    def customer_values(self) -> list:
        return [
            self.customer.name,
            self.customer.administrative_division["province"],
            self.customer.administrative_division["city"],
            self.customer.administrative_division["region"],
            self.customer.remarks,
        ]

    @classmethod
    def site_values(cls, site: Site) -> list:
        return [
            site.name,
            site.voltage_level,
            site.administrative_division["province"],
            site.administrative_division["city"],
            site.administrative_division["region"],
            f"{site.site_location[0]}, {site.site_location[1]}",
            site.remarks,
        ]

    @classmethod
    def equipment_values(cls, equipment: ElectricalEquipment) -> list:
        return [
            equipment.device_name,
            equipment.device_type,
            equipment.voltage_level,
            equipment.operation_number,
            equipment.asset_number,
            equipment.device_model,
            equipment.factory_number,
            equipment.remarks,
        ]

    @classmethod
    def point_values(cls, point: MeasurePoint) -> list:
        return [
            point.measure_name,
            point.sensor_number,
            point.measure_type,
            point.remarks,
        ]

    def get_excel_body(self) -> list:
        customer_value = self.customer_values()
        if self.site:
            site_id = str(self.site.pk)
            site_ids = [site_id]
            site_info = {site_id: self.site_values(self.site)}
        else:
            sites = Site.objects.filter(customer=self.customer.pk)
            site_info = {str(site.id): self.site_values(site) for site in sites}
            site_ids = site_info.keys()
        equipments = ElectricalEquipment.objects.filter(site_id__in=site_ids)
        equipment_info, equipment_id_site_id, equipment_ids = dict(), dict(), []
        for e in equipments:
            e_id = str(e.id)
            equipment_info[e_id] = self.equipment_values(e)
            equipment_id_site_id[e_id] = str(e.site_id)
            equipment_ids.append(e_id)
        points = MeasurePoint.objects.filter(equipment_id__in=equipment_ids)
        excel_body = []
        for p in points:
            equipment_id = str(p.equipment_id)
            p_values = self.point_values(p)
            e_values = equipment_info.get(equipment_id, [])
            site_id = equipment_id_site_id.get(equipment_id)
            s_values = site_info.get(site_id)
            excel_body.append(customer_value + s_values + e_values + p_values)
        return excel_body

    @classmethod
    def get_excel_header(cls) -> list:
        second_header_data = [
            "*公司名称",
            "行政区域-省",
            "行政区域-市",
            "行政区域-区/县",
            "备注",
            "*站点名称",
            "*电压等级",
            "行政区域-省",
            "行政区域-市",
            "行政区域-区/县",
            "站点坐标",
            "备注",
            "*设备名称",
            "*设备类型",
            "*电压等级",
            "运行编号",
            "资产编号",
            "设备编号",
            "出厂编号",
            "备注",
            "*测点名称",
            "*传感器编号",
            "*测点类型",
            "备注",
        ]
        return [second_header_data]

    def get_excel_data(self) -> list:
        excel_data = self.get_excel_header()
        excel_data += self.get_excel_body()
        return excel_data

    def get_excel_content(self) -> bytes:
        excel_data = self.get_excel_data()
        return data_to_xlsx(excel_data, color_lines=[1])


class ExcelException(APIException):
    status_code = HTTP_400_BAD_REQUEST
