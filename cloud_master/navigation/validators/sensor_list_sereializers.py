import logging

from bson import ObjectId
from cloud.models import bson_to_dict
from cloud.settings import MONGO_CLIENT
from sensor_data.models.mech_motor import MechMotor, MechMotorSep

from common.const import SensorType
from common.framework.exception import APIException
from common.framework.serializer import BaseSerializer

logger = logging.getLogger(__name__)


class SensorDetailsSerializer(BaseSerializer):
    def validate(self, data):
        sensor_obj_id = self.context["pk"]
        sensor_type = self.context["sensor_type"].upper()
        if sensor_type not in SensorType.values():
            raise APIException(f"Invalid {sensor_type=}")
        try:
            my_col = MONGO_CLIENT[sensor_type]
            sensor_data = my_col.find_one({"_id": ObjectId(sensor_obj_id)})
        except Exception as e:
            logger.exception(f"get sensor_obj failed with {e=}")
            raise APIException(f"Invalid {sensor_obj_id=}-{sensor_type=}")
        self.context["sensor_obj_dict"] = bson_to_dict(sensor_data)
        return data


class MotorDetailsSerializer(BaseSerializer):
    def validate(self, data):
        sensor_number = self.context["sensor_number"]
        mech_motor = (
            MechMotor.objects.filter(
                sensor_id=sensor_number, sensor_type=SensorType.mech.value
            )
            .order_by("-create_date")
            .first()
        )
        data["mech_motor"] = mech_motor
        return data


class NearestMotorSerializer(BaseSerializer):
    def validate(self, data):
        mech_id = self.context["mech_id"]
        try:
            my_col = MONGO_CLIENT[SensorType.mech.value]
            mech_coil = my_col.find_one({"_id": ObjectId(mech_id)})
        except Exception as e:
            logger.exception(f"get mech_coil failed with {e=}")
            raise APIException(f"Invalid {mech_id=}!")
        data["sensor_id"] = mech_coil["sensor_id"]
        data["create_date"] = mech_coil["create_date"]
        return data


class MotorSepDetailsSerializer(BaseSerializer):
    def validate(self, data):
        sensor_number = self.context["sensor_number"]
        mech_motor_sep = (
            MechMotorSep.objects.filter(
                sensor_id=sensor_number, sensor_type=SensorType.mech_sep.value
            )
            .order_by("-create_date")
            .first()
        )
        data["mech_motor_sep"] = mech_motor_sep
        return data


class NearestMotorSepSerializer(BaseSerializer):
    def validate(self, data):
        mech_sep_id = self.context["mech_sep_id"]
        try:
            my_col_sep = MONGO_CLIENT[SensorType.mech_sep.value]
            mech_coil_sep = my_col_sep.find_one({"_id": ObjectId(mech_sep_id)})
        except Exception as e:
            logger.exception(f"get my_col_sep failed with {e=}")
            raise APIException(f"Invalid {mech_sep_id=}!")
        data["sensor_id"] = mech_coil_sep["sensor_id"]
        data["create_date"] = mech_coil_sep["create_date"]
        return data
