import logging
from collections import OrderedDict, defaultdict
from typing import Optional

from alarm_management.models.alarm_info import AlarmInfo
from cloud.models import bson_to_dict
from cloud.settings import MONGO_CLIENT
from file_management.models.electrical_equipment import ElectricalEquipment
from file_management.models.measure_point import MeasurePoint
from mongoengine import Q, QuerySet

from common.const import AlarmType, SensorType
from common.framework.service import BaseService
from common.utils import get_objects_pagination

logger = logging.getLogger(__name__)


class SiteDetailsService(BaseService):
    @classmethod
    def get_site_details_from_site_or_equipment(
        cls,
        page: int,
        limit: int,
        site_id: Optional[str] = None,
        equipment_id: Optional[str] = None,
        point_name: Optional[str] = None,
        alarm_level: Optional[int] = None,
        is_online: Optional[bool] = None,
        sensor_type: Optional[str] = None,
    ) -> tuple:
        assert (
            site_id or equipment_id
        ), "should have one value in site_id or equipment_id"
        if site_id:
            point_query = Q(site_id=site_id)
        else:
            point_query = Q(equipment_id=equipment_id)
        if point_name:
            point_query &= Q(measure_name=point_name)
        if sensor_type:
            point_query &= Q(measure_type=sensor_type)
        if is_online is not None:
            point_query &= Q(is_online=is_online)
        points = MeasurePoint.objects.filter(point_query)
        m_point_ids = set(points.values_list("id"))
        alarm_info_query = None
        if alarm_level is not None:
            alarm_info_query = Q(
                is_latest=True,
                alarm_type=AlarmType.POINT_ALARM.value,
                point_id__in=m_point_ids,
            )
        if alarm_level is not None:
            alarm_info_query &= Q(alarm_level=alarm_level)
        # if is_online is not None:
        #     alarm_info_query &= Q(is_online=is_online)
        if alarm_info_query:
            a_point_ids = AlarmInfo.objects.filter(alarm_info_query).values_list(
                "point_id"
            )
            points = points.filter(id__in=a_point_ids)
        points = points.order_by("equipment_id")
        total = points.count()
        point_infos_by_page = get_objects_pagination(page, limit, points)
        data = cls.assemble_site_infos(point_infos_by_page)
        return total, data

    @classmethod
    def assemble_site_infos(cls, points: QuerySet) -> list:
        equipment_ids = set()
        sensor_type2_sensor_numbers = defaultdict(list)
        point_id2_data_dict = OrderedDict()
        for p in points:
            point_id = str(p.pk)
            sensor_type = p.measure_type
            sensor_number = p.sensor_number
            equipment_id = str(p.equipment_id)
            equipment_ids.add(equipment_id)
            if sensor_number is not None:
                sensor_type2_sensor_numbers[sensor_type].append(sensor_number)
            point_id2_data_dict[point_id] = {
                "point_id": point_id,
                "point_name": p.measure_name,
                "equipment_id": equipment_id,
                "customer_id": str(p.customer_id),
                "site_id": str(p.site_id),
                "sensor_number": p.sensor_number,
                "sensor_type": p.measure_type,
                "is_online": p.is_online,
            }
        equipment_id2_name = cls.get_equipment_info(equipment_ids)
        point_id2_sensor_data = cls.get_sensor_data(sensor_type2_sensor_numbers)
        data = []
        for point_id, data_dict in point_id2_data_dict.items():
            data_dict["has_sensor_data"] = False
            equipment_id = data_dict["equipment_id"]
            data_dict["equipment_name"] = equipment_id2_name.get(equipment_id, "")
            sensor_info = point_id2_sensor_data.get(point_id, {})
            if sensor_info:
                data_dict.update(sensor_info)
                data_dict["has_sensor_data"] = True
            data.append(data_dict)
        return data

    @classmethod
    def get_equipment_info(cls, equipment_ids: set) -> dict:
        equipments = ElectricalEquipment.objects(id__in=equipment_ids)
        return {str(e.pk): e.device_name for e in equipments}

    @classmethod
    def get_sensor_data(cls, sensor_type2_sensor_numbers: dict) -> dict:
        list_data = []
        for sensor_type, sensor_ids in sensor_type2_sensor_numbers.items():
            not_display_fields = None
            my_col = MONGO_CLIENT[sensor_type]
            raw_query = {"sensor_id": {"$in": sensor_ids}, "is_latest": True}
            if sensor_type in SensorType.partial_discharge_sensors():
                not_display_fields = {"prps": 0, "prpd": 0}
            elif sensor_type == SensorType.mech.value:
                not_display_fields = {
                    "Mech_On_Coil_I": 0,
                    "Mech_Off_Coil_I": 0,
                    "Mech_CT_A_I": 0,
                    "Mech_CT_B_I": 0,
                    "Mech_CT_C_I": 0,
                    "Mech_DIS_I": 0,
                }
            elif sensor_type == SensorType.mech_sep.value:
                not_display_fields = {
                    "Mech_On_Coil_A_I": 0,
                    "Mech_Off_Coil_A_0_I": 0,
                    "Mech_Off_Coil_A_1_I": 0,
                    "Mech_CT_A_I": 0,
                    "Mech_DIS_A_I": 0,
                    "Mech_Off_Coil_B_0_I": 0,
                    "Mech_Off_Coil_B_1_I": 0,
                    "Mech_On_Coil_B_I": 0,
                    "Mech_CT_B_I": 0,
                    "Mech_DIS_B_I": 0,
                    "Mech_Off_Coil_C_0_I": 0,
                    "Mech_Off_Coil_C_1_I": 0,
                    "Mech_On_Coil_C_I": 0,
                    "Mech_CT_C_I": 0,
                    "Mech_DIS_C_I": 0,
                    "Mech_Switch_A_St": 0,
                    "Mech_Switch_B_St": 0,
                    "Mech_Switch_C_St": 0
                }
            sensors = my_col.find(raw_query, not_display_fields)
            list_data.extend(sensors)
        point_id2_sensor_info = {}
        for sensor in list_data:
            sensor_obj_id = sensor["_id"]
            sensor_type = sensor["sensor_type"]
            point_id = sensor["point_id"]
            info = {
                "sensor_data_id": str(sensor_obj_id),
                "sensor_number": sensor["sensor_id"],
                "sensor_type": sensor_type,
                "model_key": sensor["model_key"],
                "unit": sensor.get("unit"),
                "create_date": bson_to_dict(sensor["create_date"]),
                "alarm_level": sensor["alarm_level"],
                "alarm_describe": sensor["alarm_describe"],
                "upload_interval": sensor.get("upload_interval", ""),
                # work status fields
                # "is_online": sensor["is_online"],
                "battery": sensor.get("battery", ""),
                "rssi": sensor.get("rssi", ""),
                "snr": sensor.get("snr", ""),
                "alarm_type": sensor.get("alarm_type"),
            }
            if sensor_type in SensorType.single_value_sensors():
                info["character_value"] = sensor["amplitude"]
            elif sensor_type in SensorType.partial_discharge_sensors():
                info["PDType"] = sensor["PDType"]
                info["character_value"] = sensor["peak"]
            elif sensor_type == SensorType.temp:
                info["character_value"] = sensor["T"]
            elif sensor_type == SensorType.mech:
                # mech
                info["character_value"] = (
                    sensor.get("Mech_Results", {}).get("CoilA", {}).get("ON_OFF_STATE")
                )
            elif sensor_type == SensorType.mech_sep:
                info["character_value"] = (
                    sensor.get("Mech_Results", {}).get("Coil_ALL", {}).get("ON_OFF_STATE")
                )
            point_id2_sensor_info[point_id] = info
        return point_id2_sensor_info
