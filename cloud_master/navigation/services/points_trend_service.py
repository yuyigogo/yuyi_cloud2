import logging
from datetime import datetime, timedelta

from bson import SON
from cloud.models import bson_to_dict
from cloud.settings import MONGO_CLIENT
from file_management.models.electrical_equipment import ElectricalEquipment
from file_management.models.measure_point import MeasurePoint

from common.const import SensorType
from common.framework.service import BaseService
logger = logging.getLogger(__name__)


class PointsTrendService(BaseService):
    @classmethod
    def get_points_trend_data(
        cls, point_ids: list, start_date: str, end_date: str
    ) -> list:
        start_date = datetime.strptime(start_date, "%Y-%m-%d %H:%M:%S")
        end_date = datetime.strptime(end_date, "%Y-%m-%d %H:%M:%S")
        points = MeasurePoint.objects.only("measure_type", "measure_name").filter(
            pk__in=point_ids
        )
        point_to_sensor = {
            str(point.pk): (point.measure_type, point.measure_name) for point in points
        }
        data = []
        for (
            point_id,
            (sensor_type, measure_name),
        ) in point_to_sensor.items():
            mongo_col = MONGO_CLIENT[sensor_type]
            not_display_fields = None
            if sensor_type in SensorType.partial_discharge_sensors():
                not_display_fields = {"prps": 0, "prpd": 0}
            sensors = mongo_col.find(
                {
                    "point_id": point_id,
                    "create_date": {"$gte": start_date, "$lte": end_date},
                },
                not_display_fields,
            )
            if sensors.count():
                data.append(
                    {
                        "point_id": point_id,
                        "measure_type": sensor_type,
                        "measure_name": measure_name,
                        "point_data": [bson_to_dict(sensor) for sensor in sensors],
                    }
                )
        return data

    @classmethod
    def get_equipment_names(cls, equipment_ids: set) -> dict:
        return dict(
            ElectricalEquipment.objects.filter(id__in=equipment_ids).values_list(
                "id", "device_name"
            )
        )

    @classmethod
    def get_point_graph_data_on_certain_time(
        cls, point_ids: list, start_date: str
    ) -> list:
        start_date = datetime.strptime(start_date, "%Y-%m-%d %H:%M:%S")
        points = MeasurePoint.objects.only(
            "measure_type", "measure_name", "equipment_id"
        ).filter(id__in=point_ids)
        equipment_ids = set(points.values_list("equipment_id"))
        equipment_id_names = cls.get_equipment_names(equipment_ids)
        data = []
        for point in points:
            point_id = str(point.pk)
            equipment_id = point.equipment_id
            equipment_name = equipment_id_names.get(equipment_id, "")
            sensor_type = point.measure_type
            s = cls.get_nearest_data(point_id, sensor_type, start_date)
            if s:
                sensor_info = bson_to_dict(s)
                sensor_info["equipment_name"] = equipment_name
                sensor_info["point_name"] = point.measure_name
                data.append(sensor_info)
        return data

    @classmethod
    def get_nearest_data(cls, point_id, sensor_type, start_date):
        mongo_col = MONGO_CLIENT[sensor_type]
        pipeline = [
            {
                "$match": {
                    "point_id": point_id,
                    "create_date": {
                        "$gte": start_date - timedelta(days=1),
                        "$lt": start_date + timedelta(days=1),
                    },
                }
            },
            {
                "$addFields": {
                    "time_difference": {
                        "$abs": {
                            "$subtract": [
                                start_date,
                                "$create_date",
                            ]
                        }
                    }
                }
            },
            {"$sort": SON([("time_difference", 1), ("create_date", 1)])},
            {"$limit": 1},
        ]

        result = list(mongo_col.aggregate(pipeline))
        return result[0] if result else None
