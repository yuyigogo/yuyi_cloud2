from django.urls import path, re_path
from navigation.apis.equipment_navigation_apis import CustomerTreesView
from navigation.apis.gateway_navigation_apis import GatewayTreesView
from navigation.apis.points_trend_apis import PointsGraphView, PointsTrendView
from navigation.apis.sensor_list_apis import (
    MotorDetailsView,
    MotorSepDetailsView,
    NearestMotorSepView,
    NearestMotorView,
    SensorDetailsView,
)
from navigation.apis.site_details_apis import EquipmentDetailsView, SiteDetailsView

urlpatterns = [
    re_path(
        r"^equipments/(?P<equipment_id>[a-zA-Z0-9]+)/sensor_info/$",
        EquipmentDetailsView.as_view(),
        name="sensor_info_in_equipment",
    ),
    re_path(
        r"^sites/(?P<site_id>[a-zA-Z0-9]+)/sensor_info/$",
        SiteDetailsView.as_view(),
        name="sensor_info_in_site",
    ),
    # re_path(
    #     r"^customers/(?P<customer_id>[a-zA-Z0-9]+)/sensor_info/$",
    #     CustomerSensorsView.as_view(),
    #     name="sensor_info_in_customer",
    # ),
    re_path(
        r"^sensor/(?P<pk>[a-zA-Z0-9]+)/sensor_type/(?P<sensor_type>[a-zA-Z]+)/$",
        SensorDetailsView.as_view(),
        name="sensor_details",
    ),
    re_path(
        r"^mech-motor/(?P<sensor_number>[a-zA-Z0-9]+)/details/$",
        MotorDetailsView.as_view(),
        name="motor_details",
    ),
    re_path(
        r"^mech-motor-sep/(?P<sensor_number>[a-zA-Z0-9]+)/details/$",
        MotorSepDetailsView.as_view(),
        name="motor_seq_details",
    ),
    path("customer-trees/", CustomerTreesView.as_view(), name="customer_trees_info"),
    path(
        "gateway-trees/", GatewayTreesView.as_view(), name="customer_gateway_trees_info"
    ),
    re_path(r"^points_trend/$", PointsTrendView.as_view(), name="points_trend"),
    re_path(
        r"^points-graph/$",
        PointsGraphView.as_view(),
        name="points_graph",
    ),
    re_path(
        r"^nearest-motor/(?P<mech_id>[a-zA-Z0-9]+)/$",
        NearestMotorView.as_view(),
        name="nearest_motor",
    ),
    re_path(
        r"^nearest-motor_sep/(?P<mech_sep_id>[a-zA-Z0-9]+)/$",
        NearestMotorSepView.as_view(),
        name="nearest_motor_sep",
    ),
]
