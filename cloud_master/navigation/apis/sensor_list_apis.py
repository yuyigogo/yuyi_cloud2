import datetime
import logging

from cloud.models import bson_to_dict
from file_management.models.electrical_equipment import ElectricalEquipment
from file_management.models.measure_point import MeasurePoint
from mongoengine import Q
from navigation.validators.sensor_list_sereializers import (
    MotorDetailsSerializer,
    MotorSepDetailsSerializer,
    NearestMotorSepSerializer,
    NearestMotorSerializer,
    SensorDetailsSerializer,
)
from sensor_data.models.mech_motor import MechMotor

from common.framework.response import BaseResponse
from common.framework.view import BaseView

logger = logging.getLogger(__name__)


class CustomerSensorsView(BaseView):
    def get(self, request, customer_id):
        """
        all points(sensors) in the corresponding customer；
        公司下所有最新一条传感器的数据(暂时没用，最大粒度在站点)
        :param request:
        :param customer_id:
        :return:
        """
        # try:
        #     customer = Customer.objects.get(id=customer_id)
        # except DoesNotExist:
        #     logger.info(f"invalid {customer_id=}")
        #     return BaseResponse(status_code=HTTP_404_NOT_FOUND)
        # customer_sensors = SiteNavigationService.get_all_sensors_in_customer(customer)
        # return BaseResponse(data=customer_sensors)
        pass


class SensorDetailsView(BaseView):
    @classmethod
    def get_equipment_name(cls, equipment_id: str) -> str:
        return ElectricalEquipment.objects.get(pk=equipment_id).device_name

    @classmethod
    def get_point_name(cls, point_id: str) -> str:
        return MeasurePoint.objects.get(pk=point_id).measure_name

    def get(self, request, pk, sensor_type):
        _, context = self.get_validated_data(
            SensorDetailsSerializer, pk=pk, sensor_type=sensor_type
        )
        logger.info(
            f"{request.user.username} request sensor details for {pk=}, {sensor_type=}"
        )
        sensor_obj_dict = context["sensor_obj_dict"]
        equipment_id = sensor_obj_dict["equipment_id"]
        point_id = sensor_obj_dict["point_id"]
        sensor_obj_dict["equipment_name"] = self.get_equipment_name(equipment_id)
        sensor_obj_dict["point_name"] = self.get_point_name(point_id)
        return BaseResponse(data=sensor_obj_dict)


class MotorDetailsView(BaseView):
    def get(self, request, sensor_number):
        data, _ = self.get_validated_data(
            MotorDetailsSerializer, sensor_number=sensor_number
        )
        mech_motor = data["mech_motor"]
        data = {}
        if mech_motor:
            data = bson_to_dict(mech_motor)
        return BaseResponse(data=data)


class NearestMotorView(BaseView):
    """电机电流某个时间点最近的一条数据"""

    def get(self, request, mech_id):
        data, _ = self.get_validated_data(NearestMotorSerializer, mech_id=mech_id)
        sensor_id = data["sensor_id"]
        create_date = data["create_date"]
        motor_query = Q(sensor_id=sensor_id)
        start_date = create_date - datetime.timedelta(minutes=1)
        end_date = create_date + datetime.timedelta(minutes=1)
        motor_query &= Q(create_date__gte=start_date, create_date__lte=end_date)
        mech_motor = (
            MechMotor.objects.filter(motor_query).order_by("-create_date").first()
        )
        data = mech_motor.to_dict() if mech_motor else dict()
        return BaseResponse(data=data)


class MotorSepDetailsView(BaseView):
    def get(self, request, sensor_number):
        data, _ = self.get_validated_data(
            MotorSepDetailsSerializer, sensor_number=sensor_number
        )
        mech_motor_sep = data["mech_motor_sep"]
        data = {}
        if mech_motor_sep:
            data = bson_to_dict(mech_motor_sep)
        return BaseResponse(data=data)


class NearestMotorSepView(BaseView):
    """机械共体电机电流某个时间点最近的一条数据"""

    def get(self, request, mech_sep_id):
        data, _ = self.get_validated_data(
            NearestMotorSepSerializer, mech_id=mech_sep_id
        )
        sensor_id = data["sensor_id"]
        create_date = data["create_date"]
        motor_sep_query = Q(sensor_id=sensor_id)
        start_date = create_date - datetime.timedelta(minutes=1)
        end_date = create_date + datetime.timedelta(minutes=1)
        motor_sep_query &= Q(create_date__gte=start_date, create_date__lte=end_date)
        motor_sep = (
            MechMotor.objects.filter(motor_sep_query).order_by("-create_date").first()
        )
        data = motor_sep.to_dict() if motor_sep else dict()
        return BaseResponse(data=data)
