import logging
from typing import Optional

from rest_framework.status import HTTP_404_NOT_FOUND

from common.framework.response import BaseResponse
from common.framework.view import BaseView
from file_management.models.electrical_equipment import ElectricalEquipment
from navigation.services.sensor_list_service import SiteDetailsService
from navigation.validators.site_details_sereializers import SiteDetailsSerializer
from sites.models.site import Site

logger = logging.getLogger(__name__)


class DetailsBaseView(BaseView):
    @classmethod
    def validate_site_or_equipment(
        cls, site_id: Optional[str] = None, equipment_id: Optional[str] = None
    ) -> int:
        assert (
            site_id or equipment_id
        ), "should have one value in site_id or equipment_id "
        if site_id:
            return Site.objects(id=site_id).count()
        else:
            return ElectricalEquipment.objects(id=equipment_id).count()

    @classmethod
    def _get_site_details_from_site_or_equipment(
        cls,
        data: dict,
        site_id: Optional[str] = None,
        equipment_id: Optional[str] = None,
    ) -> tuple:
        page = data.get("page", 1)
        limit = data.get("limit", 10)
        point_name = data.get("point_name")
        alarm_level = data.get("alarm_level")
        is_online = data.get("is_online")
        sensor_type = data.get("sensor_type")
        total, data = SiteDetailsService.get_site_details_from_site_or_equipment(
            page,
            limit,
            site_id=site_id,
            equipment_id=equipment_id,
            point_name=point_name,
            alarm_level=alarm_level,
            is_online=is_online,
            sensor_type=sensor_type,
        )
        return total, data


class SiteDetailsView(DetailsBaseView):
    def get(self, request, site_id):
        """
        all points(sensors) in the corresponding site；
        站点下所有最新一条传感器的数据详情
        """
        if not self.validate_site_or_equipment(site_id=site_id):
            logger.info(f"invalid {site_id=}")
            return BaseResponse(status_code=HTTP_404_NOT_FOUND)
        data, _ = self.get_validated_data(SiteDetailsSerializer, site_id=site_id)
        logger.info(f"{request.user.username} request list sensors in {site_id=}")
        total, site_sensor_infos = self._get_site_details_from_site_or_equipment(
            data, site_id=site_id
        )
        return BaseResponse(data={"sensor_list": site_sensor_infos, "total": total})


class EquipmentDetailsView(DetailsBaseView):
    def get(self, request, equipment_id):
        """
        all points(sensors) in the corresponding equipment；
        设备下所有最新一条传感器的数据
        """
        if not self.validate_site_or_equipment(equipment_id=equipment_id):
            logger.info(f"invalid {equipment_id=}")
            return BaseResponse(status_code=HTTP_404_NOT_FOUND)
        data, _ = self.get_validated_data(
            SiteDetailsSerializer, equipment_id=equipment_id
        )
        total, equipment_sensor_infos = self._get_site_details_from_site_or_equipment(
            data, equipment_id=equipment_id
        )
        return BaseResponse(
            data={"sensor_list": equipment_sensor_infos, "total": total}
        )
