import logging

from cloud.models import CloudDocument
from mongoengine import DictField, ListField, ObjectIdField, StringField

from common.const import MAX_LENGTH_NAME, MAX_MESSAGE_LENGTH

logger = logging.getLogger(__name__)


class Site(CloudDocument):
    name = StringField(unique=True, max_length=MAX_LENGTH_NAME)
    administrative_division = DictField(required=True)
    voltage_level = StringField(required=True)
    site_location = ListField()
    remarks = StringField(max_length=MAX_MESSAGE_LENGTH)
    customer = ObjectIdField(required=True)

    meta = {
        "indexes": ["name", "customer"],
        "index_background": True,
        "collection": "site",
    }

    def __str__(self):
        return "SiteModel: {}".format(self.name)

    def __repr__(self):
        return self.__str__()

    @property
    def id(self):
        return self._id

    # def delete(self, is_delete_customer_resource=True, **kwargs):
    #     from cloud_home.models.status_statistics import SAbnormalRatio, SStatusStatistic
    #
    #     if is_delete_customer_resource:
    #         print("1"*100)
    #         SStatusStatistic.objects.filter(site_id=self.id).delete()
    #         SAbnormalRatio.objects.filter(site_id=self.id).delete()
    #         delete_keys = [
    #             f"{SITE_UNPROCESSED_NUM}{str(self.id)}"
    #             f"{site_day_abnormal_info}{str(self.id)}",
    #             f"{site_week_abnormal_info}{str(self.id)}",
    #             f"{site_month_abnormal_info}{str(self.id)}",
    #         ]
    #         k1, *k2 = delete_keys
    #         try:
    #             normal_redis.delete(k1, *k2)
    #         except Exception as e:
    #             logger.exception(
    #                 f"delete site resource for {delete_keys=} exception with {e=}"
    #             )
    #     super(Site, self).delete(**kwargs)
