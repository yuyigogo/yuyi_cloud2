import logging

from rest_framework.status import HTTP_201_CREATED

from cloud.celery import app
from sites.services.site_service import SiteService
from sites.validators.sites_serializers import (
    BaseSiteSerializer,
    CreateSiteSerializer,
    DeleteSiteSerializer,
    GetCustomerSitesSerializer,
    UpdateSiteSerializer,
)

from common.const import RoleLevel
from common.framework.permissions import PermissionFactory
from common.framework.response import BaseResponse
from common.framework.view import BaseView

logger = logging.getLogger(__name__)


class CustomerSitesView(BaseView):
    permission_classes = (
        PermissionFactory(
            RoleLevel.CLIENT_SUPER_ADMIN.value,
            RoleLevel.CLOUD_SUPER_ADMIN.value,
            RoleLevel.ADMIN.value,
        ),
    )

    def get(self, request, customer_id):
        user = request.user
        self.get_validated_data(GetCustomerSitesSerializer, customer_id=customer_id)
        sites = SiteService(user, customer_id).get_sites_by_customer_id()
        data = [site.to_dict() for site in sites]
        return BaseResponse(data={"sites": data, "total": sites.count()})

    def post(self, request, customer_id):
        user = request.user
        data, _ = self.get_validated_data(CreateSiteSerializer, customer_id=customer_id)
        logging.info(
            f"{user.username} request create site for {customer_id=} with {data=}"
        )
        service = SiteService(user, customer_id)
        site_location = [float(s) for s in data["site_location"]]
        site = service.create_site(
            name=data["name"],
            administrative_division=data["administrative_division"],
            voltage_level=data["voltage_level"],
            site_location=site_location,
            remarks=data.get("remarks", ""),
        )
        customer_ids = [customer_id]
        site_ids = [str(site.pk)]
        app.send_task(
            "cloud_home.tasks.async_customer_status_statistic", args=(customer_ids,)
        )
        app.send_task(
            "cloud_home.tasks.async_customer_equipment_abnormal_ratio", args=(customer_ids,)
        )
        app.send_task(
            "cloud_home.tasks.async_site_status_statistic", args=(site_ids,),
        )
        app.send_task(
            "cloud_home.tasks.async_site_equipment_abnormal_ratio", args=(site_ids,),
        )
        app.send_task(
            "cloud_home.tasks.async_site_abnormal_count", args=(site_ids,)
        )
        return BaseResponse(data=site.to_dict(), status_code=HTTP_201_CREATED)


class CustomerSiteView(BaseView):
    permission_classes = (
        PermissionFactory(
            RoleLevel.CLIENT_SUPER_ADMIN.value,
            RoleLevel.CLOUD_SUPER_ADMIN.value,
            RoleLevel.ADMIN.value,
        ),
    )

    def get(self, request, customer_id, site_id):
        _, context = self.get_validated_data(
            BaseSiteSerializer, customer_id=customer_id, site_id=site_id
        )
        site = context["site"]
        return BaseResponse(data=site.to_dict())

    def put(self, request, customer_id, site_id):
        user = request.user
        data, context = self.get_validated_data(
            UpdateSiteSerializer, customer_id=customer_id, site_id=site_id
        )
        logger.info(f"{user.username} request update {site_id=} with {data=}")
        name = data.get("name")
        administrative_division = data.get("administrative_division")
        remarks = data.get("remarks")
        voltage_level = data.get("voltage_level")
        site_location = data.get("site_location")
        site = context["site"]
        update_fields = {}
        if name:
            update_fields["name"] = name
        if administrative_division:
            update_fields["administrative_division"] = administrative_division
        if remarks:
            update_fields["remarks"] = remarks
        if voltage_level:
            update_fields["voltage_level"] = voltage_level
        if site_location:
            update_fields["site_location"] = site_location
        if update_fields:
            site.update(**update_fields)
        return BaseResponse(data=update_fields)

    def delete(self, request, customer_id, site_id):
        user = request.user
        data, context = self.get_validated_data(
            DeleteSiteSerializer, customer_id=customer_id, site_id=site_id
        )
        logger.info(
            f"{user.username} request delete {site_id=} in {customer_id=} with {data}"
        )
        site = context["site"]
        clear_resource = data["clear_resource"]
        SiteService.delete_site(site, clear_resource)
        logger.info(f"{user.username} delete {site_id=} successfully!")
        customer_ids = [customer_id]
        app.send_task(
            "cloud_home.tasks.async_customer_status_statistic", args=(customer_ids,)
        )
        app.send_task(
            "cloud_home.tasks.async_customer_equipment_abnormal_ratio", args=(customer_ids,)
        )
        app.send_task(
            "cloud_home.tasks.async_customer_abnormal_count", args=(customer_ids,)
        )
        return BaseResponse()
