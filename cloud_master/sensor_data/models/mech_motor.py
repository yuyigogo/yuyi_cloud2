import logging

from cloud.models import CloudDocument
from mongoengine import DictField, ObjectIdField, StringField

logger = logging.getLogger(__name__)


class MechMotor(CloudDocument):
    sensor_id = StringField(required=True)
    sensor_type = StringField(required=True)
    client_number = StringField(required=True)
    customer_id = ObjectIdField()
    site_id = ObjectIdField()
    equipment_id = ObjectIdField()
    point_id = ObjectIdField()
    Mech_Motor_I = DictField()
    Mech_Results = DictField()
    model_key = StringField()

    meta = {
        "indexes": ["sensor_id", "client_number", "sensor_type"],
        "index_background": True,
        "collection": "mech_motor",
    }

    def __str__(self):
        return "MechMotor: {}-{}".format(self.sensor_type, self.sensor_id)

    def __repr__(self):
        return self.__str__()


class MechMotorSep(CloudDocument):
    """机械特性三相分体-电机电流"""
    sensor_id = StringField(required=True)
    sensor_type = StringField(required=True)
    client_number = StringField(required=True)
    customer_id = ObjectIdField()
    site_id = ObjectIdField()
    equipment_id = ObjectIdField()
    point_id = ObjectIdField()
    Mech_Results = DictField()
    Mech_Motor_A_I = DictField()
    Mech_Motor_B_I = DictField()
    Mech_Motor_C_I = DictField()

    meta = {
        "indexes": ["sensor_id", "client_number", "sensor_type"],
        "index_background": True,
        "collection": "mech_motor_sep",
    }

    def __str__(self):
        return f"MechMotorSep: {self.sensor_type}-{self.sensor_id}"

    def __repr__(self):
        return self.__str__()
