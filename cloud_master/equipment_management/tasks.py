import logging
from collections import defaultdict

from cloud.settings import MONGO_CLIENT
from cloud_ws.ws_group_send import WsGatewaySensorsDataSend
from cloud_ws.ws_uri_code import Uri2C

from common.const import GATEWAY_SENSORS_TOPIC_PREFIX, MODEL_KEY_TO_SENSOR_TYPE
from common.scheduler import cloud_task
from common.storage.redis import normal_redis

logger = logging.getLogger(__name__)


@cloud_task
def deal_with_cached_sensor_in_gateway(client_number: str):
    """
    if the gateway_key exists in redis, means subscribe the topic timeout/failed,
    so need delete the key and notice by ws
    :param client_number:
    :return:
    """
    gateway_key = f"{GATEWAY_SENSORS_TOPIC_PREFIX}{client_number}"
    if normal_redis.exists(gateway_key):
        logger.warning(f"subscribe sensor in gateway timeout with {gateway_key=}")
        normal_redis.delete(gateway_key)
        WsGatewaySensorsDataSend(client_number).ws_send(
            msg="subscribe timeout!", code=Uri2C.S2C_GATEWAY_TIMEOUT_DATA.value
        )


@cloud_task
def delete_data_from_gateway(client_id: str, sensor_ids: list, model_keys: list):
    """
    when delete gateway, should delete alarm_info and sensor_data
    :return:
    """
    logger.info(f"start to run delete_data_from_gateway with {client_id=}")
    delete_filters = {"client_number": client_id, "sensor_id": {"$in": sensor_ids}}
    alarm_info_col = MONGO_CLIENT["alarm_info"]
    alarm_info_col.delete_many(delete_filters)
    logger.info(
        f"delete alarm_infos successfully in delete_data_from_gateway with {client_id=}, {sensor_ids=}"
    )
    sensor_types = [
        MODEL_KEY_TO_SENSOR_TYPE.get(model_key) for model_key in model_keys
    ]
    sensor_id_type = list(zip(sensor_ids, sensor_types))
    sensor_types_info = defaultdict(list)
    for (sensor_id, sensor_type) in sensor_id_type:
        sensor_types_info[sensor_type].append(sensor_id)

    for sensor_type, _sensor_ids in sensor_types_info.items():
        try:
            mongo_col = MONGO_CLIENT[sensor_type]
            mongo_col.delete_many(
                {"client_id": client_id, "sensor_id": {"$in": _sensor_ids}}
            )
            logger.info(
                f"delete {sensor_type=} successfully in delete_data_from_gateway with {client_id=}, {_sensor_ids=}"
            )
        except Exception as e:
            logger.error(
                f"delete sensor data failed with {sensor_type=},{client_id=}, {_sensor_ids=},{e=}",
                exc_info=True,
            )
    logger.info(f"run delete_data_from_gateway successfully with {client_id=}")
