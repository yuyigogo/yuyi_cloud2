from cloud.models import CloudDocument
from mongoengine import BooleanField, IntField, ObjectIdField, StringField


class SensorConfig(CloudDocument):
    """
    1. create this model when get sensor_list from gateway or create point;
    2. set model_key, client_number, can_senor_online, communication_mode fields when get sensor_list from
        gateway;
    3. delete this model when customer/site/point/gateway deleted
    """

    sensor_number = StringField(required=True)
    sensor_type = StringField()
    client_number = StringField()
    model_key = StringField()
    can_senor_online = IntField(default=0)  # 是否支持在线模式, 0:不支持， 1：支持
    communication_mode = StringField()
    customer_id = ObjectIdField()
    site_id = ObjectIdField()
    equipment_id = ObjectIdField()
    point_id = ObjectIdField()
    has_bound = BooleanField(default=False)  # 是否已绑定

    meta = {
        "indexes": [
            "sensor_number",
            "client_number",
            "sensor_type",
            "site_id",
            "has_bound",
        ],
        "index_background": True,
        "collection": "sensor_config",
    }

    def __str__(self):
        return "SensorConfig: {}-{}".format(self.sensor_number, self.sensor_type)

    def __repr__(self):
        return self.__str__()
