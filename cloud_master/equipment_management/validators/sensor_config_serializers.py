import logging

from equipment_management.models.gateway import GateWay
from equipment_management.models.sensor_config import SensorConfig
from mongoengine import DoesNotExist, MultipleObjectsReturned
from rest_framework.fields import CharField, IntegerField

from common.const import SensorType, SensorWorkMode
from common.framework.exception import APIException
from common.framework.serializer import BaseSerializer

logger = logging.getLogger(__name__)


class BaseSensorConfigSerializer(BaseSerializer):
    def validate(self, data):
        client_number = self.context["client_number"]
        sensor_id = self.context["sensor_id"]
        try:
            sensor_config = SensorConfig.objects.only("sensor_type").get(
                sensor_number=sensor_id, client_number=client_number
            )
        except DoesNotExist:
            raise APIException(f"invalid {client_number=} or {sensor_id=}")
        sensor_type = sensor_config.sensor_type
        if sensor_type == SensorType.mech:
            raise APIException("mech sensor not support this action!")
        data["sensor_type"] = sensor_type
        return data


class PubSensorConfigSerializer(BaseSensorConfigSerializer):
    acq_period = IntegerField(required=True)

    def validate(self, data):
        return super().validate(data)


class PubMultiSensorConfigSerializer(BaseSerializer):
    sensor_id = CharField(required=True)
    sensor_type = CharField(required=True)
    acq_period = IntegerField(required=True)

    def validate_sensor_type(self, sensor_type):
        if sensor_type not in SensorType.values():
            raise APIException(f"invalid {sensor_type=}")
        if sensor_type == SensorType.mech:
            raise APIException("mech sensor not support this action!")
        return sensor_type

    def validate(self, data: dict) -> dict:
        sensor_id = data["sensor_id"]
        try:
            sensor_config = SensorConfig.objects.only("site_id", "sensor_type").get(
                sensor_number=sensor_id
            )
        except (DoesNotExist, MultipleObjectsReturned) as e:
            logger.exception(f"get sensor_config {e=}")
            raise APIException(f"invalid {sensor_id=}")
        if sensor_config.sensor_type == SensorType.mech:
            raise APIException(f"not support for mech!")
        data["site_id"] = sensor_config.site_id
        return data


class SensorWorkModeSerializer(BaseSerializer):
    work_mode = IntegerField(required=True)

    def validate_work_mode(self, work_mode: int) -> int:
        if work_mode not in SensorWorkMode.values():
            raise APIException(f"invalid {work_mode=}")
        return work_mode

    def validate(self, data: dict) -> dict:
        sensor_id = self.context["sensor_id"]
        try:
            sensor_config = SensorConfig.objects.get(sensor_number=sensor_id)
        except (DoesNotExist, MultipleObjectsReturned) as e:
            raise APIException(f"invalid {sensor_id=} with {e=}")
        if sensor_config.sensor_type == SensorType.mech:
            raise APIException(f"not support for mech!")
        data["sensor_config"] = sensor_config
        return data


class SiteSensorConfigSerializer(BaseSerializer):
    sensor_type = CharField(required=True)

    def validate_sensor_type(self, sensor_type: str) -> str:
        if sensor_type not in SensorType.values():
            raise APIException(f"invalid {sensor_type=}")
        return sensor_type

    def validate(self, data: dict) -> dict:
        site_id = self.context["site_id"]
        client_numbers = GateWay.objects.filter(site_id=site_id).values_list("client_number")
        data["client_numbers"] = client_numbers
        return data
