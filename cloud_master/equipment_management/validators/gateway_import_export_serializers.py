from customer.models.customer import Customer
from mongoengine import DoesNotExist, MultipleObjectsReturned
from rest_framework.fields import CharField, FileField
from sites.models.site import Site

from common.framework.exception import APIException
from common.framework.serializer import BaseSerializer


class GatewayImportSerializer(BaseSerializer):
    file = FileField(required=True, allow_empty_file=False)

    def validate(self, data: dict) -> dict:
        user = self.context["request"].user
        validate_customer_id = None
        if user.is_normal_admin():
            validate_customer_id = user.customer
        data["validate_customer_id"] = validate_customer_id
        return data


class GatewayExportSerializer(BaseSerializer):
    customer_id = CharField(required=True)
    site_id = CharField(required=False)

    def validate(self, data: dict) -> dict:
        user = self.context["request"].user
        customer_id = data["customer_id"]
        if user.is_normal_admin() and str(user.customer) != customer_id:
            raise APIException(f"Not enough permission for this {customer_id=}")
        return data

    def validate_customer_id(self, customer_id: str) -> str:
        try:
            customer = Customer.objects.get(pk=customer_id)
        except (DoesNotExist, MultipleObjectsReturned) as e:
            raise APIException(f"can't get customer by {customer_id}")
        self.context["customer"] = customer
        return customer_id

    def validate_site_id(self, site_id: str) -> str:
        try:
            site = Site.objects.get(pk=site_id)
        except (DoesNotExist, MultipleObjectsReturned) as e:
            raise APIException(f"can't get site by {site_id}")
        self.context["site"] = site
        return site_id
