import logging
import os
from datetime import datetime
# from urllib.parse import quote

from cloud.settings import BASE_DIR
from django.http import HttpResponse
from equipment_management.services.gateway_export_service import GatewayExportService
from equipment_management.services.gateway_import_service import GatewayExcelService
from equipment_management.validators.gateway_import_export_serializers import (
    GatewayExportSerializer,
    GatewayImportSerializer,
)
from openpyxl import load_workbook

from common.const import RoleLevel
from common.framework.permissions import PermissionFactory
from common.framework.response import BaseResponse
from common.framework.view import BaseView
from common.utils.str_utils import to_ascii

logger = logging.getLogger(__name__)


class GatewayImportView(BaseView):
    permission_classes = (
        PermissionFactory(
            RoleLevel.CLIENT_SUPER_ADMIN.value,
            RoleLevel.CLOUD_SUPER_ADMIN.value,
            RoleLevel.ADMIN.value,
        ),
    )

    def post(self, request):
        data, _ = self.get_validated_data(GatewayImportSerializer)
        logger.info(f"{request.user} request import gateway from excel!")
        validate_customer_id = data["validate_customer_id"]
        service = GatewayExcelService(validate_customer_id=validate_customer_id)
        import_succeed_num = service.gateway_file_import(data["file"])
        return BaseResponse(data={"import_succeed_num": import_succeed_num})


class GatewayExportView(BaseView):
    permission_classes = (
        PermissionFactory(
            RoleLevel.CLIENT_SUPER_ADMIN.value,
            RoleLevel.CLOUD_SUPER_ADMIN.value,
            RoleLevel.ADMIN.value,
        ),
    )

    def get(self, request):
        data, context = self.get_validated_data(GatewayExportSerializer)
        user = request.user
        logger.info(f"{user} request export gateways with {data=}!")
        gateway_export_service = GatewayExportService(
            user, data["customer_id"], site_id=data.get("site_id")
        )
        content = gateway_export_service.get_excel_content()
        now = datetime.now()
        customer = context["customer"]
        site = context.get("site")
        if site:
            filename = (
                f"{customer.name}--{site.name}--设备管理台账--{now.strftime('%Y%m%d')}.xlsx"
            )
        else:
            filename = f"{customer.name}--云平台档案--{now.strftime('%Y%m%d')}.xlsx"
        disposition = f"attachment; filename={to_ascii(filename)}"
        response = HttpResponse(content, content_type="application/vnd.ms-excel")
        response["Content-Disposition"] = disposition
        return response


class GatewayExcelTemplateView(BaseView):
    def get(self, request, *args, **kwargs):
        template_path = os.path.join(BASE_DIR, "gateway_file.xlsx")
        # load excel template
        wb = load_workbook(template_path)
        response = HttpResponse(
            content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        )
        # filename = "云平台主机档案管理模板.xlsx"
        # url_encoded_filename = quote(filename)
        # response[
        #     "Content-Disposition"
        # ] = f"attachment; filename*=UTF-8''{url_encoded_filename}"
        response['Content-Disposition'] = 'attachment; filename=gateway_template_file.xlsx'

        wb.save(response)

        return response
