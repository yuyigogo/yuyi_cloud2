import logging

from cloud.settings import MONGO_CLIENT
from equipment_management.models.sensor_config import SensorConfig
from equipment_management.validators.sensor_config_serializers import (
    BaseSensorConfigSerializer,
    PubMultiSensorConfigSerializer,
    PubSensorConfigSerializer,
    SensorWorkModeSerializer,
    SiteSensorConfigSerializer,
)
from rest_framework.status import HTTP_400_BAD_REQUEST

from common.const import (
    BASE_SENSOR_SAMPLE_PERIOD_PUBLISH_TOPIC,
    BASE_SENSOR_WORK_MODE_PUBLISH_TOPIC,
    RoleLevel,
)
from common.framework.permissions import PermissionFactory
from common.framework.response import BaseResponse
from common.framework.view import BaseView
from common.utils.cloud_mqtt_client import mqtt_publish

logger = logging.getLogger(__name__)


class SensorConfigView(BaseView):
    """get/update the upload_interval value for sensor"""

    def get(self, request, client_number, sensor_id):
        data, _ = self.get_validated_data(
            BaseSensorConfigSerializer, client_number=client_number, sensor_id=sensor_id
        )
        sensor_type = data["sensor_type"]
        my_col = MONGO_CLIENT[sensor_type]
        raw_query = {"sensor_id": sensor_id, "is_latest": True}
        projection = {"acq_period": 1}
        sensor_obj_data = my_col.find_one(raw_query, projection)
        if sensor_obj_data:
            acq_period = sensor_obj_data.get("acq_period")
        else:
            acq_period = None
        return BaseResponse(data={"acq_period": acq_period})

    def put(self, request, client_number, sensor_id):
        data, _ = self.get_validated_data(
            PubSensorConfigSerializer, client_number=client_number, sensor_id=sensor_id
        )
        logger.info(
            f"{request.user.username} request update sensor upload_interval with {client_number=}, {sensor_id=}"
            f" by mqtt publish client"
        )
        sensor_type = data["sensor_type"]
        payload = {"sensor_type": sensor_type, "params": {"period": data["acq_period"]}}
        topic = f"/{client_number}/subnode/{sensor_id}{BASE_SENSOR_SAMPLE_PERIOD_PUBLISH_TOPIC}"
        try:
            mqtt_publish([(topic, payload)])
        except Exception as e:
            logger.exception(
                f"update sensor's upload_interval failed in mqtt with {e=}"
            )
            return BaseResponse(status_code=HTTP_400_BAD_REQUEST)
        return BaseResponse()


class SensorConfigsView(BaseView):
    """同种批量应用"""

    def put(self, request):
        data, _ = self.get_validated_data(PubMultiSensorConfigSerializer)
        logger.info(
            f"{request.user.username} request update multi sensor's upload_interval with {data=}"
        )
        site_id = data["site_id"]
        sensor_type = data["sensor_type"]
        sensor_id_client_number = SensorConfig.objects.filter(
            site_id=site_id, sensor_type=sensor_type
        ).values_list("sensor_number", "client_number")
        payload = {"sensor_type": sensor_type, "params": {"period": data["acq_period"]}}
        topic_payload_list = [
            (
                f"/{client_number}/subnode/{sensor_id}{BASE_SENSOR_SAMPLE_PERIOD_PUBLISH_TOPIC}",
                payload,
            )
            for (sensor_id, client_number) in sensor_id_client_number
        ]
        try:
            mqtt_publish(topic_payload_list)
        except Exception as e:
            logger.exception(
                f"update sensor's upload_interval failed in mqtt with {e=}"
            )
        return BaseResponse()


class SensorWorkModeView(BaseView):
    def get(self, request, sensor_id):
        pass

    def put(self, request, sensor_id):
        data, _ = self.get_validated_data(SensorWorkModeSerializer, sensor_id=sensor_id)
        logger.info(
            f"{request.user.username} request enable/disable sensor mode for {sensor_id=} with {data=}"
        )
        sensor_config = data["sensor_config"]
        client_number = sensor_config.client_number
        sensor_type = sensor_config.sensor_type
        topic = (
            f"/{client_number}/subnode/{sensor_id}{BASE_SENSOR_WORK_MODE_PUBLISH_TOPIC}"
        )
        payload = {
            "sensor_type": sensor_type,
            "params": {"work_mode": data["work_mode"]},
        }
        mqtt_publish([(topic, payload)])
        return BaseResponse()


class SiteSensorConfigView(BaseView):
    permission_classes = (
        PermissionFactory(
            RoleLevel.CLIENT_SUPER_ADMIN.value,
            RoleLevel.CLOUD_SUPER_ADMIN.value,
            RoleLevel.ADMIN.value,
        ),
    )

    def get(self, request, site_id):
        """
        get unbound sensor_configs;
        when bound sensor_id to point, should get from this api.
        should only query from client_number
        :param request
        :param site_id:
        :return:
        """
        data, _ = self.get_validated_data(SiteSensorConfigSerializer, site_id=site_id)
        logger.info(
            f"{request.user.username} request unbound sensor_configs with {data=}"
        )
        client_numbers = data["client_numbers"]
        sensor_type = data["sensor_type"]
        sensor_numbers = SensorConfig.objects.filter(
            sensor_type=sensor_type, has_bound=False, client_number__in=client_numbers
        ).values_list("sensor_number", "sensor_type")
        return BaseResponse(data=sensor_numbers)
