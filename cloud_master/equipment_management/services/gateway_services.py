import logging
from collections import defaultdict
from datetime import datetime
from typing import Optional

from alarm_management.models.alarm_info import AlarmInfo
from cloud.models import bson_to_dict
from cloud.settings import MONGO_CLIENT
from equipment_management.models.gateway import GateWay
from equipment_management.models.sensor_config import SensorConfig
from file_management.models.measure_point import MeasurePoint
from mongoengine import Q

from common.const import SensorType
from common.framework.service import BaseService
from common.utils import get_objects_pagination

logger = logging.getLogger(__name__)


class GatewayService(BaseService):
    def __init__(self, site_id: str):
        self.site_id = site_id

    def create_gateway(self, data: dict) -> GateWay:
        gateway = GateWay(
            name=data["name"],
            customer=data["customer"],
            client_number=data["client_number"],
            time_adjusting=datetime.fromtimestamp(data["time_adjusting"]),
            site_id=self.site_id,
            remarks=data.get("remarks"),
        )
        gateway.save()
        return gateway

    def get_list_gateway_in_site(self) -> list:
        gateways = GateWay.objects.only("name", "client_number").filter(
            site_id=self.site_id
        )
        data, client_numbers = [], []
        for gateway in gateways:
            client_number = gateway.client_number
            client_numbers.append(client_number)
            data.append(
                {
                    "gateway_id": str(gateway.id),
                    "name": gateway.name,
                    "client_number": client_number,
                }
            )
        latest_info_dict = self.get_latest_gateway_infos(client_numbers)
        if latest_info_dict:
            [
                d.update(latest_info_dict.get(d["client_number"]))
                for d in data
                if latest_info_dict.get(d["client_number"])
            ]
        return data

    @classmethod
    def get_latest_gateway_infos(cls, client_numbers: list) -> dict:
        alarm_infos = AlarmInfo.objects.only(
            "is_online", "create_date", "client_number"
        ).filter(client_number__in=client_numbers, is_latest=True)
        return {
            alarm_info.client_number: {
                "is_online": alarm_info.is_online,
                "update_time": bson_to_dict(alarm_info.create_date),
            }
            for alarm_info in alarm_infos
        }

    @classmethod
    def get_sensor_info_in_gateway(
        cls,
        page: int,
        limit: int,
        client_number: str,
        sensor_id: Optional[str],
        sensor_type: Optional[str],
        is_online: Optional[bool],
    ) -> tuple:
        sensor_query = Q(client_number=client_number)
        point_query = Q()
        if sensor_id:
            # alarm_query &= Q(sensor_id=sensor_id)
            sensor_query &= Q(sensor_number=sensor_id)
            point_query &= Q(sensor_number=sensor_id)
        if sensor_type:
            # alarm_query &= Q(sensor_type=sensor_type)
            sensor_query &= Q(sensor_type=sensor_type)
            point_query &= Q(measure_type=sensor_type)
        # if is_online is not None:
        #     # alarm_query &= Q(is_online=is_online)
        #     # sensor_ids = AlarmInfo.objects.filter(alarm_query).values_list("sensor_id")
        #     point_query &= Q(is_online=is_online)
        # 过滤is_online查询
        # is_online未true，表明传感器一定已绑定，is_online为false，有可能已绑定，有可能位绑定
        if is_online:
            sensor_query &= Q(has_bound=True)
        elif is_online is False:
            # 查所有的sensor_conf，去掉查point中在线的sensor_ids
            point_query &= Q(is_online=True)
            sensor_ids = set(MeasurePoint.objects.filter(point_query).values_list(
                "sensor_number"
            ))
            all_sensor_ids = set(SensorConfig.objects.filter(sensor_query).values_list("sensor_number"))
            filter_sensor_ids = all_sensor_ids - sensor_ids
            sensor_query &= Q(sensor_number__in=filter_sensor_ids)
        sensors = SensorConfig.objects.only(
            "sensor_number", "sensor_type", "communication_mode", "client_number"
        ).filter(sensor_query)
        total = sensors.count()
        sensor_by_page = get_objects_pagination(page, limit, sensors)
        data, sensor_type_sensor_ids = [], defaultdict(list)
        sensor_ids = []
        for sensor_config in sensor_by_page:
            sensor_type = sensor_config.sensor_type
            sensor_id = sensor_config.sensor_number
            data.append(
                {
                    "name": f"{sensor_type}传感器",
                    "sensor_id": sensor_id,
                    "sensor_type": sensor_type,
                    "client_number": sensor_config.client_number,
                }
            )
            sensor_type_sensor_ids[sensor_type].append(sensor_id)
            sensor_ids.append(sensor_id)
        sensors_online_info = cls.get_point_is_online_by_sensor_ids(sensor_ids)
        sensor_infos = cls._get_sensor_info_in_gateway(
            sensor_type_sensor_ids, sensors_online_info
        )
        [
            info_dict.update(sensor_infos.get(info_dict["sensor_id"]))
            for info_dict in data
            if sensor_infos.get(info_dict["sensor_id"])
        ]
        return total, data

    @classmethod
    def _get_sensor_info_in_gateway(
        cls, sensor_type_sensor_ids: dict, sensors_online_info: dict
    ) -> dict:
        sensor_data = []
        for sensor_type, sensor_ids in sensor_type_sensor_ids.items():
            not_display_fields = None
            my_col = MONGO_CLIENT[sensor_type]
            raw_query = {"sensor_id": {"$in": sensor_ids}, "is_latest": True}
            if sensor_type in SensorType.partial_discharge_sensors():
                not_display_fields = {"prps": 0, "prpd": 0}
            elif sensor_type == SensorType.mech.value:
                not_display_fields = {
                    "Mech_On_Coil_I": 0,
                    "Mech_Off_Coil_I": 0,
                    "Mech_CT_A_I": 0,
                    "Mech_CT_B_I": 0,
                    "Mech_CT_C_I": 0,
                    "Mech_DIS_I": 0,
                }
            sensors = my_col.find(raw_query, not_display_fields)
            sensor_data.extend(sensors)
        sensor_infos = {
            sensor["sensor_id"]: {
                "sensor_data_id": str(sensor["_id"]),
                # "update_time": bson_to_dict(sensor["create_date"]),
                "update_time": sensors_online_info.get(sensor["sensor_id"], {}).get("last_received_time"),
                "upload_interval": sensor.get("upload_interval", ""),
                # "is_online": sensor["is_online"],
                "is_online": sensors_online_info.get(sensor["sensor_id"], {}).get("is_online", False),
                "battery": sensor.get("battery", ""),
                "rssi": sensor.get("rssi", ""),
                "snr": sensor.get("snr", ""),
            }
            for sensor in sensor_data
        }
        return sensor_infos
