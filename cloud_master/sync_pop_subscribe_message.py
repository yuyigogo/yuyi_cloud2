"""
This script is to pop msg from redis queue, and do following things:
    1. store sensor data;
    2. store alarm data;
    3. push alarm data with ws.
    4. deal with sensor configs in gateway
"""
import json
import logging
import os
from datetime import timedelta
from functools import lru_cache
from typing import Optional, Tuple

from alarm_management.models.alarm_info import AlarmInfo
from bson import ObjectId
from cloud.models import bson_to_dict
from cloud.settings import MONGO_CLIENT
from cloud_home.services.abnormal_count_service import AbnormalCacheService
from cloud_ws.ws_group_send import WsGatewaySensorsDataSend, WsSensorDataSend
from cloud_ws.ws_uri_code import Uri2C
from customer.models.customer import Customer
from file_management.models.electrical_equipment import ElectricalEquipment
from file_management.models.measure_point import MeasurePoint
from mongoengine import Q
from sensor_data.models.mech_motor import MechMotor, MechMotorSep
from sites.models.site import Site

from common.const import (
    CLOUD_SUBSCRIBE_MSG_LIST,
    MODEL_KEY_TO_SENSOR_TYPE,
    SENSOR_INFO_PREFIX,
    SITE_UNPROCESSED_NUM,
    AlarmFlag,
    AlarmLevel,
    AlarmRestraintKeys,
    AlarmType,
    MsgQueueType,
    SensorType,
)
from common.framework.service import BaseService, SensorConfigService
from common.storage.redis import msg_queue_redis, normal_redis
from common.utils import datetime_from_str

LOG_FORMAT = "%(asctime)s - %(levelname)s - %(message)s"
DATE_FORMAT = "%m/%d/%Y %H:%M:%S %p"
logging.basicConfig(
    # filename="/home/logs/sync_pop_subscribe_message.log",
    level=logging.INFO,
    format=LOG_FORMAT,
    datefmt=DATE_FORMAT,
)


class SyncPopMsg(object):
    @classmethod
    def run(cls):
        while True:
            while msg_queue_redis.llen(CLOUD_SUBSCRIBE_MSG_LIST):
                try:
                    handle_data = msg_queue_redis.rpop(CLOUD_SUBSCRIBE_MSG_LIST)
                    data = json.loads(handle_data)
                    cls.deal_with_msg_queue(data)
                except Exception as e:
                    logging.exception(
                        f"deal with pop msg from queue error with exception: {e=}",
                        exc_info=True,
                    )

    @classmethod
    def deal_with_msg_queue(cls, data: dict):
        msg_queue_type = data["msg_queue_type"]
        gateway_id = data["gateway_id"]
        sensor_id = data["sensor_id"]
        msg_dict = json.loads(data["msg_str"])
        if msg_queue_type == MsgQueueType.DATA_LOADER:
            # 采集数据上传
            cls.deal_with_sensor_data(gateway_id, sensor_id, msg_dict)
        elif msg_queue_type == MsgQueueType.SENSOR_ALARM.value:
            # 传感器报警
            cls.deal_with_sensor_alarm(gateway_id, sensor_id, msg_dict)
        elif msg_queue_type == MsgQueueType.SENSORS_IN_GATEWAY.value:
            # 网关下传感器列表
            try:
                SensorConfigService.deal_with_sensors_in_gateway_msg(
                    gateway_id, msg_dict
                )
                msg = "success"
                code = Uri2C.S2C_GATEWAY_SUCCESS_DATA.value
            except Exception as e:
                logging.error(
                    f"deal_with_sensors_in_gateway_msg failed with {e=}, {gateway_id=}"
                )
                msg = "failed"
                code = Uri2C.S2C_GATEWAY_FAILED_DATA.value
            # send ws msg
            WsGatewaySensorsDataSend(gateway_id).ws_send(msg=msg, code=code)

    @classmethod
    def deal_with_sensor_data(cls, gateway_id: str, sensor_id: str, sensor_data: dict):
        """
        采集数据上传
        1. store sensor data to db;
        2. store alarm data to db;
        3. push data from ws;
        """
        logging.info(
            f"begin to process sensor_data_ret for {gateway_id=}, {sensor_id=}"
        )
        base_sensor_info = cls.get_sensor_info(sensor_id)
        if not base_sensor_info:
            logging.warning(
                f"can't get sensor_info in deal_with_sensor_data:{sensor_id=}"
            )
            return
        point_id = base_sensor_info.get("point_id")
        if not point_id:
            logging.error(f"can't get file info for {sensor_id=}")
            return
        model_key = base_sensor_info.get("model_key")
        sensor_type = MODEL_KEY_TO_SENSOR_TYPE.get(model_key)
        if not sensor_type or sensor_type not in SensorType.values():
            logging.warning(f"can't get {sensor_type=} in deal_with_sensor_data")
            return

        alarm_block_info = cls.get_alarm_block_info(point_id)
        logging.info(f"get {alarm_block_info=}")

        (
            new_obj_dict,
            mech_motor_data,
            mech_motor_sep_data,
        ) = cls.insert_and_update_sensor_data(
            sensor_type,
            gateway_id,
            sensor_id,
            sensor_data,
            base_sensor_info,
            alarm_block_info,
        )
        alarm_info = None
        if new_obj_dict:
            if sensor_type not in SensorType.mech_sensors():
                # 机械特性传感器默认始终在线
                BaseService.update_point_online_info(
                    point_id,
                    True,
                    new_obj_dict.get("acq_period", 0),
                    new_obj_dict["create_date"],
                )
            alarm_info = cls.insert_and_update_alarm_info(new_obj_dict)
        # ws: 1. push alarm data; 2. sensor_list data
        cls.ws_send_sensor_data(
            new_obj_dict, alarm_info, mech_motor_data, mech_motor_sep_data
        )

    @classmethod
    def deal_with_sensor_alarm(cls, gateway_id: str, sensor_id: str, origin_data: dict):
        """
        This method is to deal with sensor alarm(alarm type is SENSOR_ALARM).
        when a point alarm has happened, the following thing need to do:
            1. insert a new data in alarm_info;
            2. update the corresponding record's is_online field;
        """
        logging.info(
            f"begin to process sensor_alarm_ret for {gateway_id=}, {sensor_id=}"
        )
        sensor_type = origin_data.get("sensor_type")
        if sensor_type not in SensorType.values():
            logging.warning(f"************invalid {sensor_type=}")
            return
        parsed_dict = {
            "sensor_type": sensor_type,
            "client_number": gateway_id,
            "sensor_id": sensor_id,
            "is_latest": True,
            "is_processed": False,
            "alarm_type": AlarmType.SENSOR_ALARM.value,
        }
        sensor_info = cls.get_sensor_info(sensor_id)
        if not sensor_info:
            logging.warning(
                f"can't get sensor_info in process sensor alarm :{sensor_id=}"
            )
            return
        parsed_dict.update(sensor_info)
        params = origin_data.get("params", {})
        battery_alert = params.get("battery_alert")
        online_alert = params.get("online_alert")
        if battery_alert:
            create_time = datetime_from_str(battery_alert["time"])
            parsed_dict["alarm_describe"] = 7  # "电池低电量报警"
            parsed_dict["is_online"] = True
        else:
            create_time = datetime_from_str(online_alert["time"])
            parsed_dict["alarm_describe"] = 7  # "掉线报警"
            parsed_dict["is_online"] = False
        parsed_dict["alarm_flag"] = AlarmFlag.PUSH.value
        parsed_dict["alarm_level"] = AlarmLevel.ALARM.value
        parsed_dict["create_date"] = create_time
        parsed_dict["update_date"] = create_time
        if parsed_dict["is_online"] is False:
            AlarmInfo.objects.filter(
                is_latest=True,
                sensor_id=sensor_id,
                alarm_type=AlarmType.POINT_ALARM.value,
            ).update(is_online=False)
            # set corresponding sensor_type db's is_online to false
            my_col = MONGO_CLIENT[sensor_type]
            my_query = {"is_latest": True, "sensor_id": sensor_id}
            new_values = {"$set": {"is_online": False}}
            my_col.update_one(my_query, new_values)
        new_alarm_info = AlarmInfo(**parsed_dict)
        new_alarm_info.save()
        # 传感器告警不需要计算如下异常信息
        # cls.set_abnormal_infos(parsed_dict)
        cls.ws_send_alarm_data(new_alarm_info)

    @classmethod
    def calculate_alarm_restraint(
        cls, alarm_block_info: dict, alarm_info: dict
    ) -> bool:
        enable_block_alarm = alarm_block_info.get("enable_block_alarm", False)
        enable_alarm_restraint = alarm_block_info.get("enable_alarm_restraint", False)
        alarm_restraint_info = alarm_block_info.get("alarm_restraint_info", {})
        restraint_flag = alarm_block_info.get("restraint_flag", False)
        restraint_deadline = alarm_block_info.get("restraint_deadline")
        point_id = alarm_info["point_id"]
        create_date = alarm_info["create_date"]
        if enable_block_alarm:
            push_alarm_data = False
        elif not enable_block_alarm and enable_alarm_restraint and alarm_restraint_info:
            alarm_duration = alarm_restraint_info[
                AlarmRestraintKeys.ALARM_DURATION.value
            ]
            max_alarm_nums = alarm_restraint_info[
                AlarmRestraintKeys.MAX_ALARM_NUMS.value
            ]
            push_no_alarm_minutes = alarm_restraint_info[
                AlarmRestraintKeys.PUSH_NO_ALARM_MINUTES.value
            ]

            if restraint_flag:
                if create_date <= restraint_deadline:
                    logging.info(
                        f"{create_date=} less then {restraint_deadline=}, no need push data {point_id=}."
                    )
                    push_alarm_data = False
                else:
                    logging.info(f"{create_date=} more then {restraint_deadline=}, push data")
                    push_alarm_data = True
                    restraint_flag = False
                    restraint_deadline = None
                    cls.update_point_restraint_flag(
                        point_id,
                        restraint_flag=restraint_flag,
                        restraint_deadline=restraint_deadline,
                    )
                    logging.info(f"update {restraint_flag=}, {restraint_deadline=}, {point_id=}")

            else:
                alarm_nums = cls.count_alarm_by_alarm_duration(
                    alarm_duration, point_id, create_date
                )
                logging.info(f"{alarm_nums=} for {point_id=}")
                if alarm_nums + 1 >= max_alarm_nums:
                    # 等于max_alarm_nums时，当条数据告警，大于时，当条数据不告警
                    push_alarm_data = True if alarm_nums + 1 == max_alarm_nums else False
                    logging.info(f"{alarm_nums=} for {point_id=}, {push_alarm_data=} push data")
                    restraint_deadline = create_date + timedelta(
                        minutes=push_no_alarm_minutes
                    )
                    logging.info(f"{create_date=}, {restraint_deadline=} for {point_id=}")
                    cls.update_point_restraint_flag(
                        point_id,
                        restraint_flag=True,
                        restraint_deadline=restraint_deadline,
                    )
                    logging.info(f"update {restraint_flag=}, {restraint_deadline=} in counts for {point_id=}")
                else:
                    logging.info(f"less then max_alarm_nums, push for {point_id=}")
                    push_alarm_data = True
        else:
            logging.info(f"both no enabled for {point_id=}")
            push_alarm_data = True
        msg = "push data" if push_alarm_data else "no need push data!"
        logging.info(f"{alarm_block_info=}, {point_id=}, {create_date=}, {msg=}")
        return push_alarm_data

    @classmethod
    def count_alarm_by_alarm_duration(
        cls, alarm_duration: int, point_id: str, sample_date
    ) -> int:
        alarm_query = Q(
            alarm_type=AlarmType.POINT_ALARM.value,
            alarm_level__in=AlarmLevel.abnormal_alarm_level(),
            point_id=point_id,
        )
        last_alarm_time = sample_date - timedelta(minutes=alarm_duration)
        time_range_query = Q(create_date__gt=last_alarm_time) & Q(
            create_date__lte=sample_date
        )
        alarm_query &= time_range_query
        return AlarmInfo.objects(alarm_query).count()

    @classmethod
    def update_point_restraint_flag(
        cls, point_id: str, restraint_flag: bool, restraint_deadline
    ):
        MeasurePoint.objects.filter(pk=point_id).update(
            set__restraint_flag=restraint_flag,
            set__restraint_deadline=restraint_deadline,
        )

    @classmethod
    def get_sensor_info(cls, sensor_id: str) -> Optional[dict]:
        sensor_info_key = f"{SENSOR_INFO_PREFIX}{sensor_id}"
        data_from_redis = normal_redis.hgetall(sensor_info_key)
        if data_from_redis:
            return data_from_redis
        else:
            sensor_info = SensorConfigService(
                sensor_id
            ).get_or_set_sensor_info_from_sensor_config()
            if sensor_info is None:
                logging.warning(f"get sensor_info failed for {sensor_id=}")
            return sensor_info

    @classmethod
    def insert_and_update_alarm_info(cls, sensor_obj_dict: dict) -> Optional[dict]:
        sensor_id = sensor_obj_dict["sensor_id"]
        site_id = sensor_obj_dict["site_id"]
        alarm_level = sensor_obj_dict["alarm_level"]
        is_abnormal_alarm = alarm_level in AlarmLevel.abnormal_alarm_level()
        alarm_info = {
            "sensor_id": sensor_id,
            "sensor_type": sensor_obj_dict["sensor_type"],
            "client_number": sensor_obj_dict["client_number"],
            "is_latest": True,
            "alarm_flag": sensor_obj_dict["alarm_flag"],
            "alarm_type": AlarmType.POINT_ALARM.value,
            "alarm_level": alarm_level,
            "alarm_describe": sensor_obj_dict["alarm_describe"],
            "sensor_data_id": sensor_obj_dict["_id"],
            "is_online": True,
            "is_processed": False if is_abnormal_alarm else True,
            "create_date": sensor_obj_dict["create_date"],
            "update_date": sensor_obj_dict["update_date"],
            "customer_id": sensor_obj_dict["customer_id"],
            "site_id": site_id,
            "equipment_id": sensor_obj_dict["equipment_id"],
            "point_id": sensor_obj_dict["point_id"],
        }
        # update is_latest filed to false
        AlarmInfo.objects.filter(
            is_latest=True, sensor_id=sensor_id, alarm_type=AlarmType.POINT_ALARM.value
        ).update(is_latest=False)
        new_alarm_info = AlarmInfo(**alarm_info)
        new_alarm_info.save()
        alarm_info["alarm_id"] = str(new_alarm_info.pk)
        cls.set_abnormal_infos(alarm_info)
        return alarm_info

    @classmethod
    def set_abnormal_infos(cls, alarm_info: dict):
        """
        1. set unprocessed_unm for site
        2. increment customer/site day/week/month's alarm num
        :param alarm_info: dict
        :return:
        """
        if alarm_info["alarm_level"] in AlarmLevel.abnormal_alarm_level():
            site_id = alarm_info["site_id"]
            # set unprocessed_unm for site
            normal_redis.incrby(f"{SITE_UNPROCESSED_NUM}{str(site_id)}")
            # increment customer/site day/week/month's alarm num
            service = AbnormalCacheService(
                customer_id=alarm_info["customer_id"], site_id=site_id
            )
            service.auto_increment_customer_abnormal_infos()
            service.auto_increment_site_abnormal_infos()

    @classmethod
    def insert_and_update_sensor_data(
        cls,
        sensor_type: str,
        gateway_id: str,
        sensor_id: str,
        sensor_data: dict,
        base_sensor_info: dict,
        alarm_block_info: dict,
    ) -> Tuple[Optional[dict], Optional[dict], Optional[dict]]:
        """
        1. insert new sensor data to db;
        2. update old sensor data's is_latest to false
        """
        my_query = {"is_latest": True, "sensor_id": sensor_id}
        new_values = {"$set": {"is_latest": False}}
        try:
            (
                parsed_sensor_dict,
                mech_motor_data,
                mech_motor_sep_data,
            ) = cls.parse_origin_sensor_data(
                sensor_type,
                gateway_id,
                sensor_id,
                sensor_data,
                base_sensor_info,
                alarm_block_info,
            )
            if not parsed_sensor_dict and sensor_type not in SensorType.mech_sensors():
                logging.warning(
                    "-----------------parsed_sensor_dict is none, discard this data"
                )
                return None, None, None
            if parsed_sensor_dict:
                # when sensor_data only contain mech_motor data, parsed_sensor_dict is none
                my_col = MONGO_CLIENT[sensor_type]
                # update old data before insert new sensor data
                my_col.update_many(my_query, new_values)
                # insert new sensor data
                my_col.insert_one(parsed_sensor_dict)
            if mech_motor_data:
                MechMotor(**mech_motor_data).save()
            if mech_motor_sep_data:
                MechMotorSep(**mech_motor_sep_data).save()
        except Exception as e:
            logging.error(f"***************insert new sensor data error with {e=}")
            return None, None, None
        return parsed_sensor_dict, mech_motor_data, mech_motor_sep_data

    @classmethod
    def parse_origin_sensor_data(
        cls,
        sensor_type: str,
        gateway_id: str,
        sensor_id: str,
        sensor_data: dict,
        base_sensor_info: dict,
        alarm_block_info: dict,
    ) -> Optional[tuple]:
        parsed_dict = {
            "_id": ObjectId(),
            "sensor_type": sensor_type,
            "client_number": gateway_id,
            "sensor_id": sensor_id,
            "is_latest": True,
            "is_online": True,
        }
        parsed_dict.update(base_sensor_info)
        params = sensor_data.get("params", {})
        mech_motor_data = dict()  # store mech_motor
        mech_motor_sep_data = dict()  # store mech_motor_sep
        if sensor_type in SensorType.single_value_sensors():
            d_sensor_data = cls.parse_single_value_sensors(params, parsed_dict)
        elif sensor_type in SensorType.partial_discharge_sensors():
            d_sensor_data = cls.parse_partial_discharge_sensors(params, parsed_dict)
        elif sensor_type == SensorType.temp:
            d_sensor_data = cls.parse_temp_sensor(params, parsed_dict)
        elif sensor_type == SensorType.mech:
            d_sensor_data, mech_motor_data = cls.parse_mech_sensor(params, parsed_dict)
        elif sensor_type == SensorType.mech_sep:
            d_sensor_data, mech_motor_sep_data = cls.parse_mech_sep_sensor(
                params, parsed_dict
            )
        else:
            logging.warning(
                f"not supported sensor_type: {sensor_type} in parse_origin_sensor_data"
            )
            return
        if d_sensor_data:
            origin_alarm_level = d_sensor_data["alarm_level"]
            if origin_alarm_level in AlarmLevel.abnormal_alarm_level():
                push_alarm_data = cls.calculate_alarm_restraint(
                    alarm_block_info, d_sensor_data
                )
                if not push_alarm_data:
                    # 如果开启报警屏蔽或满足告警屏蔽，将该条告警等级变更为普通
                    d_sensor_data["alarm_level"] = AlarmLevel.NORMAL.value
        return d_sensor_data, mech_motor_data, mech_motor_sep_data

    @classmethod
    def parse_single_value_sensors(
        cls, origin_params: dict, d_sensor_data: dict
    ) -> dict:
        """
        解析单值传感器(设备温度、环境温度、环境湿度、臭氧)
        :param origin_params:
        :param d_sensor_data:
        :return:
        """
        sensor_type = d_sensor_data["sensor_type"]
        d_sensor_data.update(origin_params.get("status", {}))
        d_sensor_data.update(origin_params.get("wparam", {}))
        data = origin_params.get(sensor_type, {})
        create_time = datetime_from_str(data.get("Acqtime", ""))
        d_sensor_data["create_date"] = create_time
        d_sensor_data["update_date"] = create_time
        d_sensor_data["unit"] = data.get("Unit", 0)
        d_sensor_data["max_limit"] = data.get("MaxLimit")
        d_sensor_data["min_limit"] = data.get("MinLimit")
        d_sensor_data["amplitude"] = data.get("Amplitude")  # 特征值
        # 新加的5中单值传感器有告警信息,其余的无
        alarm_type = data.get("AlarmType")
        d_sensor_data["alarm_flag"] = data.get(
            "alert_flag", AlarmFlag.NO_PUSH.value
        )  # alert_flag not used now
        d_sensor_data["alarm_level"] = data.get("AlarmLevel", AlarmLevel.NORMAL.value)
        d_sensor_data["alarm_describe"] = 0
        if alarm_type is not None:
            d_sensor_data["AlarmType"] = alarm_type
            if alarm_type == 0:
                d_sensor_data["alarm_describe"] = 0
            elif alarm_type == 1:
                d_sensor_data["alarm_describe"] = 100
            else:
                d_sensor_data["alarm_describe"] = 101
        return d_sensor_data

    @classmethod
    def parse_partial_discharge_sensors(
        cls, origin_params: dict, d_sensor_data: dict
    ) -> dict:
        """
        解析局放传感器(UHF、TEV、AE、HFCT)
        :param origin_params:
        :param d_sensor_data:
        :return:
        """
        sensor_type = d_sensor_data["sensor_type"]
        d_sensor_data.update(origin_params.get("status", {}))
        d_sensor_data.update(origin_params.get("wparam", {}))
        data = origin_params.get(sensor_type, {})
        create_time = datetime_from_str(data.get("Acqtime", ""))
        _data = {
            "create_date": create_time,
            "update_date": create_time,
            "phase_windows": data.get("PhaseWindows"),
            "quantized_amplitude": data.get("QuantizedAmplitude"),
            "power_frequency_cycles": data.get("PowerFrequencyCycles"),
            "unit": data.get("Unit", 0),
            "max_limit": data.get("MaxLimit"),
            "min_limit": data.get("MinLimit"),
            "band_width": data.get("BandWidth"),
            "PD_type_probability": data.get("PDTypeProbability"),
            "PDType": data.get("PDType", 0),
            "alarm_level": data.get("AlarmLevel", AlarmLevel.NORMAL.value),
            "alarm_describe": data.get("PDType", 0),
            "alarm_flag": data.get(
                "AlarmFlag", AlarmFlag.PUSH.value
            ),  # this version set it to 1
            "peak": float(data.get("Peak", 0)),  # 特征值
            "average": data.get("Average"),
            "DenoisingN": data.get("DenoisingN"),
            "F50": data.get("F50"),
            "F100": data.get("F100", ""),
            "prps": data.get("PRPS", ""),
            "prpd": data.get("PRPD"),
        }
        d_sensor_data.update(_data)
        return d_sensor_data

    @classmethod
    def parse_temp_sensor(cls, origin_params: dict, d_sensor_data: dict) -> dict:
        """
        :param origin_params:
        :param d_sensor_data:
        :return:
        """
        status = origin_params.get("status", {})
        temp = origin_params.get("Temp", {})
        d_sensor_data.update(status)
        d_sensor_data.update(temp)
        # 温度传感器冗余一个acq_period字段，用于判断是否在线
        d_sensor_data["acq_period"] = status.get("upload_interval", 0)
        create_time = datetime_from_str(temp.get("acqtime", ""))
        d_sensor_data["create_date"] = create_time
        d_sensor_data["update_date"] = create_time
        # no alarm info for mow, set default values
        d_sensor_data["alarm_flag"] = temp.get("alert_flag", AlarmFlag.NO_PUSH.value)
        d_sensor_data["alarm_level"] = temp.get("alert_level", AlarmLevel.NORMAL.value)
        d_sensor_data["alarm_describe"] = 0
        return d_sensor_data

    @classmethod
    def parse_mech_sensor(cls, origin_params: dict, d_sensor_data: dict) -> tuple:
        # 机械特性三相共体 Mech
        mech_motor_data = dict()
        # 电机参数
        Mech_Motor_I = origin_params.get("Mech_Motor_I", {})
        if Mech_Motor_I:
            create_time = datetime_from_str(Mech_Motor_I["acqtime"])
            mech_motor_data["create_date"] = create_time
            mech_motor_data["update_date"] = create_time
            mech_motor_data["Mech_Motor_I"] = Mech_Motor_I
            mech_motor_data["Mech_Results"] = origin_params.get("Mech_Results", {})
            # set sensor_info
            mech_motor_data.update(
                {
                    "sensor_type": d_sensor_data["sensor_type"],
                    "client_number": d_sensor_data["client_number"],
                    "sensor_id": d_sensor_data["sensor_id"],
                    "customer_id": d_sensor_data["customer_id"],
                    "site_id": d_sensor_data["site_id"],
                    "equipment_id": d_sensor_data["equipment_id"],
                    "point_id": d_sensor_data["point_id"],
                    "model_key": d_sensor_data["model_key"],
                }
            )
        Mech_On_Coil_I = origin_params.get("Mech_On_Coil_I")
        if not Mech_On_Coil_I:
            d_sensor_data = {}
        if Mech_On_Coil_I:
            # 线圈部分
            create_time = datetime_from_str(Mech_On_Coil_I["acqtime"])
            d_sensor_data["create_date"] = create_time
            d_sensor_data["update_date"] = create_time
            d_sensor_data["Mech_On_Coil_I"] = Mech_On_Coil_I
            d_sensor_data["Mech_Off_Coil_I"] = origin_params.get("Mech_Off_Coil_I", {})
            d_sensor_data["Mech_CT_A_V"] = origin_params.get("Mech_CT_A_I", {})
            d_sensor_data["Mech_CT_B_V"] = origin_params.get("Mech_CT_B_I", {})
            d_sensor_data["Mech_CT_C_V"] = origin_params.get("Mech_CT_C_I", {})
            d_sensor_data["Mech_DIS_I"] = origin_params.get("Mech_DIS_I", {})
            d_sensor_data["Mech_Results"] = origin_params.get("Mech_Results", {})
            # Mech no alarm for now
            d_sensor_data["alarm_flag"] = AlarmFlag.NO_PUSH.value
            d_sensor_data["alarm_level"] = AlarmLevel.NORMAL.value
            d_sensor_data["alarm_describe"] = 0
        return d_sensor_data, mech_motor_data

    @classmethod
    def parse_mech_sep_sensor(cls, origin_params: dict, d_sensor_data: dict) -> tuple:
        # 机械特性三相分体 mech_sep
        # mech_coil_sep 线圈电流， mech_motor_sep 电机电流
        mech_motor_sep = dict()
        mech_result = origin_params.get("Mech_Results")
        Motor_A = mech_result.get("Motor_A")
        if Motor_A:
            # 电机
            # set sensor_info
            mech_motor_sep.update(
                {
                    "sensor_type": d_sensor_data["sensor_type"],
                    "client_number": d_sensor_data["client_number"],
                    "sensor_id": d_sensor_data["sensor_id"],
                    "customer_id": d_sensor_data["customer_id"],
                    "site_id": d_sensor_data["site_id"],
                    "equipment_id": d_sensor_data["equipment_id"],
                    "point_id": d_sensor_data["point_id"],
                    "model_key": d_sensor_data["model_key"],
                }
            )
            Mech_Motor_A_I = origin_params.get("Mech_Motor_A_I", {})
            create_time = datetime_from_str(Mech_Motor_A_I["acqtime"])
            mech_motor_sep["create_date"] = create_time
            mech_motor_sep["update_date"] = create_time
            mech_motor_sep["Mech_Results"] = {
                "Motor_A": Motor_A,
                "Motor_B": mech_result["Motor_B"],
                "Motor_C": mech_result["Motor_C"],
                "Motor_ALL": mech_result["Motor_ALL"],
            }
            mech_motor_sep["Mech_Motor_A_I"] = Mech_Motor_A_I
            mech_motor_sep["Mech_Motor_B_I"] = origin_params.get("Mech_Motor_B_I", {})
            mech_motor_sep["Mech_Motor_C_I"] = origin_params.get("Mech_Motor_C_I", {})
        # 线圈数据
        Coil_ALL = mech_result.get("Coil_ALL")
        if not Coil_ALL:
            d_sensor_data = dict()
        else:
            # 有线圈数据，即有机器传感器数据
            d_sensor_data["Mech_Results"] = {
                "Coil_ALL": Coil_ALL,
                "Coil_A": mech_result["Coil_A"],
                "Coil_B": mech_result["Coil_B"],
                "Coil_C": mech_result["Coil_C"],
            }
            Mech_On_Coil_A_I = origin_params.get("Mech_On_Coil_A_I", {})
            create_time = datetime_from_str(Mech_On_Coil_A_I["acqtime"])
            d_sensor_data["create_date"] = create_time
            d_sensor_data["update_date"] = create_time
            d_sensor_data["Mech_On_Coil_A_I"] = Mech_On_Coil_A_I
            d_sensor_data["Mech_Off_Coil_A_0_I"] = origin_params.get(
                "Mech_Off_Coil_A_0_I", {}
            )
            d_sensor_data["Mech_Off_Coil_A_1_I"] = origin_params.get(
                "Mech_Off_Coil_A_1_I", {}
            )
            d_sensor_data["Mech_CT_A_I"] = origin_params.get("Mech_CT_A_I", {})
            d_sensor_data["Mech_DIS_A_I"] = origin_params.get("Mech_DIS_A_I", {})
            d_sensor_data["Mech_Switch_A_St"] = origin_params.get(
                "Mech_Switch_C_St", {}
            )

            d_sensor_data["Mech_On_Coil_B_I"] = origin_params.get(
                "Mech_On_Coil_B_I", {}
            )
            d_sensor_data["Mech_Off_Coil_B_0_I"] = origin_params.get(
                "Mech_Off_Coil_B_0_I", {}
            )
            d_sensor_data["Mech_Off_Coil_B_1_I"] = origin_params.get(
                "Mech_Off_Coil_B_1_I", {}
            )
            d_sensor_data["Mech_CT_B_I"] = origin_params.get("Mech_CT_B_I", {})
            d_sensor_data["Mech_DIS_B_I"] = origin_params.get("Mech_DIS_B_I", {})
            d_sensor_data["Mech_Switch_B_St"] = origin_params.get(
                "Mech_Switch_B_St", {}
            )

            d_sensor_data["Mech_On_Coil_C_I"] = origin_params.get(
                "Mech_On_Coil_C_I", {}
            )
            d_sensor_data["Mech_Off_Coil_C_0_I"] = origin_params.get(
                "Mech_Off_Coil_C_0_I", {}
            )
            d_sensor_data["Mech_Off_Coil_C_1_I"] = origin_params.get(
                "Mech_Off_Coil_C_1_I", {}
            )
            d_sensor_data["Mech_CT_C_I"] = origin_params.get("Mech_CT_C_I", {})
            d_sensor_data["Mech_DIS_C_I"] = origin_params.get("Mech_DIS_C_I", {})
            d_sensor_data["Mech_Switch_C_St"] = origin_params.get(
                "Mech_Switch_C_St", {}
            )
            # Mech_sep no alarm for now
            d_sensor_data["alarm_flag"] = AlarmFlag.NO_PUSH.value
            d_sensor_data["alarm_level"] = AlarmLevel.NORMAL.value
            d_sensor_data["alarm_describe"] = 0
        return d_sensor_data, mech_motor_sep

    @classmethod
    def ws_send_sensor_data(
        cls,
        sensor_data: Optional[dict],
        alarm_info: Optional[dict],
        mech_motor_data: Optional[dict],
        mech_motor_sep_data: Optional[dict],
    ):
        data = {"alarm_broadcast": False, "latest3_alarm": False}
        customer_id, site_id, equipment_id, point_id = "", "", "", ""
        customer_name, site_name, equipment_name, point_name = "", "", "", ""
        # assemble sensor data in sensor_list
        if sensor_data:
            sensor_data["has_sensor_data"] = True
            # alarm_describe_num = sensor_data.get("alarm_describe", 0)
            # sensor_data["alarm_describe"] = ALARM_DESCRIBE_DICT.get(
            #     alarm_describe_num, ""
            # )
            customer_id = sensor_data["customer_id"]
            site_id = sensor_data["site_id"]
            equipment_id = sensor_data["equipment_id"]
            point_id = sensor_data["point_id"]
            data["customer_id"] = customer_id
            data["site_id"] = site_id
            data["equipment_id"] = equipment_id
            data["point_id"] = point_id
            customer_name = cls.get_customer_name(str(customer_id))
            site_name = cls.get_site_name(site_id)
            equipment_name = cls.get_equipment_name(equipment_id)
            point_name = cls.get_point_name(point_id)
            sensor_data["customer_name"] = customer_name
            sensor_data["site_name"] = site_name
            sensor_data["equipment_name"] = equipment_name
            sensor_data["point_name"] = point_name
            sensor_data["sensor_data_id"] = sensor_data["_id"]
            sensor_type = sensor_data["sensor_type"]
            if sensor_type in SensorType.single_value_sensors():
                sensor_data["character_value"] = sensor_data["amplitude"]
            elif sensor_type in SensorType.partial_discharge_sensors():
                sensor_data["character_value"] = sensor_data["peak"]
            elif sensor_type == SensorType.temp:
                sensor_data["character_value"] = sensor_data["T"]
            elif sensor_type == SensorType.mech:
                # 机械特性 共体
                sensor_data["character_value"] = (
                    sensor_data.get("Mech_Results", {})
                    .get("CoilA", {})
                    .get("ON_OFF_STATE", 0)
                )
            elif sensor_type == SensorType.mech_sep:
                # 机械特性 分体
                sensor_data["character_value"] = (
                    sensor_data.get("Mech_Results", {})
                    .get("Coil_ALL", {})
                    .get("ON_OFF_STATE", 0)
                )
            data["sensor_data"] = bson_to_dict(sensor_data)
            s_character_value = data["sensor_data"]["character_value"]
            if s_character_value:
                data["sensor_data"]["character_value"] = float(s_character_value)
        if alarm_info:
            customer_id = alarm_info["customer_id"]
            site_id = alarm_info["site_id"]
            equipment_id = alarm_info["equipment_id"]
            point_id = alarm_info["point_id"]
            data["customer_id"] = customer_id
            data["site_id"] = site_id
            data["equipment_id"] = equipment_id
            data["point_id"] = point_id
            alarm_flag = alarm_info["alarm_flag"]
            # if alarm_flag == AlarmFlag.PUSH.value:
            #     data["alarm_broadcast"] = True
            alarm_level = alarm_info["alarm_level"]
            if alarm_level in AlarmLevel.abnormal_alarm_level():
                # 当前版本根据alarm_level(1,2)来判断是否进行弹框全局显示报警
                data["alarm_broadcast"] = True
                alarm_info["customer_name"] = cls.get_customer_name(str(customer_id))
                alarm_info["site_name"] = site_name
                alarm_info["equipment_name"] = equipment_name
                alarm_info["point_name"] = point_name
                site_unprocessed_key = f"{SITE_UNPROCESSED_NUM}{site_id}"
                unprocessed_num = normal_redis.get(site_unprocessed_key)
                c_abnormal_info = (
                    AbnormalCacheService.get_customer_abnormal_count_infos(customer_id)
                )
                s_abnormal_info = AbnormalCacheService.get_site_abnormal_count_infos(
                    site_id
                )
                abnormal_count_info = {
                    "customer_abnormal_info": c_abnormal_info,
                    "site_abnormal_info": s_abnormal_info,
                }
                data["alarm_data"] = bson_to_dict(alarm_info)
                data["unprocessed_num"] = unprocessed_num
                data["abnormal_count_info"] = abnormal_count_info
                data["latest3_alarm"] = True
        if mech_motor_data:
            customer_id = mech_motor_data["customer_id"]
            data["customer_id"] = customer_id
            data["site_id"] = mech_motor_data["site_id"]
            data["equipment_id"] = mech_motor_data["equipment_id"]
            data["point_id"] = mech_motor_data["point_id"]
            if sensor_data:
                mech_motor_data["site_name"] = site_name
                mech_motor_data["equipment_name"] = equipment_name
                mech_motor_data["point_name"] = point_name
                mech_motor_data["customer_name"] = customer_name
            else:
                mech_motor_data["customer_name"] = cls.get_customer_name(
                    str(customer_name)
                )
                mech_motor_data["site_name"] = cls.get_site_name(data["site_id"])
                mech_motor_data["equipment_name"] = cls.get_equipment_name(
                    data["equipment_id"]
                )
                mech_motor_data["point_name"] = cls.get_point_name(data["point_id"])
            data["mech_motor_data"] = bson_to_dict(mech_motor_data)
        if mech_motor_sep_data:
            customer_id = mech_motor_sep_data["customer_id"]
            data["customer_id"] = customer_id
            data["site_id"] = mech_motor_sep_data["site_id"]
            data["equipment_id"] = mech_motor_sep_data["equipment_id"]
            data["point_id"] = mech_motor_sep_data["point_id"]
            if sensor_data:
                mech_motor_sep_data["site_name"] = site_name
                mech_motor_sep_data["equipment_name"] = equipment_name
                mech_motor_sep_data["point_name"] = point_name
                mech_motor_sep_data["customer_name"] = customer_name
            else:
                mech_motor_sep_data["customer_name"] = cls.get_customer_name(
                    str(customer_name)
                )
                mech_motor_sep_data["site_name"] = cls.get_site_name(data["site_id"])
                mech_motor_sep_data["equipment_name"] = cls.get_equipment_name(
                    data["equipment_id"]
                )
                mech_motor_sep_data["point_name"] = cls.get_point_name(data["point_id"])
            data["mech_motor_sep_data"] = bson_to_dict(mech_motor_sep_data)
        if customer_id:
            ws_service = WsSensorDataSend(customer_id)
            ws_service.ws_send(code=Uri2C.S2C_SENSOR_DATA.value, data=data)

    @classmethod
    def ws_send_alarm_data(cls, alarm_info: AlarmInfo):
        """
        传感器报警数据推送，不包含测点报警
        :param alarm_info:
        :return:
        """
        alarm_info_dict = alarm_info.to_dict()
        alarm_info_dict["alarm_id"] = alarm_info_dict["_id"]
        customer_id = alarm_info_dict["customer_id"]
        site_id = alarm_info_dict["site_id"]
        equipment_id = alarm_info_dict["equipment_id"]
        point_id = alarm_info_dict["point_id"]
        equipment_name = cls.get_equipment_name(equipment_id)
        point_name = cls.get_point_name(point_id)
        alarm_info_dict["equipment_name"] = equipment_name
        alarm_info_dict["point_name"] = point_name
        alarm_info_dict["site_name"] = cls.get_site_name(site_id)
        alarm_info_dict["customer_name"] = cls.get_customer_name(str(customer_id))
        # 传感器报警不用这些数据
        # site_unprocessed_key = f"{SITE_UNPROCESSED_NUM}{site_id}"
        # unprocessed_num = normal_redis.get(site_unprocessed_key)
        # c_abnormal_info = AbnormalCacheService.get_customer_abnormal_count_infos(
        #     customer_id
        # )
        # s_abnormal_info = AbnormalCacheService.get_site_abnormal_count_infos(site_id)
        # abnormal_count_info = {
        #     "customer_abnormal_info": c_abnormal_info,
        #     "site_abnormal_info": s_abnormal_info,
        # }
        data = {
            "customer_id": customer_id,
            "site_id": site_id,
            "equipment_id": equipment_id,
            "point_id": point_id,
            "alarm_data": alarm_info_dict,
            # "unprocessed_num": unprocessed_num,
            # "abnormal_count_info": abnormal_count_info,
            "latest3_alarm": False,
            "alarm_broadcast": False,
        }
        ws_service = WsSensorDataSend(customer_id)
        ws_service.ws_send(code=Uri2C.S2C_ALARM_DATA.value, data=data)

    @classmethod
    @lru_cache(maxsize=128)
    def get_equipment_name(cls, equipment_id: str) -> str:
        return ElectricalEquipment.objects.get(pk=equipment_id).device_name

    @classmethod
    @lru_cache(maxsize=128)
    def get_point_name(cls, point_id: str) -> str:
        return MeasurePoint.objects.get(pk=point_id).measure_name

    @classmethod
    def get_alarm_block_info(cls, point_id: str) -> dict:
        point = MeasurePoint.objects.get(pk=point_id)
        return {
            "enable_block_alarm": point.enable_block_alarm,
            "enable_alarm_restraint": point.enable_alarm_restraint,
            "alarm_restraint_info": point.alarm_restraint_info,
            "restraint_flag": point.restraint_flag,
            "restraint_deadline": point.restraint_deadline,
        }

    @classmethod
    @lru_cache(maxsize=128)
    def get_site_name(cls, site_id: str) -> str:
        return Site.objects.get(pk=site_id).name

    @classmethod
    @lru_cache(maxsize=128)
    def get_customer_name(cls, customer_id: str) -> str:
        return Customer.objects.get(pk=customer_id).name


if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "cloud.settings")
    logging.info(
        "************************* start to run sync_pop_subscribe_message ********************"
    )
    SyncPopMsg.run()
