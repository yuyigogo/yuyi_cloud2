# For constants defined
from enum import Enum, unique
from functools import lru_cache

B = 1
KB = B * (2 ** 10)
MB = KB * (2 ** 10)

# max token expire days
MAX_EXPIRE_DAYS = 15
TOKEN_EXPIRE = 10 * 24 * 60 * 60
# max file size
MAX_FILE_SIZE = 10 * MB

# model name's max length const
MAX_LENGTH_NAME = 200

DD_MM_YY = "%d/%m/%Y"
MM_DD_YY = "%m/%d/%Y"
YY_MM_DD = "%Y/%m/%d"
DD_MONTH_YY = "%d %m %Y"

EN_MONTH_ABBREVIATION = {
    1: "January",
    2: "February",
    3: "March",
    4: "April",
    5: "May",
    6: "June",
    7: "July",
    8: "August",
    9: "September",
    10: "October",
    11: "November",
    12: "December",
}

EN_MONTH_ABBREVIATION_REVERSE = {v: k for k, v in EN_MONTH_ABBREVIATION.items()}

FR_MONTH_ABBREVIATION = {
    1: "janvier",
    2: "février",
    3: "mars",
    4: "avril",
    5: "mai",
    6: "juin",
    7: "juillet",
    8: "août",
    9: "septembre",
    10: "octobre",
    11: "novembre",
    12: "décembre",
}

FR_MONTH_ABBREVIATION_REVERSE = {v: k for k, v in FR_MONTH_ABBREVIATION.items()}

MONTH_LANG_ABBREVIATION = {"en": EN_MONTH_ABBREVIATION, "fr": FR_MONTH_ABBREVIATION}

MONTH_LANG_ABBREVIATION_REVERSE = {
    "en": EN_MONTH_ABBREVIATION_REVERSE,
    "fr": FR_MONTH_ABBREVIATION_REVERSE,
}

DATE_FORMAT_DICT = {
    "dd_mm_yy": DD_MM_YY,
    "mm_dd_yy": MM_DD_YY,
    "yy_mm_dd": YY_MM_DD,
    "month_abbreviation": DD_MONTH_YY,
}

MONTH_LANG_FORMAT = {"en": MM_DD_YY, "fr": DD_MM_YY}

"""set the date in database by language"""
DATE_FMT = {"fr": "%d/%m/%Y", "en": "%m/%d/%Y"}

DATE_FORMAT_EN = "%m/%d/%Y"  # format of date data in database

FORMULA_DATE_FORMAT = "%m/%d/%Y"
IP_MAX_ATTEMPTS = 20
PASSWORD_MAX_ATTEMPTS = 5


class BaseEnum(Enum):
    @classmethod
    def is_valid(cls, value):
        """
        >>> class E(BaseEnum):
        ...     x = "a"
        ...     y = "b"

        >>> E.is_valid("a")
        True
        >>> E.is_valid("c")
        False
        >>> E.is_valid("x")
        False

        check is value is a valid value
        :param value:
        :return: True if valid else False
        """
        return value in cls._value2member_map_

    @classmethod
    @lru_cache
    def values(cls):
        """
        >>> class E(BaseEnum):
        ...     x = "a"
        ...     y = "b"

        >>> E.values()
        ['a', 'b']

        get all values of class
        :return: values list
        """
        return [x.value for x in cls]

    @classmethod
    def is_sup_list(cls, values: list) -> bool:
        """Report whether another list contains this list."""
        return set(values).issubset(set(cls.values()))

    @classmethod
    def to_dict(cls):
        return {x.name: x.value for x in cls}


class RoleLevel(int, BaseEnum):
    """
    there are four roles in this system:
    cloud super admin: inner super administrator, manages all companies and all sites;
    client super admin: external super administrator, manages all companies and all sites;
    admin: an administrator who manages one company and some sites which belong to this company;
    normal: normal user under a certain company site.
    """

    CLOUD_SUPER_ADMIN = 0
    CLIENT_SUPER_ADMIN = 10
    ADMIN = 20
    NORMAL = 30

    @classmethod
    def allowed_role_level(cls):
        return [cls.CLIENT_SUPER_ADMIN.value, cls.ADMIN.value, cls.NORMAL.value]


ROLE_DICT = {
    RoleLevel.CLOUD_SUPER_ADMIN: "超级管理员",
    RoleLevel.CLIENT_SUPER_ADMIN: "超级管理员",
    RoleLevel.ADMIN: "管理员",
    RoleLevel.NORMAL: "普通用户",
}

ALL = "ALL"
MAX_MESSAGE_LENGTH = 2000


class SensorType(str, BaseEnum):
    """传感器/测点类型"""

    ae = "AE"  # 超声波局放
    tev = "TEV"  # 暂态地电压局放
    uhf = "UHF"  # 特高频局放
    temp = "TEMP"  # 温度传感器
    mech = "MECH"  # 断路器机械特性-共体
    hfct = "HFCT"  # 高频电流局放
    vibration = "VIBRATION"  # 振动
    ozone = "OZONE"  # 臭氧
    devtemp = "DEVTEMP"  # 设备温度
    envtemp = "ENVTEMP"  # 环境温度
    envth = "ENVTH"  # 环境湿度
    ct = "CT"  # 电流传感器
    gastemp = "GASTEMP"  # 气体温度传感器
    gaspress = "GASPRESS"  # 气体压力传感器
    gasdensity = "GASDENSITY"  # 气体密度传感器
    gasmoist = "GASMOIST"  # 气体水分传感器
    mech_sep = "MECHSEP"  # 断路器机械特性-分体separation

    @classmethod
    def partial_discharge_sensors(cls):
        # 局部放电传感器
        return [cls.ae.value, cls.tev.value, cls.uhf.value, cls.hfct.value]

    @classmethod
    def single_value_sensors(cls):
        return [
            cls.ozone.value,
            cls.devtemp.value,
            cls.envth.value,
            cls.envtemp.value,
            cls.ct.value,
            cls.gastemp.value,
            cls.gaspress.value,
            cls.gasdensity.value,
            cls.gasmoist.value,
        ]

    @classmethod
    def mech_sensors(cls):
        # 所有的机器特性传感器
        return [cls.mech.value, cls.mech_sep.value]


class DeviceType(str, BaseEnum):
    """设备类型"""

    switch_cabinet = "开关柜"
    gis = "GIS"
    voltage_transformer = "变压器"
    electric_cable = "电缆"


class VoltageLevel(str, BaseEnum):
    """电压等级"""

    k6 = "6kV"
    k1 = "10kV"
    k20 = "20kV"
    k35 = "35kV"
    k66 = "66kV"
    k110 = "110kV"
    k220 = "220kV"
    k330 = "330kV"
    k500 = "500kV"
    k750 = "750kV"
    k1000 = "1000kV"
    k1100 = "±1100kV"
    _k500 = "±500kV"
    _k600 = "±600kV"
    _k800 = "±800kV"


MODEL_KEY_TO_SENSOR_TYPE = {
    "2000000000000001": SensorType.uhf.value,
    "2000000000000002": SensorType.hfct.value,
    "2000000000000003": SensorType.tev.value,
    "2000000000000004": SensorType.ae.value,
    "0000000000000005": SensorType.mech.value,
    "2000000000000010": SensorType.vibration.value,
    "2000000000000011": SensorType.ozone.value,
    "2000000000000012": SensorType.devtemp.value,
    "2000000000000013": SensorType.envtemp.value,
    "2000000000000014": SensorType.envth.value,
    "0000000000000004": SensorType.temp.value,
    "0000000000000016": SensorType.ct.value,
    "0000000000000017": SensorType.gastemp.value,
    "0000000000000018": SensorType.gaspress.value,
    "0000000000000019": SensorType.gasdensity.value,
    "0000000000000020": SensorType.gasmoist.value,
    "2000000000000005": SensorType.mech_sep.value,
}


class AlarmFlag(int, BaseEnum):
    """报警上送"""

    NO_PUSH = 0
    PUSH = 1


class AlarmLevel(int, BaseEnum):
    """报警等级 0: 正常，1：预警，2：报警"""

    NORMAL = 0
    WARNING = 1
    ALARM = 2

    @classmethod
    def abnormal_alarm_level(cls):
        return [cls.WARNING.value, cls.ALARM.value]


class AlarmType(int, BaseEnum):
    """报警类型：1：传感器报警；2：测点报警"""

    SENSOR_ALARM = 1
    POINT_ALARM = 2


class SensorOnline(int, BaseEnum):
    """传感器是否支持在线模式： 0：不支持；1：支持"""

    CAN_NOT_ONLINE = 0
    CAN_ONLINE = 1


class SensorCommunicationMode(str, BaseEnum):
    LORA = "国网LoRa"
    LORAWAN = "LoRaWan"
    MHZ433 = "433MHz"
    NB = "NB-IoT"
    RS485 = "RS485"
    ETHERNET = "LAN"
    WIFI = "WIFI"
    BLUETOOTH = "Bluetooth"

    @classmethod
    def support_online_types(cls) -> list:
        return [cls.RS485.value, cls.ETHERNET.value, cls.WIFI.value]


class SensorWorkMode(int, BaseEnum):
    TIMER_MODE = 0
    ONLINE_MODE = 1


# 测点报警编码
ALARM_DESCRIBE_DICT = {
    0: "正常",
    1: "尖端放电",
    2: "悬浮放电",
    3: "沿面放电",
    4: "内部放电",
    5: "颗粒放电",
    6: "外部干扰",
    7: "其他",
    100: "阈值报警",  # alarm_type=1
    101: "趋势报警",  # alarm_type=2
}


class SensorAlarmDesc(int, BaseEnum):
    # 传感器报警编码
    normal = 0
    low_power = 2
    offline = 3
    other = 255  # (0xff)


class AlarmRestraintKeys(str, BaseEnum):
    ALARM_DURATION = "alarm_duration"
    MAX_ALARM_NUMS = "max_alarm_nums"
    PUSH_NO_ALARM_MINUTES = "push_no_alarm_minutes"

# ********************************************* redis keys info begin *************************************
# store in redis db5---->normal key
# store sensor_info,it's hset like:
# {"sensor_id": {"site_id": "xxx", "equipment_id": xxx", "measure_point_id": "xxx", "model_key": "xxx"}}


SENSOR_INFO_PREFIX = "cloud_sensor_info:sensor_id:"
SITE_UNPROCESSED_NUM = "site_unprocessed_num:"  # "site_unprocessed_num:xxx" 12

CLIENT_IDS = "cloud:client_ids:set"
CLIENT_ID_SENSOR_IDS_PREFIX = (
    "cloud:client_id_sensor_ids:"  # str:store sensor_ids in its gateway
)
# 网关下传感器列表缓存
GATEWAY_SENSORS_TOPIC_PREFIX = "cloud:gateway_id:"

# day/week/month abnormal count,
# it's hset like: {customer_day_abnormal_info:customer_id: {"alarm_num": 20, "processed_num": 2}}
customer_day_abnormal_info = "customer_day_abnormal_info:"
customer_week_abnormal_info = "customer_week_abnormal_info:"
customer_month_abnormal_info = "customer_month_abnormal_info:"

site_day_abnormal_info = "site_day_abnormal_info:"
site_week_abnormal_info = "site_week_abnormal_info:"
site_month_abnormal_info = "site_month_abnormal_info:"

CLOUD_SUBSCRIBE_MSG_LIST = "cloud:subscribe:msg:list"

# ws in redis infos
WS_CUSTOMER_ID_SET = "ws_customer_id_set:"  # connected ws users, like ws_customer_id_set:xxx: {ws_user1, ws_user2}


# ********************************************* redis keys info end *************************************


class MsgQueueType(str, BaseEnum):
    """need to deal with different type of matched msg which subscribed from mqtt in redis list"""

    DATA_LOADER = "data_loader"  # 采集数据上传
    SENSOR_ALARM = "sensor_alarm"  # 传感器报警
    SENSORS_IN_GATEWAY = "sensors_in_gateway"  # 传感器列表
    SENSOR_PERIOD = "sensor_period"  # 采集周期
    SENSOR_WORK_MODE = "sensor_work_mode"  # 工作模式


# constant topics which are need to subscribe or publish
# 网关下传感器列表主题
BASE_GATEWAY_SUBSCRIBE_TOPIC = "/serivice_reply/sub_get"
BASE_GATEWAY_PUBLISH_TOPIC = "/serivice/sub_get"

# 采集周期设置主题
BASE_SENSOR_SAMPLE_PERIOD_PUBLISH_TOPIC = "/common/service/sample_period_set"
BASE_SENSOR_SAMPLE_PERIOD_SUBSCRIBE_TOPIC = "/common/service_reply/sample_period_set"

# 工作模式设置主题
BASE_SENSOR_WORK_MODE_PUBLISH_TOPIC = "/common/service/work_mode_set"
BASE_SENSOR_WORK_MODE_SUBSCRIBE_TOPIC = "/common/service_reply/work_mode_set"
