import logging
from time import time

logger = logging.getLogger(__name__)


def profile(func):
    """Log time needed to run a method"""

    def wrap(*args, **kwargs):
        started_at = time()
        log_prefix = ""
        if args and isinstance(args[0], object):
            log_prefix = "%s." % args[0].__class__.__name__
        result = func(*args, **kwargs)
        logger.info(
            "Profiling method [%s%s]: %f s",
            log_prefix,
            func.__name__,
            time() - started_at,
        )
        return result

    return wrap
