import random
import string
from io import BytesIO

from PIL import Image, ImageDraw, ImageFont

from common.storage.redis import normal_redis


class Captcha:
    CAPTCHA_KEY_PREFIX = "captcha_"

    @classmethod
    def generate_captcha(cls):
        characters = string.ascii_letters + string.digits
        captcha_text = "".join(random.choices(characters, k=6))

        # create img
        width, height = 160, 60
        image = Image.new("RGB", (width, height), (255, 255, 255))
        draw = ImageDraw.Draw(image)

        font_path = "./fonts/Arial.ttf"
        font = ImageFont.truetype(font_path, 40)
        # 绘制文本
        for i in range(6):
            draw.text((10 + i * 25, 10), captcha_text[i], font=font, fill=(0, 0, 0))

        # 添加一些噪点
        for _ in range(20):
            x1 = random.randint(0, width)
            y1 = random.randint(0, height)
            draw.line(
                ((x1, y1), (x1 + random.randint(0, 10), y1 + random.randint(0, 10))),
                fill=(0, 0, 0),
            )

        buffer = BytesIO()
        image.save(buffer, "jpeg")
        image_data = buffer.getvalue()
        return captcha_text, image_data

    @classmethod
    def store_captcha(cls, client_ip: str, cap_code: str):
        cap_key = cls.CAPTCHA_KEY_PREFIX + client_ip
        normal_redis.set(cap_key, cap_code, ex=60)

    @classmethod
    def is_captcha_validate(cls, client_ip: str, cap_code: str) -> bool:
        cap_key = cls.CAPTCHA_KEY_PREFIX + client_ip
        code = normal_redis.get(cap_key)
        validate = False
        if code and code.lower() == cap_code.lower():
            validate = True
        return validate

    @classmethod
    def delete_validated_code(cls, client_ip: str):
        cap_key = cls.CAPTCHA_KEY_PREFIX + client_ip
        normal_redis.delete(cap_key)


def get_client_ip(request) -> str:
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip
