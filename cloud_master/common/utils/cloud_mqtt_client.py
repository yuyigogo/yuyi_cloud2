import json
import logging
from typing import List, Tuple, Optional

import paho.mqtt.client as mqtt_client
from bson import ObjectId

from cloud.settings import MQTT_CLIENT_CONFIG

logger = logging.getLogger(__name__)


def on_mqtt_connect(client, userdata, flags, rc):
    if rc == 0:
        logger.info(f"{client._client_id} success to connected to MQTT Broker!")
    else:
        logger.info(f"{client._client_id} failed to connect to MQTT Broker with {rc=}")


def mqtt_connect():
    client_id = str(ObjectId())
    client_instance = mqtt_client.Client(client_id)
    client_instance.on_connect = on_mqtt_connect
    client_instance.username_pw_set(MQTT_CLIENT_CONFIG["user"], MQTT_CLIENT_CONFIG["pw"])
    client_instance.connect(MQTT_CLIENT_CONFIG["host"], MQTT_CLIENT_CONFIG["port"], 60)
    client_instance.loop_start()
    return client_instance


def mqtt_publish(topic_payload_list: List[Tuple[str, Optional[dict]]]):
    """publish one topic or many topics"""
    client_instance = mqtt_connect()
    for (topic, payload) in topic_payload_list:
        if not payload:
            payload = {}
        payload.update({"id": "123", "version": "1.0"})
        client_instance.publish(topic, payload=json.dumps(payload))
        logging.info(f"succeed publish {topic=} with {payload=}")
    client_instance.loop_stop()


# if __name__ == "__main__":
#     topic = "/8E00130200000100/serivice/sub_get"
#     mqtt_publish([(topic, None)])


