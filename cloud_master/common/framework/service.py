import datetime
import json
import logging
from collections import defaultdict
from typing import List, Optional, Union

from cloud.celery import app
from cloud.models import bson_to_dict
from cloud.settings import MONGO_CLIENT
from cloud_home.models.status_statistics import (
    CAbnormalRatio,
    CAlarmCount,
    CStatusStatistic,
    SAbnormalRatio,
    SAlarmCount,
    SStatusStatistic,
)
from equipment_management.models.gateway import GateWay
from equipment_management.models.sensor_config import SensorConfig
from file_management.models.measure_point import MeasurePoint

from common.const import (
    CLIENT_ID_SENSOR_IDS_PREFIX,
    CLIENT_IDS,
    MODEL_KEY_TO_SENSOR_TYPE,
    SENSOR_INFO_PREFIX,
    SITE_UNPROCESSED_NUM,
    SensorType,
    customer_day_abnormal_info,
    customer_month_abnormal_info,
    customer_week_abnormal_info,
    site_day_abnormal_info,
    site_month_abnormal_info,
    site_week_abnormal_info,
)
from common.scheduler import cloud_task
from common.storage.redis import normal_redis, normal_redis_pipeline

logger = logging.getLogger(__name__)


@cloud_task
def delete_sensor_data_from_points(deleted_sensor_infos: dict):
    """
    when delete points, should delete alarm_info and sensor_data
    :return:
    """
    logger.info(
        f"start to run delete_sensor_data_from_points with {deleted_sensor_infos=}"
    )
    sensor_type_ids = defaultdict(list)
    for sensor_id, sensor_type in deleted_sensor_infos.items():
        sensor_type_ids[sensor_type].append(sensor_id)
    delete_filters = {
        "sensor_type": {"$in": list(deleted_sensor_infos.values())},
        "sensor_id": {"$in": list(deleted_sensor_infos.keys())},
    }
    alarm_info_col = MONGO_CLIENT["alarm_info"]
    alarm_info_col.delete_many(delete_filters)
    logger.info(f"delete alarm_infos successfully in delete_sensor_data_from_points")
    for sensor_type, _sensor_ids in sensor_type_ids.items():
        try:
            mongo_col = MONGO_CLIENT[sensor_type]
            mongo_col.delete_many({"sensor_id": {"$in": _sensor_ids}})
            logger.info(
                f"delete {sensor_type=} successfully in delete_sensor_data_from_points with {sensor_type=}, {_sensor_ids=}"
            )
        except Exception as e:
            logger.error(
                f"delete sensor data failed with {sensor_type=}, {_sensor_ids=},{e=}",
                exc_info=True,
            )
    logger.info(
        f"run delete_sensor_data_from_points successfully with {deleted_sensor_infos=}"
    )


class BaseService(object):
    """
    All service should inherit this class.
    """

    @classmethod
    def delete_points(cls, points, clear_resource=False, delete_sensor_config=False):
        """
        when delete_sensor_config is false, only need set corresponding sensor_config's has_bound to false,
        otherwise, should delete corresponding sensor_config
        """
        deleted_sensor_infos = dict(
            (points.values_list("sensor_number", "measure_type"))
        )
        cls.delete_sensor_config_resource(
            list(deleted_sensor_infos.keys()), delete_sensor_config
        )
        points.delete()
        if clear_resource:
            app.send_task(
                "common.framework.service.delete_sensor_data_from_points",
                args=(deleted_sensor_infos,),
            )

    @classmethod
    def delete_sensor_data_from_gateway(cls, gateway: GateWay, clear_resource=False):
        client_id = gateway.client_number
        if clear_resource:
            # delete alarm_info
            sensor_ids = gateway.sensor_ids
            model_keys = gateway.model_keys
            if sensor_ids:
                app.send_task(
                    "equipment_management.tasks.delete_data_from_gateway",
                    args=(client_id, sensor_ids, model_keys),
                )
        gateway.delete()
        # delete client_ids
        normal_redis.srem(CLIENT_IDS, client_id)
        # delete client_id_sensor_ids in redis
        cls.delete_client_id_sensor_ids_info_in_redis([client_id])
        # delete sensor_config and sensor_infos in redis
        sensor_ids = list(
            SensorConfig.objects(client_number=client_id).values_list("sensor_number")
        )
        if sensor_ids:
            cls.delete_sensor_config_resource(sensor_ids, delete_sensor_config=True)

    @classmethod
    def delete_client_id_sensor_ids_info_in_redis(cls, client_ids: list):
        keys = [f"{CLIENT_ID_SENSOR_IDS_PREFIX}{client_id}" for client_id in client_ids]
        normal_redis.delete(*keys)

    @classmethod
    def remove_client_ids_from_redis(cls, client_ids: list):
        normal_redis.srem(CLIENT_IDS, *client_ids)

    @classmethod
    def get_latest_sensor_info(cls, sensor_number: str, sensor_type: str) -> dict:
        mongo_col = MONGO_CLIENT[sensor_type]
        sensor_data = mongo_col.find_one(
            {"sensor_id": sensor_number, "is_latest": True},
        )
        return bson_to_dict(sensor_data) if sensor_data else {}

    @classmethod
    def delete_customer_status_infos(cls, customer_id: str):
        CStatusStatistic.objects.filter(customer_id=customer_id).delete()
        CAbnormalRatio.objects.filter(customer_id=customer_id).delete()
        CAlarmCount.objects.filter(customer_id=customer_id).delete()
        delete_keys = [
            f"{customer_day_abnormal_info}{customer_id}",
            f"{customer_week_abnormal_info}{customer_id}",
            f"{customer_month_abnormal_info}{customer_id}",
        ]
        try:
            normal_redis.delete(*delete_keys)
        except Exception as e:
            logger.exception(
                f"delete customer resource for {delete_keys=} exception with {e=}"
            )

    @classmethod
    def delete_sites_status_infos(cls, site_ids: List[str]):
        SStatusStatistic.objects.filter(site_id__in=site_ids).delete()
        SAbnormalRatio.objects.filter(site_id__in=site_ids).delete()
        SAlarmCount.objects.filter(site_id__in=site_ids).delete()
        delete_keys = []
        for site_id in site_ids:
            delete_keys.extend(
                [
                    f"{SITE_UNPROCESSED_NUM}{site_id}"
                    f"{site_day_abnormal_info}{site_id}",
                    f"{site_week_abnormal_info}{site_id}",
                    f"{site_month_abnormal_info}{site_id}",
                ]
            )
        if delete_keys:
            normal_redis.delete(*delete_keys)

    @classmethod
    def delete_sensor_config_resource(
        cls, sensor_ids: Union[list, set], delete_sensor_config=True
    ):
        """
        when delete point, should delete the corresponding sensor_config and sensor_info in redis
        """
        sensor_configs = SensorConfig.objects.filter(sensor_number__in=sensor_ids)
        delete_sensor_info_keys = [
            f"{SENSOR_INFO_PREFIX}{sensor_id}" for sensor_id in sensor_ids
        ]
        if delete_sensor_config is False:
            sensor_configs.update(has_bound=False)
            for k in delete_sensor_info_keys:
                normal_redis_pipeline.hmset(k, {"has_bound": "False"})
            normal_redis_pipeline.execute()
        else:
            sensor_configs.delete()
            normal_redis.delete(*delete_sensor_info_keys)

    @classmethod
    def update_point_online_info(
        cls,
        point_id: str,
        is_online: bool,
        acq_period: int,
        last_received_time: datetime.datetime,
    ):
        info = {
            "is_online": is_online,
            "acq_period": acq_period,
            "last_received_time": last_received_time,
        }
        logger.info(f"update_point_online_info with {point_id=}, {info=}")
        MeasurePoint.objects.filter(id=point_id).update(**info, upsert=True)

    @classmethod
    def get_point_is_online_by_sensor_ids(cls, sensor_ids: list) -> dict:
        return {
            p.sensor_number: {
                "is_online": p.is_online,
                "last_received_time": bson_to_dict(p.last_received_time),
            }
            for p in MeasurePoint.objects.only(
                "is_online", "last_received_time", "sensor_number"
            ).filter(sensor_number__in=sensor_ids)
        }


class SensorConfigService(BaseService):
    def __init__(self, sensor_id: str):
        self.sensor_id = sensor_id
        self.sensor_info_key = f"{SENSOR_INFO_PREFIX}{sensor_id}"

    def sensor_config_operations_in_point_creation(
        self,
        customer_id: str,
        site_id: str,
        equipment_id: str,
        point_id: str,
        sensor_type: str,
    ):
        model_key = None
        sensor_config = SensorConfig.objects.filter(
            sensor_number=self.sensor_id
        ).first()
        has_bound = False
        if sensor_config:
            # 更新对应的档案信息且has_bound = True
            sensor_config.customer_id = customer_id
            sensor_config.site_id = site_id
            sensor_config.equipment_id = equipment_id
            sensor_config.point_id = point_id
            sensor_config.has_bound = True
            sensor_config.save()
            logger.info(
                f"sensor_config_operations_in_point_creation for {self.sensor_id=} success!"
            )
            model_key = sensor_config.model_key
            has_bound = True
        # no matter whether sensor_config exists, should always set sensor_info into redis
        self.set_sensor_info_to_redis(
            customer_id,
            site_id,
            equipment_id,
            point_id,
            sensor_type,
            has_bound,
            model_key=model_key,
        )

    def set_sensor_info_to_redis(
        self,
        customer_id: str,
        site_id: str,
        equipment_id: str,
        point_id: str,
        sensor_type: str,
        has_bound: bool = False,
        model_key: Optional[str] = None,
    ):
        value = {
            "customer_id": customer_id,
            "site_id": site_id,
            "equipment_id": equipment_id,
            "point_id": point_id,
            # add sensor_type to redis is for parsing data when compare model_key to sensor_type
            "sensor_type": sensor_type,
            "has_bound": str(has_bound),
        }
        if model_key:
            value["model_key"] = model_key
        normal_redis.hmset(self.sensor_info_key, value)
        logger.info(
            f"set ids to sensor_info in redis success with key:{self.sensor_info_key}, {value=}"
        )

    def sensor_config_operations_in_point_update(
        self,
        old_sensor_id: str,
        customer_id: str,
        site_id: str,
        equipment_id: str,
        point_id: str,
        sensor_type: str,
    ):
        # set has_bound to false for old sensor_config and update new sensor_config info
        SensorConfig.objects.filter(sensor_number=old_sensor_id).update(has_bound=False)
        key = SENSOR_INFO_PREFIX + old_sensor_id
        normal_redis.hmset(key, {"has_bound": "False"})
        self.sensor_config_operations_in_point_creation(
            customer_id, site_id, equipment_id, point_id, sensor_type
        )

    def get_sensor_info_from_redis(self) -> dict:
        return normal_redis.hgetall(self.sensor_info_key)

    def get_or_set_sensor_info_from_sensor_config(self) -> Optional[dict]:
        # not set sensor info to redis at here
        try:
            sensor_config = SensorConfig.objects.get(sensor_number=self.sensor_id)
        except Exception as e:
            logger.warning(f"get sensor_info failed for {self.sensor_id=} with {e=}")
            return
        customer_id = str(sensor_config.customer_id)
        site_id = str(sensor_config.site_id)
        equipment_id = str(sensor_config.equipment_id)
        point_id = str(sensor_config.point_id)
        model_key = sensor_config.model_key
        # self.set_sensor_info_to_redis(
        #     customer_id, site_id, equipment_id, point_id, sensor_type, model_key
        # )
        return {
            "customer_id": customer_id,
            "site_id": site_id,
            "equipment_id": equipment_id,
            "point_id": point_id,
            "model_key": model_key,
        }

    def get_or_set_sensor_info_from_point(self) -> Optional[dict]:
        try:
            point = MeasurePoint.objects.get(sensor_number=self.sensor_id)
        except Exception as e:
            logger.warning(f"get sensor_info failed for {self.sensor_id=} with {e=}")
            return
        customer_id = str(point.customer_id)
        site_id = str(point.site_id)
        equipment_id = str(point.equipment_id)
        point_id = str(point.id)
        self.set_sensor_info_to_redis(
            customer_id,
            site_id,
            equipment_id,
            point_id,
            point.measure_type,
            has_bound=True,
        )
        return {
            "customer_id": customer_id,
            "site_id": site_id,
            "equipment_id": equipment_id,
            "point_id": point_id,
        }

    @classmethod
    def deal_with_sensors_in_gateway_msg(cls, client_number: str, msg_dict: dict):
        """
        1.网关下传感器列表逻辑：
            a.SensorConfig的创建/删除只能在这(网关下传感器列表)；
            b.创建SensorConfig时，赋值model_key, client_number,has_bound = False,同时需要拿sensor_id去查测点信息，
            以赋值档案信息(point_id,site_id)；
            c.删除时，直接删除对应的SensorConfig,测点和数据不做任何修改；

        2.测点绑定传感器的逻辑：
            a.新建测点：拿sensor_id查询是否有SensorConfig,如果有，更新对应的档案信息且has_bound = True;
            b.修改测点: 将old_sensor_id对应的SensorConfig has_bound 更新为False，同时将new_sensor_id对应进行a操作；
            c.删除测点：SensorConfig的has_bound 更新为False，如果选择要清楚数据，将该sensor_id对应的数据清除；
        1. update GateWay's sensor_ids and model_keys
        2. 根据GateWay里面的sensor_ids，得出需要删除的SensorConfig
        3.set client_id_sensor_ids info in redis.
        场景：a.删除设备以上的档案，SensorConfig将删除；
            b.主机删除，SensorConfig将删除；
            c.档案不删除，只删除主机，然后再设置主机，这个时候应该从测点中获取SensorConfig；
            d.先设置主机，再绑定档案；
            e.先绑定档案，再设置主机
        :param client_number:
        :param msg_dict:
        :return:
        """
        logger.info(f"deal_with_sensors_in_gateway_msg for {client_number=}, {msg_dict=}")
        params = msg_dict.get("params", {})
        _sensor_ids = params.get("sensor", [])
        _model_keys = params.get("modelkey", [])
        if not _sensor_ids:
            return
        _sensor_id_model_key = list(zip(_sensor_ids, _model_keys))
        new_sensor_id_model_key_dict = {
            sensor_id: model_key
            for (sensor_id, model_key) in _sensor_id_model_key
            if model_key in MODEL_KEY_TO_SENSOR_TYPE
        }
        new_sensor_id_model_key_set = set(new_sensor_id_model_key_dict.items())
        logger.info(f"get sensors in gateway from mqtt with {new_sensor_id_model_key_set=}")
        gateway = GateWay.objects(client_number=client_number).first()
        if not gateway:
            logger.warning(f"can't find the gateway: {client_number=}")
            return
        old_sensor_ids = gateway.sensor_ids
        old_model_keys = gateway.model_keys
        old_sensor_id_model_key_set = set(zip(old_sensor_ids, old_model_keys))
        add_items = new_sensor_id_model_key_set - old_sensor_id_model_key_set
        delete_items = old_sensor_id_model_key_set - new_sensor_id_model_key_set
        logger.info(f"process sensors in mqtt with {old_sensor_id_model_key_set=}, {add_items=}, {delete_items=}")
        if delete_items:
            delete_sensor_ids = set(d_sensor for d_sensor, _ in delete_items)
            cls.delete_sensor_config_resource(delete_sensor_ids)
        if add_items:
            # create SensorConfig
            bulk_operations = []
            for sensor_id, model_key in add_items:
                s = SensorConfig(
                    sensor_number=sensor_id,
                    model_key=model_key,
                    client_number=client_number,
                    can_senor_online=1,
                    sensor_type=MODEL_KEY_TO_SENSOR_TYPE.get(model_key),
                )
                has_bound = False
                key = SENSOR_INFO_PREFIX + sensor_id
                sensor_info = normal_redis.hgetall(key)
                if not sensor_info:
                    sensor_info = cls(sensor_id).get_or_set_sensor_info_from_point()

                if sensor_info:
                    logger.info(f"has sensor_info in redis:{sensor_info=}")
                    s.customer_id = sensor_info.get("customer_id")
                    s.site_id = sensor_info.get("site_id")
                    s.equipment_id = sensor_info.get("equipment_id")
                    s.point_id = sensor_info.get("point_id")
                    has_bound = True
                    s.has_bound = has_bound
                bulk_operations.append(s)
                normal_redis_pipeline.hmset(
                    key, {"model_key": model_key, "has_bound": str(has_bound)}
                )
            # bulk insert
            SensorConfig.objects.insert(bulk_operations)
            logger.info(f"bulk insert sensor_config success with {bulk_operations=}")

        # update gateway
        new_sensor_ids = list(new_sensor_id_model_key_dict.keys())
        gateway.sensor_ids = new_sensor_ids
        gateway.model_keys = list(new_sensor_id_model_key_dict.values())
        gateway.save()
        logger.info(f"update gateway {client_number=} success!")
        # set client_id_sensor_ids info in redis
        client_sensor_ids_key = f"{CLIENT_ID_SENSOR_IDS_PREFIX}{client_number}"
        normal_redis_pipeline.set(client_sensor_ids_key, json.dumps(new_sensor_ids))
        normal_redis_pipeline.execute()
