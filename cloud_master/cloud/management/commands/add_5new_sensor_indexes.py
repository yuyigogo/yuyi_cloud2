from cloud.settings import MONGO_CLIENT
from django.core.management import BaseCommand

from common.const import SensorType


class Command(BaseCommand):
    help = "create indexes for CT/GASTEMP/GASPRESS/GASDENSITY/GASMOIST"

    def handle(self, *argcs, **options):
        """
        :param args:
        :param options:
        :return:
        """
        self.stdout.write(
            "You are creating indexes for CT/GASTEMP/GASPRESS/GASDENSITY/GASMOIST models"
        )

        indexes = [
            "client_number",
            "is_latest",
            "sensor_id",
            "is_online",
            "customer_id",
            "site_id",
            [("sensor_id", 1), ("is_latest", 1)],
            [("sensor_id", 1), ("create_date", 1)],
            [("point_id", 1), ("create_date", 1)],
        ]
        for model in [
            SensorType.ct.value,
            SensorType.gastemp.value,
            SensorType.gasmoist.value,
            SensorType.gaspress.value,
            SensorType.gasdensity.value,
        ]:
            for index in indexes:
                mongo_col = MONGO_CLIENT[model]
                mongo_col.create_index(index)

            self.stdout.write(f"create {model=} indexes success!")
