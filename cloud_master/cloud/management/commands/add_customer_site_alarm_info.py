from datetime import datetime

from alarm_management.models.alarm_info import AlarmInfo
from bson import ObjectId
from cloud_home.models.status_statistics import CAlarmCount, SAlarmCount
from customer.models.customer import Customer
from django.core.management import BaseCommand
from file_management.models.measure_point import MeasurePoint
from mongoengine import Q
from pymongo import UpdateOne
from sites.models.site import Site

from common.const import ALL, SITE_UNPROCESSED_NUM, AlarmLevel, AlarmType
from common.storage.redis import normal_redis


class Command(BaseCommand):
    help = "create CAlarmCount SAlarmCount data"

    def handle(self, *args, **options):
        """
        :param args:
        :param options:
        :return:
        """
        self.stdout.write("You are creating CAlarmCount SAlarmCount data")

        customer_ids = Customer.objects().values_list("id")
        customer_ids = list(map(str, customer_ids))
        customer_bulk_operations = []
        for customer_id in customer_ids:
            point_ids = MeasurePoint.objects.filter(
                customer_id=customer_id
            ).values_list("id")
            base_alarm_query = Q(
                alarm_type=AlarmType.POINT_ALARM.value,
                alarm_level__in=AlarmLevel.abnormal_alarm_level(),
                point_id__in=point_ids,
            )
            base_alarm = AlarmInfo.objects.filter(base_alarm_query)
            # 先查总数
            abnormal_num = base_alarm.count()
            processed_query = base_alarm_query & Q(is_processed=True)
            processed_num = base_alarm.filter(processed_query).count()
            customer_bulk_operations.append(
                UpdateOne(
                    {"customer_id": ObjectId(customer_id)},
                    {
                        "$set": {
                            "customer_id": ObjectId(customer_id),
                            "processed_num": processed_num,
                            "alarm_num": abnormal_num,
                            "create_date": datetime.now(),
                            "update_date": datetime.now(),
                        }
                    },
                    upsert=True,
                ),
            )

            self.stdout.write(f"create {customer_id=} CAlarmCount success!")
        collection = CAlarmCount._get_collection()
        collection.bulk_write(customer_bulk_operations, ordered=False)
        self.stdout.write(
            f"success to insert {len(customer_bulk_operations)} c_alarm_count for {customer_ids=}"
        )

        # site
        site_ids = Site.objects(name__ne=ALL).values_list("id")
        site_ids = list(map(str, site_ids))
        site_bulk_operations = []
        for site_id in site_ids:
            point_ids = MeasurePoint.objects.filter(site_id=site_id).values_list("id")
            base_alarm_query = Q(
                alarm_type=AlarmType.POINT_ALARM.value,
                alarm_level__in=AlarmLevel.abnormal_alarm_level(),
                point_id__in=point_ids,
            )
            base_alarm = AlarmInfo.objects.filter(base_alarm_query)
            # 先查总数
            abnormal_num = base_alarm.count()
            processed_query = base_alarm_query & Q(is_processed=True)
            processed_num = base_alarm.filter(processed_query).count()
            # 上面的6
            normal_redis.set(
                f"{SITE_UNPROCESSED_NUM}{site_id}", abnormal_num - processed_num
            )
            site_bulk_operations.append(
                UpdateOne(
                    {"customer_id": ObjectId(site_id)},
                    {
                        "$set": {
                            "site_id": ObjectId(site_id),
                            "processed_num": processed_num,
                            "alarm_num": abnormal_num,
                            "create_date": datetime.now(),
                            "update_date": datetime.now(),
                        }
                    },
                    upsert=True,
                ),
            )
            self.stdout.write(f"create {site_id=} SAlarmCount success!")

        if site_bulk_operations:
            collection = SAlarmCount._get_collection()
            collection.bulk_write(site_bulk_operations, ordered=False)
            self.stdout.write(
                f"success to insert {len(site_bulk_operations)} s_alarm_count for {site_ids=}"
            )
