from django.contrib.auth.hashers import make_password
from django.core.management import BaseCommand
from user_management.models.user import CloudUser


class Command(BaseCommand):
    """python manage.py change_password -u "yy" """

    help = "change user's password"

    def add_arguments(self, parser):
        parser.add_argument(
            "-u", "--username", type=str, required=True, help="user's name"
        )

    def handle(self, *args, **options):
        username = options["username"]
        self.stdout.write(f"You are changing {username}'s password......")

        if CloudUser.objects(username=username).count() == 0:
            self.stdout.write(f"{username} not existed!")
            return

        try:
            while True:
                password = input("Enter new password: ")
                password_confirm = input("Confirm new password: ")

                if password == password_confirm:
                    break
                else:
                    self.stdout.write("Passwords do not match. Please try again.")

            encrypted_password = make_password(password)
            CloudUser.objects(username=username).update(set__password=encrypted_password)
            self.stdout.write("Change password success!")
        except Exception as e:
            self.stdout.write(f"Change password error with {e}!")