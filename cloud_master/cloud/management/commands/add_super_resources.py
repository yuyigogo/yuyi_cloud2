from bson import ObjectId
from customer.models.customer import Customer
from customer.services.customer_service import CustomerService
from django.contrib.auth.hashers import make_password
from django.core.management import BaseCommand
from sites.models.site import Site
from user_management.models.user import CloudUser

from common.const import ALL, RoleLevel


class Command(BaseCommand):
    """python manage.py add_super_resources -e "1212@gmail.com" -u "aaaa" -pw '111111111'"""

    help = "create a super admin"

    def add_arguments(self, parser):
        parser.add_argument(
            "-u", "--username", type=str, required=True, help="super user's name"
        )
        parser.add_argument(
            "-pw", "--password", type=str, required=True, help="super user's password"
        )
        parser.add_argument(
            "-ph",
            "--phone",
            type=str,
            required=False,
            default="110",
            help="super user's phone",
        )
        parser.add_argument(
            "-e",
            "--email",
            type=str,
            required=False,
            default=f"{str(ObjectId())}@gmail.com",
            help="super user's email",
        )

    def handle(self, *args, **options):
        self.stdout.write("You are creating a super admin and named ALL customer/site")
        if CloudUser.objects(role_level=RoleLevel.CLOUD_SUPER_ADMIN).count() > 0:
            self.stdout.write("super admin has existed!")
            return
        administrative_division = {"province": "北京市", "city": "市辖区", "region": "东城区"}
        customer = Customer.objects(name=ALL).first()
        if customer is None:
            customer = CustomerService.create_customer(
                ALL,
                administrative_division=administrative_division,
                remarks="this is named ALL customer",
            )
        site = Site.objects(name=ALL).first()
        if site is None:
            site = Site(
                name=ALL,
                customer=customer.pk,
                administrative_division=administrative_division,
                voltage_level="220V",
                site_location=[0, 0],
                remarks="this is named all Site.",
            )
            site.save()
        username = options["username"]
        password = make_password(options["password"])
        phone = options["phone"]
        email = options["email"]
        super_user = CloudUser(
            username=username,
            password=password,
            customer=customer.pk,
            sites=[site.pk],
            phone=phone,
            email=email,
            role_level=RoleLevel.CLOUD_SUPER_ADMIN,
        )
        super_user.save()
        self.stdout.write("create success!")
