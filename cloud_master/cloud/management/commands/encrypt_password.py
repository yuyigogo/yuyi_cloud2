from django.contrib.auth.hashers import make_password
from django.core.management import BaseCommand
from user_management.models.user import CloudUser


class Command(BaseCommand):
    help = "encrypt user's password!"

    def handle(self, *args, **options):
        """
        :param args:
        :param options:
        :return:
        """
        self.stdout.write("You are encrypting user's password!")
        users = CloudUser.objects.filter()
        for user in users:
            try:
                old_password = user.password
                user.password = make_password(old_password)
                user.save()
            except Exception as e:
                self.stdout.write(
                    f"encrypting user's password error with {user.username},{e=}"
                )
        self.stdout.write("encrypting user's done!")
