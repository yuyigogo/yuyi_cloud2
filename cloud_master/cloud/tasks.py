import logging
from datetime import datetime

from dateutil.relativedelta import relativedelta

from alarm_management.models.alarm_info import AlarmInfo
from cloud.celery import app
from cloud.settings import DATA_SAVE_MONTH, MONGO_CLIENT
from common.const import SensorType
from common.scheduler import cloud_task
from sensor_data.models.mech_motor import MechMotor, MechMotorSep

logger = logging.getLogger(__name__)


# @cloud_task
# def test_task():
#     logger.info(f"Task executed at {datetime.now()}")


@cloud_task
def async_clear_cloud_data():
    logger.info("start to clear cloud data......")
    cutoff_date = datetime.now() - relativedelta(months=DATA_SAVE_MONTH)
    logger.info(f"deleting data before: {cutoff_date=}")
    alarm_count = AlarmInfo.objects(create_date__lt=cutoff_date).delete()
    mech_count = MechMotor.objects(create_date__lt=cutoff_date).delete()
    mech_sep_count = MechMotorSep.objects(create_date__lt=cutoff_date).delete()
    logger.info(f"delete {alarm_count=}, {mech_count=}, {mech_sep_count=}")
    for model in SensorType.values():
        my_col = MONGO_CLIENT[model]
        result = my_col.delete_many({'create_date': {'$lt': cutoff_date}})
        logger.info(f"Deleted {result.deleted_count} documents from {model}")
    app.send_task(
        "cloud_home.tasks.async_customer_status_statistic", args=(None,)
    )
    app.send_task(
        "cloud_home.tasks.async_customer_equipment_abnormal_ratio", args=(None,)
    )
    app.send_task(
        "cloud_home.tasks.async_customer_abnormal_count", args=(None,)
    )
    app.send_task(
        "cloud_home.tasks.async_site_status_statistic", args=(None,),
    )
    app.send_task(
        "cloud_home.tasks.async_site_equipment_abnormal_ratio", args=(None,),
    )
    app.send_task(
        "cloud_home.tasks.async_site_abnormal_count", args=(None,)
    )
    logger.info("finished clear data.")
