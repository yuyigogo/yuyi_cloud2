import logging

from customer.services.customer_service import CustomerService
from django.contrib.auth import login, logout
from django.contrib.auth.hashers import check_password
from django.http import HttpResponse
from mongoengine import DoesNotExist
from rest_framework.permissions import AllowAny
from rest_framework.request import Request
from rest_framework.status import (
    HTTP_200_OK,
    HTTP_400_BAD_REQUEST,
    HTTP_429_TOO_MANY_REQUESTS,
)
from sites.services.site_service import SiteService
from user_management.models.mongo_token import MongoToken
from user_management.models.user import CloudUser
from user_management.models.user_session import UserSession
from user_management.services.user_token_service import UserTokenService
from user_management.validators.user_login_serializers import LoginViewViewSerializer

from common.const import IP_MAX_ATTEMPTS, PASSWORD_MAX_ATTEMPTS
from common.framework.response import BaseResponse
from common.framework.view import BaseView
from common.storage.redis import normal_redis
from common.utils.captcha_utils import Captcha, get_client_ip

logger = logging.getLogger(__name__)


class LoginView(BaseView):
    authentication_classes = ()
    permission_classes = (AllowAny,)

    @staticmethod
    def _add_cloud_token_in_request_header(request: Request) -> MongoToken:
        try:
            token = UserTokenService.get_token_by_user_id(user_id=request.user.pk)
            token.emit_key()
        except DoesNotExist:
            token = MongoToken(user=request.user).save()
        finally:
            # in order to get the latest token for FE, set token into request header
            request.META["HTTP_X_AUTH_TOKEN"] = token.key
        return token

    def post(self, request):
        client_ip = get_client_ip(request)
        data, _ = self.get_validated_data(LoginViewViewSerializer, client_ip=client_ip)
        logger.info(f"{request.user.username} request login with {data=}, {client_ip=}")
        # check ip limit
        ip_key = f"login_attempts_ip_{client_ip}"
        ip_attempts = int(normal_redis.get(ip_key) or 0)
        if ip_attempts >= IP_MAX_ATTEMPTS:
            logger.info(f"{ip_key=}, {ip_attempts=}")
            return BaseResponse(
                status_code=HTTP_429_TOO_MANY_REQUESTS, msg="登录尝试次数过多，请稍后再试!"
            )

        try:
            user = CloudUser.objects.get(username=data["username"])
        except DoesNotExist:
            normal_redis.set(ip_key, ip_attempts + 1, ex=3600)
            logger.info(f"login failed with {data=}, {ip_key=}, attempts: {ip_attempts + 1}")
            return BaseResponse(
                status_code=HTTP_400_BAD_REQUEST,
                msg="账号或密码错误!",
            )
        # check user password error
        user_key = f"login_attempts_user_{str(user.id)}"
        user_attempts = int(normal_redis.get(user_key) or 0)
        if user_attempts >= PASSWORD_MAX_ATTEMPTS:
            logger.info(f"{user_key=},aaaaaaa")
            return BaseResponse(
                status_code=HTTP_429_TOO_MANY_REQUESTS, msg="密码错误次数过多，请24小时后再试!"
            )
        # check password
        if not check_password(data["password"], user.password):
            normal_redis.set(user_key, user_attempts + 1, ex=86400)
            logger.info(f"password error with {data=}, {user_key=}, {user_attempts + 1}")
            return BaseResponse(
                status_code=HTTP_400_BAD_REQUEST,
                msg="账号或密码错误!",
            )

        # reset ip and password limit
        normal_redis.delete(ip_key)
        normal_redis.delete(user_key)

        if user.is_active:
            user.remove_sessions()
            login(request, user)
            # session may be flushed after auth_login(), in this case, generate a new session key
            if not request.session.session_key:
                request.session.cycle_key()
            self._add_cloud_token_in_request_header(request)
            # note the new session of user
            UserSession(user_id=user.id, session_key=request.session.session_key).save()
            current_user = {
                "id": str(user.id),
                "username": user.username,
                "email": user.email,
                "is_cloud_super_admin": user.is_cloud_super_admin(),
                "is_client_super_admin": user.is_client_super_admin(),
                "customer": str(user.customer),
                "role_level": user.role_level,
                "customer_info": CustomerService.get_customer_info(user.customer),
                "site_info": SiteService.get_user_sites_info(user.sites),
            }
            return BaseResponse(data=current_user)
        else:
            logger.info(f"{user.username} is not active!")
            return BaseResponse(
                status_code=HTTP_400_BAD_REQUEST,
                msg="登陆失败!",
            )

    def get(self, request):
        # redirect to login template.
        return BaseResponse(status_code=HTTP_200_OK)


class LogOutView(BaseView):
    def post(self, request):
        user = request.user
        logger.info(f"{user.username} request logout!")
        UserTokenService.get_token_by_user_id(user_id=user.pk).delete()
        user.remove_sessions()
        logout(request)
        # redirect to login template.
        return BaseResponse(status_code=HTTP_200_OK)


class CaptchaView(BaseView):
    authentication_classes = ()
    permission_classes = (AllowAny,)

    def get(self, request):
        # generate captcha
        client_ip = get_client_ip(request)
        captcha_text, captcha_image = Captcha.generate_captcha()
        Captcha.store_captcha(client_ip, captcha_text)
        return HttpResponse(captcha_image, content_type="image/jpeg")
