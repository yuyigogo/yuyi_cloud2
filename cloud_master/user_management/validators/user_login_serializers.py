import logging

from rest_framework.fields import CharField

from common.const import MAX_LENGTH_NAME
from common.framework.exception import APIException
from common.framework.serializer import BaseSerializer
from common.utils.captcha_utils import Captcha

logger = logging.getLogger(__name__)


class LoginViewViewSerializer(BaseSerializer):
    username = CharField(max_length=MAX_LENGTH_NAME)
    password = CharField()
    code = CharField(required=True, allow_null=False, allow_blank=False)

    def validate_code(self, code: str):
        client_ip = self.context["client_ip"]
        if not Captcha.is_captcha_validate(client_ip, code):
            raise APIException("验证码错误!")
        logger.info(f"validate {code=} success!")
        Captcha.delete_validated_code(client_ip)
        return code
