"""
This script is to subscribe message from mqtt, and push msg to redis(list).
"""

import json
import logging
import os
import re

from cloud.settings import MQTT_CLIENT_CONFIG
from paho.mqtt import client as mqtt_client

from common.const import (
    CLIENT_ID_SENSOR_IDS_PREFIX,
    CLIENT_IDS,
    CLOUD_SUBSCRIBE_MSG_LIST,
    GATEWAY_SENSORS_TOPIC_PREFIX,
    SENSOR_INFO_PREFIX,
    MsgQueueType,
)
from common.storage.redis import msg_queue_redis, normal_redis

LOG_FORMAT = "%(asctime)s - %(levelname)s - %(message)s"
DATE_FORMAT = "%m/%d/%Y %H:%M:%S %p"
logging.basicConfig(
    # filename="/home/logs/sync_push_subscribe_message.log",
    level=logging.INFO,
    format=LOG_FORMAT,
    datefmt=DATE_FORMAT,
)

# topics to subscribe
sensor_data_pattern = re.compile(
    r"/(?P<gateway_id>[a-zA-Z0-9]+)/subnode/(?P<sensor_id>[a-zA-Z0-9]+)/data_ctrl/property"
)  # 数据上传
sensor_alarm_pattern = re.compile(
    r"/(?P<gateway_id>[a-zA-Z0-9]+)/subnode/(?P<sensor_id>[a-zA-Z0-9]+)/common/event"
)  # 传感器报警
sensors_in_gateway_pattern = re.compile(
    r"/(?P<gateway_id>[a-zA-Z0-9]+)/serivice_reply/sub_get"
)  # 匹配网关下传感器列表
sensor_period_pattern = re.compile(
    r"/(?P<gateway_id>[a-zA-Z0-9]+)/subnode/(?P<sensor_id>[a-zA-Z0-9]+)/common/service_reply/sample_period_set"
)  # 采集周期设置
sensor_work_mode_pattern = re.compile(
    r"/(?P<gateway_id>[a-zA-Z0-9]+)/subnode/(?P<sensor_id>[a-zA-Z0-9]+)/common/service_reply/work_mode_set"
)  # 工作模式设置主题


class SyncSubscribeMsg:
    """
    just push matched data into redis
    """

    def __init__(self, client_id, host, port):
        self.client_id = client_id
        self.host = host
        self.port = port
        self.client = mqtt_client.Client(self.client_id)

    @staticmethod
    def on_connect(client, userdata, flags, rc):
        logging.info(f"SyncSubscribeMsg connected with result code " + str(rc))
        client.subscribe("#")  # 订阅消息

    @staticmethod
    def on_message(client, userdata, msg):
        topic = msg.topic
        need_valid_sensor_id = False
        if sensor_data_ret := sensor_data_pattern.match(msg.topic):
            # 采集数据上传
            need_valid_sensor_id = True
            gateway_id, sensor_id = (
                sensor_data_ret.groups()[0],
                sensor_data_ret.groups()[1],
            )
            msg_queue_type = MsgQueueType.DATA_LOADER.value
        elif alarm_ret := sensor_alarm_pattern.match(topic):
            # 传感器报警
            need_valid_sensor_id = True
            gateway_id, sensor_id = alarm_ret.groups()[0], alarm_ret.groups()[1]
            msg_queue_type = MsgQueueType.SENSOR_ALARM.value
        elif sensors_in_gateway_ret := sensors_in_gateway_pattern.match(topic):
            # 匹配网关下传感器列表
            need_valid_sensor_id = False
            gateway_id = sensors_in_gateway_ret.groups()[0]
            gateway_key = f"{GATEWAY_SENSORS_TOPIC_PREFIX}{gateway_id}"
            if not normal_redis.exists(gateway_key):
                logging.info(
                    f"can't matched in sensors_in_gateway in redis for {gateway_key=}"
                )
                return
            logging.info(f"matched in sensors_in_gateway in redis for {gateway_key=}")
            normal_redis.delete(gateway_key)
            logging.info(
                f"delete the matched sensors_in_gateway key in redis for {gateway_key=}"
            )
            sensor_id = None
            msg_queue_type = MsgQueueType.SENSORS_IN_GATEWAY.value
        # elif sensor_period_ret := sensor_period_pattern.match(topic):
        #     # 采集周期设置(不需回调处理)
        #     gateway_id, sensor_id = (
        #         sensor_period_ret.groups()[0],
        #         sensor_period_ret.groups()[1],
        #     )
        #     msg_queue_type = MsgQueueType.SENSOR_PERIOD.value
        elif sensor_work_mode_ret := sensor_work_mode_pattern.match(topic):
            need_valid_sensor_id = True
            gateway_id, sensor_id = (
                sensor_work_mode_ret.groups()[0],
                sensor_work_mode_ret.groups()[1],
            )
            msg_queue_type = MsgQueueType.SENSOR_WORK_MODE.value
        else:
            return
        if not SyncSubscribeMsg.is_client_id_enabled(gateway_id):
            return
        if need_valid_sensor_id:
            gateway_sensor_ids = SyncSubscribeMsg.get_client_id_sensor_ids_info(
                gateway_id
            )
            if not gateway_sensor_ids:
                return
            if sensor_id and (
                sensor_id not in gateway_sensor_ids
                or not SyncSubscribeMsg.is_sensor_id_enabled(sensor_id)
            ):
                """
                数据解析的条件：1.网关已建立；2.sensor_id在网关下传感器列表里；3.设备等档案已绑定；4.model_key和sensor_type匹配
                """
                return
        msg_str = msg.payload.decode("utf-8")
        queue_data = {
            "msg_queue_type": msg_queue_type,
            "gateway_id": gateway_id,
            "sensor_id": sensor_id,
            "msg_str": msg_str,
        }
        logging.info(
            f"************* matched data has been configured with {gateway_id=}, {sensor_id=}, {msg_queue_type=}"
            f" {msg_str=}push it to redis! ***********************"
        )
        msg_queue_redis.lpush(CLOUD_SUBSCRIBE_MSG_LIST, json.dumps(queue_data))

    @staticmethod
    def on_subscribe(client, userdata, mid, granted_qos):
        logging.info("On Subscribed: qos = %d" % granted_qos)

    @staticmethod
    def on_disconnect(client, userdata, rc):
        if rc != 0:
            logging.warning("SyncSubscribeMsg Unexpected disconnection %s" % rc)
        else:
            logging.info("SyncSubscribeMsg disconnection !!!")

    @classmethod
    def is_sensor_id_enabled(cls, sensor_id: str) -> bool:
        sensor_info_key = f"{SENSOR_INFO_PREFIX}{sensor_id}"
        has_bound = normal_redis.hget(sensor_info_key, "has_bound")
        is_enabled = (
            True if has_bound is not None and has_bound.lower() == "true" else False
        )
        return is_enabled

    @classmethod
    def is_client_id_enabled(cls, client_id: str) -> bool:
        return normal_redis.sismember(CLIENT_IDS, client_id)

    @classmethod
    def get_client_id_sensor_ids_info(cls, client_number: str) -> list:
        client_sensor_ids_key = f"{CLIENT_ID_SENSOR_IDS_PREFIX}{client_number}"
        sensor_ids = normal_redis.get(client_sensor_ids_key)
        return json.loads(sensor_ids) if sensor_ids else []

    def run(self):
        self.client.username_pw_set(
            MQTT_CLIENT_CONFIG["user"], MQTT_CLIENT_CONFIG["pw"]
        )
        self.client.on_connect = SyncSubscribeMsg.on_connect
        self.client.on_message = SyncSubscribeMsg.on_message
        self.client.on_subscribe = SyncSubscribeMsg.on_subscribe
        self.client.on_disconnect = SyncSubscribeMsg.on_disconnect
        self.client.connect(self.host, self.port, 60)
        self.client.loop_forever()


if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "cloud.settings")
    own_client_id = MQTT_CLIENT_CONFIG.get("own_client_id", "")
    host = MQTT_CLIENT_CONFIG.get("host", "")
    port = MQTT_CLIENT_CONFIG.get("port", "")
    sync_subscribe_msg = SyncSubscribeMsg(own_client_id, host, port)
    logging.info(
        "************************* start to run sync_push_subscribe_message ********************"
    )
    try:
        sync_subscribe_msg.run()
    except Exception as e:
        logging.error(
            f"deal with push msg into queue error with exception: {e=}", exc_info=True
        )
