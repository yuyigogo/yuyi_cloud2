from cloud.models import CloudDocument
from mongoengine import (
    BooleanField,
    DateTimeField,
    IntField,
    ObjectIdField,
    StringField,
)

from common.const import MAX_MESSAGE_LENGTH, AlarmFlag


class AlarmInfo(CloudDocument):
    sensor_id = StringField(required=True)
    sensor_type = StringField(required=True)
    alarm_flag = IntField(default=AlarmFlag.NO_PUSH.value)
    alarm_type = IntField(required=True)
    alarm_level = IntField(required=True)
    alarm_describe = IntField(default=0)
    is_processed = BooleanField(default=False)
    processed_remarks = StringField(max_length=MAX_MESSAGE_LENGTH)
    is_online = BooleanField(
        default=True
    )  # for now not used, change is_online to point
    is_latest = BooleanField(default=True)  # 目前只有测点告警会用到，所以传感器告警默认设置为false
    sensor_data_id = ObjectIdField()
    client_number = StringField()  # 测点绑定了传感器，但是未获取传感器列表，传感器在线率定时任务判断时，该字段未空
    customer_id = ObjectIdField()
    site_id = ObjectIdField()
    equipment_id = ObjectIdField()
    point_id = ObjectIdField()
    last_received_time = DateTimeField(null=True)  # 传感器告警，掉线后最后一次解析数据的时间(在线时间)

    meta = {
        "indexes": [
            "sensor_id",
            "alarm_type",
            "alarm_level",
            "sensor_type",
            "is_latest",
            "is_online",
            "is_processed",
            "create_date",
            "equipment_id",
            "site_id",
            "customer_id",
            "point_id",
            ("equipment_id", "alarm_type", "is_latest", "create_date"),
            "client_number",
            # ("client_number", "is_latest"),
            # ("alarm_type", "is_latest", "equipment_id"),
            # ("alarm_type", "is_latest", "customer_id"),
            # ("alarm_type", "is_latest", "site_id"),
        ],
        "index_background": True,
        "collection": "alarm_info",
    }

    def __str__(self):
        return "AlarmInfo: {}".format(self.sensor_id)

    def __repr__(self):
        return self.__str__()
