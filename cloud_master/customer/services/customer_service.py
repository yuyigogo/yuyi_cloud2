from typing import Optional, Union

from bson import ObjectId
from customer.models.customer import Customer
from equipment_management.models.gateway import GateWay
from file_management.models.electrical_equipment import ElectricalEquipment
from file_management.models.measure_point import MeasurePoint
from sites.models.site import Site
from user_management.models.user import CloudUser

from common.const import ALL
from common.framework.service import BaseService


class CustomerService(BaseService):
    @classmethod
    def create_customer(
        cls, name: str, administrative_division: dict, remarks: Optional[str] = None
    ) -> Customer:
        customer = Customer(
            name=name, administrative_division=administrative_division, remarks=remarks
        )
        customer.save()
        return customer

    @classmethod
    def delete_customer(cls, customer: Customer, clear_resource: bool):
        # when delete customer, will delete the resources in this customer
        customer_id = customer.pk
        gateways = GateWay.objects.filter(customer=customer_id)
        client_numbers = list(gateways.values_list("client_number"))
        sites = Site.objects.filter(customer=customer_id)
        site_ids = sites.values_list("id")
        points = MeasurePoint.objects.filter(customer_id=customer_id)
        # clear points and sensor data
        if points:
            cls.delete_points(points, clear_resource=clear_resource, delete_sensor_config=True)

        gateways.delete()
        Site.objects.filter(customer=customer_id).delete()
        ElectricalEquipment.objects.filter(customer_id=customer_id).delete()
        customer.delete()
        CloudUser.objects.filter(customer=customer_id).delete()
        cls.delete_customer_status_infos(str(customer_id))
        cls.delete_sites_status_infos(list(map(str, site_ids)))
        # clear client_id from redis
        if client_numbers:
            cls.delete_client_id_sensor_ids_info_in_redis(client_numbers)
            cls.remove_client_ids_from_redis(client_numbers)

    @classmethod
    def named_all_customer_id(cls):
        return str(Customer.objects.get(name=ALL).pk)

    @classmethod
    def get_customer_id_name_dict(cls, customer_ids: Union[set, list]) -> dict:
        return dict(
            Customer.objects.filter(id__in=customer_ids).values_list("id", "name")
        )

    @classmethod
    def get_customer_info(cls, customer_id: Union[str, ObjectId]) -> dict:
        customer = Customer.objects.get(id=customer_id)
        return {
            "id": str(customer.id),
            "name": customer.name,
            "administrative_division": customer.administrative_division,
            "remarks": customer.remarks,
        }
