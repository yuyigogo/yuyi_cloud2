import logging

from cloud.models import CloudDocument
from mongoengine import DictField, StringField

from common.const import MAX_LENGTH_NAME, MAX_MESSAGE_LENGTH

logger = logging.getLogger(__name__)


class Customer(CloudDocument):
    name = StringField(unique=True, max_length=MAX_LENGTH_NAME)
    administrative_division = DictField(required=True)
    remarks = StringField(max_length=MAX_MESSAGE_LENGTH)

    meta = {"indexes": ["name"], "index_background": True, "collection": "customer"}

    def __str__(self):
        return "CustomerModel: {}".format(self.name)

    def __repr__(self):
        return self.__str__()

    @property
    def id(self):
        return self._id

    # def delete(self, is_delete_customer_resource=True, **kwargs):
    #     from cloud_home.models.status_statistics import CAbnormalRatio, CStatusStatistic
    #     if is_delete_customer_resource:
    #         CStatusStatistic.objects.filter(customer_id=self.id).delete()
    #         CAbnormalRatio.objects.filter(customer_id=self.id).delete()
    #         delete_keys = [
    #             f"{customer_day_abnormal_info}{str(self.id)}",
    #             f"{customer_week_abnormal_info}{str(self.id)}",
    #             f"{customer_month_abnormal_info}{str(self.id)}",
    #         ]
    #         k1, *k2 = delete_keys
    #         try:
    #             normal_redis.delete(k1, *k2)
    #         except Exception as e:
    #             logger.exception(
    #                 f"delete customer resource for {delete_keys=} exception with {e=}"
    #             )
    #     super(Customer, self).delete(**kwargs)
