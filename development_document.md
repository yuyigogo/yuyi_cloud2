# 1. sensor_config和相关缓存的创建/更新/删除逻辑
建立档案的步骤：
    1。建立公司，站点，设备；
    2。建立主机；
    3。获取主机下传感器列表，得到传感器ID；
    4。拿到传感器ID，建立测点，绑定到测点

在mqtt传数据时，我需要将传感器的数据和档案绑定起来，一遍检索和ws长链接那边主动上推，而绑定起来我需要公司，站点，测点信息，还有model_key信息。
公司那些信息只能在建立档案时那边获得，model_key，能在获取传感器列表那边获取，需sensor_id将这2个信息关联起来关联

## 1.1 网关
### 1.1.1 新建
    a. 获取网关下的传感器列表：先找出不在该网关下的传感器，将其删除(sensor_config, redis:sensor_info);
    b. 新增新添加的传感器，新建(sensor_config, redis:sensor_info)
    c. 重新全量设置client_sensor_list缓存
### 1.1.2 更新
    网关更新不允许修改client_number,因此不需要修改其下的配置
###  1.1.3 删除
    删除网关时，需要将其下的相应资源都删除(设备档案不影响)

## 1.2 测点
### 1.2.1 新建
#### 1.2.1.1 excel新建
    更新/新建sensor_config, redis:sensor_info
#### 1.2.1.2 UI新建
    下拉/自填：a.查sensor_config: sensor_type, point_id, has_bound;
            b. 更新/新建sensor_config, redis:sensor_info
### 1.2.2 更新
    测点修改绑定的传感器：删除old,新建new

###1.2.3 删除
    测点删除：删除sensor_config, redis:sensor_info




def create_or_update_sensor_config(
    #     self,
    #     customer_id: str,
    #     site_id: str,
    #     equipment_id: str,
    #     point_id: str,
    #     sensor_type: str,
    # ):
    #     try:
    #         collection = SensorConfig._get_collection()
    #         collection.update_one(
    #             {"sensor_number": self.sensor_id},
    #             {
    #                 "$set": {
    #                     "customer_id": customer_id,
    #                     "site_id": site_id,
    #                     "equipment_id": equipment_id,
    #                     "point_id": point_id,
    #                     "sensor_type": sensor_type,
    #                     "has_bound": True,
    #                     "update_date": datetime.now(),
    #                     "create_date": datetime.now(),
    #                 }
    #             },
    #             upsert=True,
    #         )
    #     except Exception as e:
    #         logger.error(
    #             f"create_or_update_sensor_config error for {self.sensor_id=} with error: {e}"
    #         )
    #         return
    #     self.set_sensor_info_to_redis(customer_id, site_id, equipment_id, point_id)